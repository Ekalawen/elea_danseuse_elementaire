using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.VFX;
//using UnityEngine.Rendering.PostProcessing;

public class StoryManager : MonoBehaviour {

    protected static string START_ANIMATOR = "Start";
    protected static string STOP_ANIMATOR = "Stop";

    public string levelName;
    public string nextSceneName;
    public bool dontDestroyPlayerOnReachExit = true;
    public TextAsset startFile;
    public TextAsset endFile;
    public List<StoryCharacter> characters;

    public List<StoryLine> startLines;
    public List<StoryLine> endLines;

    [HideInInspector]
    public UnityEvent onStopDisplayLines = new UnityEvent();

    protected GameManager gm;
    protected bool dontUnpauseAtEndOfDisplayLines = false;
    protected Animator linesAnimator;
    protected TMP_Text storyText;
    protected Image storyImage;
    protected Animator sceneTransitionAnimator;
    protected List<AutomaticStory> automaticStories;

    private void OnValidate() {
        startLines = GetStoryLinesFor(startFile, characters);
        endLines = GetStoryLinesFor(endFile, characters);
    }

    public void Initialize() {
        gm = GameManager.Instance;
        GetUIManagerAttributes();
        sceneTransitionAnimator.gameObject.SetActive(true);
        sceneTransitionAnimator.SetTrigger(START_ANIMATOR);
        DisplayLines(startLines);
        gm.objectManager.onReachExit.AddListener(SavePlayerPrefab);
        gm.objectManager.onReachExit.AddListener(DisplayEndLinesAndGoToNextScene);
        gm.objectManager.onReachExit.AddListener(SetLevelAsCompleted);
        InitializeAutomaticStories();
        InitializeLevelName();
    }

    protected void SetLevelAsCompleted() {
        PrefsManager.SetHasCompletedLevel(SceneManager.GetActiveScene().name, true);
    }

    protected void SavePlayerPrefab() {
        if(dontDestroyPlayerOnReachExit) {
            gm.player.DontDestroyOnLoad();
        }
    }

    protected void InitializeLevelName() {
        gm.uiManager.levelNameText.text = levelName;
    }

    protected void InitializeAutomaticStories() {
        automaticStories = GetComponentsInChildren<AutomaticStory>().ToList();
        foreach(AutomaticStory automaticStory in automaticStories) {
            automaticStory.Initialize();
        }
    }

    private void GetUIManagerAttributes()
    {
        linesAnimator = gm.uiManager.linesAnimator;
        storyText = gm.uiManager.storyText;
        storyImage = gm.uiManager.storyImage;
        sceneTransitionAnimator = gm.uiManager.sceneTransitionAnimator;
    }

    public void DisplayEndLinesAndGoToNextScene() {
        onStopDisplayLines.AddListener(GoToNextScene);
        dontUnpauseAtEndOfDisplayLines = true;
        DisplayLines(endLines);
    }

    protected void GoToNextScene() {
        StartCoroutine(CGoToNextScene());
    }

    protected IEnumerator CGoToNextScene() {
        sceneTransitionAnimator.SetTrigger(STOP_ANIMATOR);
        yield return new WaitForSecondsRealtime(1.0f);

        if (nextSceneName != null && nextSceneName != "") {
            SceneManager.LoadScene(nextSceneName);
        } else {
            string currentSceneName = SceneManager.GetActiveScene().name;
            SceneManager.LoadScene(currentSceneName);
        }
    }

    public void DisplayLines(List<StoryLine> lines) {
        StartCoroutine(CDisplayLines(lines));
    }

    protected IEnumerator CDisplayLines(List<StoryLine> lines) {
        Tooltip.Hide();
        linesAnimator.SetTrigger(START_ANIMATOR);
        gm.Pause();
        foreach(StoryLine line in lines) {
            PopulateLine(line);
            if(line == lines.First()) {
                yield return new WaitForSecondsRealtime(1.0f);
            }
            yield return new WaitUntil(() => Input.anyKeyDown);
            yield return null;
        }
        if (dontUnpauseAtEndOfDisplayLines) {
            dontUnpauseAtEndOfDisplayLines = false;
        } else {
            gm.UnPause();
        }
        linesAnimator.SetTrigger(STOP_ANIMATOR);
        yield return new WaitForSecondsRealtime(1.0f);
        onStopDisplayLines.Invoke();
    }

    protected void PopulateLine(StoryLine line) {
        if (line.storyCharacter.name != "") {
            storyText.text = UIHelper.SurroundWithColor(line.storyCharacter.name + "\n\n", line.storyCharacter.nameColor);
        } else {
            storyText.text = "";
        }
        string text = UIHelper.SurroundWithColorWithoutB(line.text, line.storyCharacter.color);
        if(line.storyCharacter.useItalic) {
            text = UIHelper.SurroundWithItalic(text);
        }
        storyText.text += text;
        storyImage.sprite = line.storyCharacter.sprite;
        if(storyImage.sprite == null) {
            storyImage.color = ColorsTools.SetAlpha(Color.white, 0);
        } else {
            storyImage.color = Color.white;
        }
    }

    public static List<StoryLine> GetStoryLinesFor(TextAsset file, List<StoryCharacter> characters) {
        if(file == null) {
            return new List<StoryLine>();
        }
        List<StoryLine> storyLines = new List<StoryLine>();
        string[] lines = file.text.Split(Environment.NewLine, StringSplitOptions.RemoveEmptyEntries);
        foreach (string line in lines)
        {
            string characterId = line.Split(" ").First();
            string text = line.Split(" ", 2).Last();
            StoryCharacter character = characters.Find(c => c.id == characterId);
            if (character == null)
            {
                throw new Exception($"Character {characterId} unknown !");
            }
            storyLines.Add(new StoryLine(character, text));
        }
        return storyLines;
    }
}
