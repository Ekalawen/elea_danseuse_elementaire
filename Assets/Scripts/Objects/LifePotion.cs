using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.ResourceManagement.AsyncOperations;

public class LifePotion : Object {

    public int nbLifeGained = 1;
    public bool increaseMaxHealthToo = false;

    public override bool CanBePickedUpBy(Character character) {
        return increaseMaxHealthToo || !character.health.IsMaxHealth();
    }

    protected override void PickUpBySpecific(Socle objectSocle, Character character) {
        if(increaseMaxHealthToo) {
            character.health.GainMaxLifeOnly(nbLifeGained);
        }
        character.health.GainLife(nbLifeGained);
    }
}
