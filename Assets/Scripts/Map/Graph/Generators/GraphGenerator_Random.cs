using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GraphGenerator_Random : GraphGenerator {

    public Vector2 area = new Vector2(10, 10);
    public int nbNodes = 10;
    public int nbEdges = 20;

    public override void CreateGraph() {
        for (int i = 0; i < nbNodes; i++)
        {
            nodes.Add(CreateRandomNode());
        }

        for (int i = 0; i < nbEdges; i++)
        {
            CreateAndAddRandomEdge();
        }
    }
    protected Edge CreateAndAddRandomEdge()
    {
        Node node1 = MathTools.ChoseOne(nodes);
        Node node2 = MathTools.ChoseOneOther(nodes, node1);
        Edge edge = CreateAndAddEdge(node1, node2);
        return edge;
    }

    public Node CreateRandomNode() {
        Vector2 pos = new Vector2(UnityEngine.Random.Range(0, area.x), UnityEngine.Random.Range(0, area.y));
        Node node = CreateNode(pos);
        return node;
    }
}
