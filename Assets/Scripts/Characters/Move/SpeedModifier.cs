﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class SpeedModifier {
    public enum Mode { ADD, SET };
    public enum Period { DURATION, UNTIL_NEXT_MOVE, INFINITE_DURATION };

    public Mode mode = Mode.ADD;
    public float speed = 1.0f;
    public Period period = Period.DURATION;
    [ConditionalHide("mode")]
    public float duration = 3.0f;

    protected Movement movement;
    protected Timer timer;

    public SpeedModifier(Mode mode, Period period, float speed, float duration=0) {
        this.mode = mode;
        this.speed = speed;
        this.period = period;
        this.duration = duration;
    }


    public SpeedModifier(SpeedModifier other) {
        this.mode = other.mode;
        this.speed = other.speed;
        this.period = other.period;
        this.duration = other.duration;
    }

    public void Initialize(Movement movement) {
        this.movement = movement;
        if (UseDuration()) {
            timer = new Timer(duration);
            movement.StartCoroutine(CRemoveWhenDurationIsOver());
        } else if (UseUntilNextMove()) {
            timer = new Timer(float.PositiveInfinity);
            movement.owner.onReachNode.AddListener(Stop);
        } else {
            timer = new Timer(float.PositiveInfinity);
        }
    }

    public bool IsAdder() {
        return mode == Mode.ADD;
    }

    public bool IsSetter() {
        return mode == Mode.SET;
    }

    public bool UseDuration() {
        return period == Period.DURATION;
    }

    public bool UseUntilNextMove() {
        return period == Period.UNTIL_NEXT_MOVE;
    }

    public bool UseInfiniteDuration() {
        return period == Period.INFINITE_DURATION;
    }

    public float Get() {
        return speed;
    }

    public bool IsOver() {
        return timer.IsOver();
    }

    protected void Stop(Node nextNode = null) {
        timer.SetOver();
        movement.owner.onReachNode.RemoveListener(Stop);
        movement.RemoveModifier(this);
    }

    public override string ToString() {
        string modeString = mode == Mode.ADD ? "ADD" : "SET";
        string periodString = period == Period.DURATION ? $"for {duration}" : ((period == Period.INFINITE_DURATION) ? "forever" : "until next move");
        return $"{modeString} {periodString} : {speed}";
    }

    protected IEnumerator CRemoveWhenDurationIsOver() {
        yield return new WaitForSeconds(duration);
        Stop();
    }
}