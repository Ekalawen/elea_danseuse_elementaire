﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public abstract class AttackModifier_OnEvent : AttackModifier {

    public enum Mode { ON_HIT, ON_KILL, ON_BOTH };

    public Mode mode = Mode.ON_HIT;

    public List<UnityEvent<Character>> ModeToEvents(Mode mode) {
        Dictionary<Mode, List<UnityEvent<Character>>> modeToEvent = new Dictionary<Mode, List<UnityEvent<Character>>>();
        modeToEvent[Mode.ON_HIT] = new List<UnityEvent<Character>>() { attack.onHit };
        modeToEvent[Mode.ON_KILL] = new List<UnityEvent<Character>>() { attack.onKill };
        modeToEvent[Mode.ON_BOTH] = new List<UnityEvent<Character>>() { attack.onHit, attack.onKill };
        return modeToEvent[mode];
    }

    public List<UnityEvent<Character>> CurrentEvents() {
        return ModeToEvents(mode);
    }

    public void AddListenerToEvents(UnityAction<Character> listener) {
        foreach(UnityEvent<Character> currentEvent in CurrentEvents()) {
            currentEvent.AddListener(listener);
        }
    }

    protected string DescriptionOnEvent() {
        string attackMode = mode == Mode.ON_KILL ? "un kill" : "une attaque";
        return $"après {attackMode}";
    }
}