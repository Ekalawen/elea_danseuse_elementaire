using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GraphModifier_RandomColors : GraphModifier {
    public override void Apply() {
        foreach(Edge edge in graph.edges) {
            SetRandomColor(edge);
        }
    }

    protected void SetRandomColor(Edge edge) {
        float hue = UnityEngine.Random.Range(0f, 1f);
        edge.SetColor(hue);
    }
}
