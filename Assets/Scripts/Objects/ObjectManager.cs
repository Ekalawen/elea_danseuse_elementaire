using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;
using UnityEngine.ResourceManagement.AsyncOperations;

public class ObjectManager : SpawnerManager {

    public GameObject exitPrefab;
    public int maxNbOfObjects = 50;

    public float minSpawnDistance = 4.0f;
    public Spawner dropSpawner;
    public int nbInitialDrops = 0;


    protected GameManager gm;
    protected List<Spawner> spawners;
    [HideInInspector]
    public List<Object> objects;
    [HideInInspector]
    public UnityEvent onReachExit = new UnityEvent();
    [HideInInspector]
    public UnityEvent<Object> onCatchObject = new UnityEvent<Object>();
    [HideInInspector]
    public UnityEvent onCatchObject2 = new UnityEvent();
    [HideInInspector]
    public UnityEvent<Book> onCatchBook = new UnityEvent<Book>();
    [HideInInspector]
    public UnityEvent<ElementalAltar> onActivateElementalAltar = new UnityEvent<ElementalAltar>();
    [HideInInspector]
    public ItemRarityManager itemRarityManager;

    protected Exit exit;

    public void Initialize() {
        gm = GameManager.Instance;
        objects = new List<Object>();
        InitializeItemRarityManager();
        SpawnExit();
        InitializeSpawners();
        InitialDrops();
    }

    protected void InitializeItemRarityManager() {
        itemRarityManager = Instantiate(gm.itemRarityManagerPrefab, parent: transform).GetComponent<ItemRarityManager>();
        itemRarityManager.Initialize();
    }
    protected void SpawnExit() {
        Node exitNode = gm.map.ExitPosition();
        exit = SpawnObjectAt(exitPrefab, exitNode).GetComponent<Exit>();
    }

    protected void InitializeSpawners() {
        spawners = GetComponentsInChildren<Spawner>().ToList();
        spawners.Remove(dropSpawner);
        foreach(Spawner spawner in spawners) {
            spawner.Initialize(this);
        }
    }

    public override List<MonoBehaviour> SpawnSpawningQuantity(List<SpawningQuantity> spawningQuantities, Func<float, Node> method) {
        return SpawnObjectQuantities(spawningQuantities, method).Cast<MonoBehaviour>().ToList();
    }


    public List<Object> SpawnObjectQuantities(List<SpawningQuantity> objectQuantities, Func<float, Node> method) {
        List<Object> spawnedObjects = new List<Object>();
        foreach (SpawningQuantity objectQuantity in objectQuantities) {
            List<Object> newObject = SpawnObjectQuantity(objectQuantity, method);
            spawnedObjects.AddRange(newObject);
        }
        return spawnedObjects;
    }

    public List<Object> GetFixObjects() {
        return objects.FindAll(o => o.GetComponent<Exit>() != null || o.GetComponent<ElementalAltar>() != null);
    }

    public List<Object> SpawnObjectQuantity(SpawningQuantity objectQuantity, Func<float, Node> method) {
        List<Object> generated = new List<Object>();
        foreach(GameObject objectPrefab in objectQuantity.GetPrefabs()) {
            Object obj = SpawnObject(objectPrefab, method);
            if (obj != null) {
                generated.Add(obj);
            }
        }
        return generated;
    }

    public Object SpawnObject(GameObject prefab, Func<float, Node> method) {
        if (objects.Count >= maxNbOfObjects) {
            return null;
        }
        float objectSize = prefab.GetComponent<Object>().socle.Get();
        Node startNode = method != null ? method(objectSize) : GetSpawningNode(objectSize);
        return SpawnObjectAt(prefab, startNode);
    }

    protected Object SpawnObjectAt(GameObject prefab, Node node) {
        if (node == null) {
            return null;
        }
        Object obj = Instantiate(prefab, parent: transform).GetComponent<Object>();
        obj.Initialize(node);
        gm.postProcessManager.InstantiateVfxSpawning(obj.pos);
        return obj;
    }

    public override Node GetSpawningNode(float socleRange) {
        List<Node> candidates = new List<Node>();
        for (float currentSpawnDistance = minSpawnDistance; currentSpawnDistance >= 0 && candidates.Count == 0; currentSpawnDistance--) {
            List<Node> emptyNodes = gm.graph.EmptyNodes(socleRange / 2);
            candidates = gm.graph.GetAllAtLeastAsFarFrom(emptyNodes, gm.player.pos, currentSpawnDistance);
        }
        if (candidates.Count == 0) {
            return null;
        }
        return MathTools.ChoseOne(candidates);
    }

    public Object DropItem() {
        Node node = GetSpawningNode(0.45f); // half of a normal item socle
        return DropItemTo(node);
    }

    public Object DropItemTo(Node node) {
        GameObject itemPrefab = MathTools.ChoseOne(dropSpawner.GetPrefabs());
        return SpawnObjectAt(itemPrefab, node);
    }

    protected void InitialDrops() {
        for(int i = 0; i < nbInitialDrops; i++) {
            DropItem();
        }
    }

    public List<Book> GetBooks() {
        return objects.Select(o => o.GetComponent<Book>()).ToList().FindAll(b => b != null);
    }

    public override Spawner AddSpawner(Spawner spawner) {
        spawner.Initialize(this);
        spawners.Add(spawner);
        return spawner;
    }

    public override bool RemoveSpawner(Spawner spawner) {
        bool hasBeenRemoved = spawners.Remove(spawner);
        spawner.Stop();
        return hasBeenRemoved;
    }

    public void OpenExit() {
        exit.OpenSprite();
    }

    public void CloseExit() {
        exit.CloseExit();
    }

    public Exit GetExit() {
        return exit;
    }
}
