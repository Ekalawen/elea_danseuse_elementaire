﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Tooltip : MonoBehaviour {

    public static Tooltip instance;

    public bool useOverlay = false;
    public RectTransform background;
    public TMP_Text text;
    public RectTransform screen;

    protected new Camera camera;
    protected float planeDistance;
    protected Timer lastHideTime;
    protected bool shouldSetPositionToMouse = true;
    protected SingleCoroutine rollCoroutine;

    public void Awake() {
        instance = this;
        gameObject.SetActive(false);
        camera = Camera.main;
        rollCoroutine = new SingleCoroutine(this);
        planeDistance = FindObjectOfType<Canvas>().planeDistance;
        lastHideTime = new UnpausableTimer();
        lastHideTime.SetOver();
    }

    public void Update() {
        if (shouldSetPositionToMouse) {
            SetPositionToMouse();
        }
    }

    protected void SetPositionToMouse() {
        RectTransform rectTransform = GetComponent<RectTransform>();
        if (useOverlay) {
            Vector2 localPoint2D;
            RectTransformUtility.ScreenPointToLocalPointInRectangle(screen, Input.mousePosition, null, out localPoint2D);
            rectTransform.localPosition = localPoint2D;
        } else {
            Vector3 localPoint;
            Vector3 screenPoint = Input.mousePosition;
            screenPoint.z = planeDistance;
            localPoint = camera.ScreenToWorldPoint(screenPoint);
            rectTransform.position = localPoint;
        }


        Vector2 minPosition = screen.rect.min - background.rect.min;
        Vector2 maxPosition = screen.rect.max - background.rect.max;
        Vector2 clampedPos = new Vector2(
            Mathf.Clamp(rectTransform.localPosition.x, minPosition.x, maxPosition.x),
            Mathf.Clamp(rectTransform.localPosition.y, minPosition.y, maxPosition.y));
        rectTransform.localPosition = clampedPos;
    }

    protected void ShowProtected(string message, float timeBeforeShowingUsed) {
        //if(lastHideTime.GetElapsedTime() < timeBeforeShowingUsed) {
        //    return;
        //}
        text.SetText(message);
        //background.sizeDelta = text.GetPreferredValues(message); // ==> This will one line
        background.sizeDelta = new Vector2(background.sizeDelta.x, text.preferredHeight); // First we adjust the height by keeping the width constant
        background.sizeDelta = new Vector2(text.preferredWidth, background.sizeDelta.y); // Then if we have too much width, we cut it
        SetPositionToMouse();
        gameObject.SetActive(true);
    }

    protected void HideProtected() {
        gameObject.SetActive(false);
        lastHideTime.Reset();
    }

    public static void Show(string message, float timeBeforeShowingUsed) {
        instance.ShowProtected(message, timeBeforeShowingUsed);
        instance.shouldSetPositionToMouse = true;
    }

    public static void Hide() {
        instance.HideProtected();
    }

    public static void SetPotitionTo(Vector2 worldPosition) {
        instance.shouldSetPositionToMouse = false;
        RectTransform rectTransform = instance.GetComponent<RectTransform>();
        rectTransform.position = worldPosition;

        Vector2 minPosition = instance.screen.rect.min - instance.background.rect.min;
        Vector2 maxPosition = instance.screen.rect.max - instance.background.rect.max;
        Vector2 clampedPos = new Vector2(
            Mathf.Clamp(rectTransform.localPosition.x, minPosition.x, maxPosition.x),
            Mathf.Clamp(rectTransform.localPosition.y, minPosition.y, maxPosition.y));
        rectTransform.localPosition = clampedPos;
    }

    public static void Unroll(float duration) {
        instance.rollCoroutine.Start(instance.UnrollTo(0.0f, instance.background.sizeDelta.y, duration));
    }

    public static void Roll(float duration) {
        instance.gameObject.SetActive(true);
        instance.rollCoroutine.Start(instance.UnrollTo(instance.background.sizeDelta.y, 0.0f, duration));
    }

    protected IEnumerator UnrollTo(float initialY, float ySizeDelta, float duration) {
        Timer timer = new Timer(duration);
        background.sizeDelta = new Vector2(background.sizeDelta.x, initialY);
        while(!timer.IsOver()) {
            float newY = MathCurves.Linear(initialY, ySizeDelta, timer.GetAvancement());
            background.sizeDelta = new Vector2(background.sizeDelta.x, newY);
            yield return null;
        }
        background.sizeDelta = new Vector2(background.sizeDelta.x, ySizeDelta);
        if(ySizeDelta == 0) {
            gameObject.SetActive(false);
        }
    }
}
