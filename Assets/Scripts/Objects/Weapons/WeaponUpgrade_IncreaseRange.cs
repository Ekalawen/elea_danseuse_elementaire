﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

public class WeaponUpgrade_IncreaseRange : WeaponUpgrade {

    public float rangeIncrease = 0.5f;

    protected override void AddUpgradeToAttack(Attack attack) {
        List<AttackModifier> attackModifiers = attack.GetModifiers();
        AttackModifier_IncreaseRange theModifier = attackModifiers.Find(m => m.GetComponent<AttackModifier_IncreaseRange>() != null)?.GetComponent<AttackModifier_IncreaseRange>();
        if(theModifier == null) {
            base.AddUpgradeToAttack(attack);
            return;
        }
        theModifier.rangeIncrease += rangeIncrease;
        theModifier.isUpgrade = true;
        theModifier.upgradeRarity = RarityHelper.BestRarity(rarity, theModifier.upgradeRarity);
    }
}