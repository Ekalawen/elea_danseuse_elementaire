﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;

public class ItemModifier_AdditionnalLife : ItemModifier {

    public int addedMaxHeart = 1;
    public bool shouldGainFullHearts = false;

    protected int lifeCheated = 0;

    public override void Initialize(Item item, bool isUpgrade = false, Rarity upgradeRarity = Rarity.COMMON) {
        base.Initialize(item, isUpgrade, upgradeRarity);
        item.owner.health.GainMaxLifeOnly(addedMaxHeart);
        if (shouldGainFullHearts) {
            item.owner.health.GainLife(addedMaxHeart - lifeCheated);
        }
    }

    public override void OnUnEquip() {
        if (shouldGainFullHearts) {
            int lifeToLose = Mathf.Min(addedMaxHeart - lifeCheated, item.owner.health.CurrentLife() - 1);
            lifeCheated += (addedMaxHeart - lifeCheated) - lifeToLose;
            item.owner.health.LoseLife(lifeToLose, ignoreInvulnerability: true);
        }
        item.owner.health.LoseMaxLifeOnly(addedMaxHeart);
        base.OnUnEquip();
    }

    public override string Description() {
        string s = addedMaxHeart > 1 ? "s" : "";
        int lifeRemaining = addedMaxHeart - lifeCheated;
        string lifeS = lifeRemaining > 0 ? "s" : "";
        string restante = shouldGainFullHearts && lifeCheated > 0 ? $" ({lifeRemaining} restante{lifeS})" : "";
        string pleines = shouldGainFullHearts ? $"pleine{s}" : $"vide{s}";
        return $"{addedMaxHeart} vie{s} {pleines} supplémentaire{s}{restante}";
    }
    public override string DescriptionOneWord()
    {
        return shouldGainFullHearts ? "de vie" : "de vitalité";
    }
}