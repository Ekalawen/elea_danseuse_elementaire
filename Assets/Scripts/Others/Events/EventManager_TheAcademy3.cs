﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;

public class EventManager_TheAcademy3 : EventManager {

    [Header("Phase 1 : Catch the sword")]

    [Header("Phase 2 : FireWisps spawning")]
    public int nbEnemiesInPhase2 = 50;
    public AutomaticStory phase2Story;
    public Spawner phase2Enemies;

    [Header("Phase 3 : first Sanctuary")]
    public AutomaticStory phase3Story;
    public Spawner phase3Enemies;

    [Header("Phase 4 : 2 more Sanctuaries")]
    public AutomaticStory phase4Story;
    public Spawner phase4Enemies;

    protected int startPhase3KillCount;

    public override void Initialize() {
        base.Initialize();
        GoToPhase1();
        onNothingLeft.AddListener(GoToPhase2);
    }

    public override void StartPhase2() {
        phase2Story.InitializeAndTryTrigger();
        gm.enemyManager.AddSpawner(phase2Enemies);
        gm.enemyManager.onEnemyDie.AddListener(CheckNumberOfKill);
    }

    protected void CheckNumberOfKill(Enemy enemy) {
        if(gm.enemyManager.GetKillCount() == nbEnemiesInPhase2) {
            gm.enemyManager.RemoveSpawner(phase2Enemies);
            if (gm.enemyManager.enemies.Count <= 1) {
                GoToPhase3();
            } else {
                onNothingLeft.AddListener(GoToPhase3);
            }
        }
    }

    public override void StartPhase3() {
        phase3Story.InitializeAndTryTrigger();
        gm.enemyManager.AddSpawner(phase3Enemies);
        onNothingLeft.AddListener(GoToPhase4);
    }

    public override void StartPhase4() {
        phase4Story.InitializeAndTryTrigger();
        gm.enemyManager.RemoveSpawner(phase3Enemies);
        gm.enemyManager.AddSpawner(phase4Enemies);
        onNothingLeft.AddListener(gm.objectManager.OpenExit);
    }
}