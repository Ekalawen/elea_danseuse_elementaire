﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public abstract class AttackModifier : MonoBehaviour {

    protected Attack attack;
    [HideInInspector]
    public bool isUpgrade;
    [HideInInspector]
    public Rarity upgradeRarity;

    public virtual void Initialize(Attack attack, bool isUpgrade = false, Rarity upgradeRarity = Rarity.COMMON) {
        this.attack = attack;
        this.isUpgrade = isUpgrade;
        if (isUpgrade) {
            attack.AddWithoutInitializeModifier(this);
            this.upgradeRarity = upgradeRarity;
        }
    }

    public virtual bool CanTrigger() {
        return true;
    }

    public abstract string Description();

    public abstract string DescriptionOneWord();

    public virtual void UnEquip() {
    }
}