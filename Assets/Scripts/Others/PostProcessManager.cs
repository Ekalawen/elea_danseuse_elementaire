using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;
using UnityEngine.VFX;
//using UnityEngine.Rendering.PostProcessing;

public class PostProcessManager : MonoBehaviour {

    public Transform vfxFolder;
    public float hasTakenHitColorDuration = 0.5f;
    public Color hasTakenHitColor;
    public Color invulnerabilityColor;
    public GameObject onDeathVfxPrefab;
    public GameObject spawningVfxPrefab;
    public GameObject attackLinePrefab;

    protected GameManager gm;

    public void Initialize() {
        gm = GameManager.Instance;
    }

    public void InstantiateVfxSpawning(Vector2 pos) {
        VisualEffect vfx = Instantiate(
            spawningVfxPrefab,
            position: pos,
            rotation: Quaternion.identity,
            parent: vfxFolder).GetComponent<VisualEffect>();
        Destroy(vfx.gameObject, 2.0f);
    }
}
