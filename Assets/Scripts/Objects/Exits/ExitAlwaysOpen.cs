using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.ResourceManagement.AsyncOperations;

public class ExitAlwaysOpen : Exit {

    public override void Initialize(Node startingNode) {
        base.Initialize(startingNode);
        OpenSprite();
    }

    public override bool CanBePickedUpBy(Character character) {
        return true;
    }
}
