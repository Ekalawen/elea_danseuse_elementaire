﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Health))]
public abstract class HealthModifier : MonoBehaviour {

    protected Health health;

    public virtual void Initialize(Health health) {
        this.health = health;
    }
}