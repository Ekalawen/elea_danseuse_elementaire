﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;

public class ItemModifier_AdditionnalArmor : ItemModifier {

    public int addedArmor = 1;

    protected int armorCheated = 0;

    public override void Initialize(Item item, bool isUpgrade = false, Rarity upgradeRarity = Rarity.COMMON) {
        base.Initialize(item, isUpgrade, upgradeRarity);
        item.owner.health.GainArmor(addedArmor - armorCheated);
    }

    public override void OnUnEquip() {
        int armorToLose = Mathf.Min(addedArmor - armorCheated, item.owner.health.CurrentArmor());
        armorCheated += (addedArmor - armorCheated) - armorToLose;
        item.owner.health.LoseArmor(armorToLose, ignoreInvulnerability: true);
        base.OnUnEquip();
    }

    public override string Description() {
        string s = addedArmor > 1 ? "s" : "";
        int armureRestante = addedArmor - armorCheated;
        string armureS = armureRestante > 0 ? "s" : "";
        string restante = armorCheated > 0 ? $" ({armureRestante} restante{armureS})" : "";
        return $"{addedArmor} armure{s} supplémentaire{s}{restante}";
    }
    public override string DescriptionOneWord() {
        return "de protection";
    }
}