using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.UI;

public class ItemGiver : Object {

    public bool useUpgrades = true;
    public bool replaceWithOldItem = true;
    public Transform gemsParent;
    public GameObject gemPrefab;
    public List<Material> gemRarityMaterials;
    public List<Material> spriteOutlineRarityMaterials;
    public List<GameObject> itemPrefab;

    protected Item chosenItem;
    protected List<Image> gems;

    public override void Initialize(Node startingNode) {
        base.Initialize(startingNode);
        gems = new List<Image>();
        ChoseItem();
        DisplayChosenItem();
    }

    protected void ChoseItem() {
        GameObject itemPrefab = MathTools.ChoseOne(this.itemPrefab);
        chosenItem = Instantiate(itemPrefab, parent: transform).GetComponent<Item>();
        AddUpgradesToChosenItem();
    }

    protected void AddUpgradesToChosenItem() {
        if (!useUpgrades) {
            return;
        }
        if (chosenItem.IsWeapon()) {
            objectManager.itemRarityManager.AddUpgradesToWeapon(chosenItem.GetComponent<Weapon>());
        } else {
            objectManager.itemRarityManager.AddUpgradesToItem(chosenItem);
        }
    }

    private void DisplayChosenItem() {
        spriteRenderer.sprite = chosenItem.sprite;
        spriteRenderer.material = RarityHelper.RarityToList(spriteOutlineRarityMaterials, chosenItem.Rarity());
        foreach (Image gem in gems) {
            Destroy(gem.gameObject);
        }
        gems.Clear();
        foreach (ItemUpgrade upgrade in chosenItem.GetAllUpgrades()) {
            Image gem = Instantiate(gemPrefab, parent: gemsParent).GetComponent<Image>();
            gem.sprite = upgrade.gemSprite;
            gem.material = RarityHelper.RarityToList(gemRarityMaterials, upgrade.rarity);
            gems.Add(gem);
        }
    }

    protected override void PickUpBySpecific(Socle objectSocle, Character character) {
        Player player = (Player)character;
        Item oldItem = player.inventory.GetItemOfType(chosenItem.itemType);
        if (player != null && replaceWithOldItem && oldItem != null) {
            oldItem.transform.SetParent(transform);
            player.Equip(chosenItem);
            chosenItem = oldItem;
            DisplayChosenItem();
            DontDestroyAfterPickup();
        } else {
            player.Equip(chosenItem);
        }
    }
}
