using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.VFX;
using UnityEngine.Events;

public class PlayerRenderer : CharacterRenderer {

    public List<ElementSlider> elementSliders;

    public override void Initialize(Character character) {
        base.Initialize(character);
        InitializeElementSliders();
    }

    protected void InitializeElementSliders() {
        foreach(ElementSlider slider in elementSliders) {
            slider.Initialize();
        }
    }
}
