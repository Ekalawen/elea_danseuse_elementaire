﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public class ProjectileModifier_DisarmOnExplode : ProjectileModifier {

    public float duration = 3.0f;
    public float rangeMultiplierValue = 0.0f;

    public override void Initialize(Projectile projectile) {
        base.Initialize(projectile);
        projectile.damage.onHitOrKill.AddListener(Disarm);
    }

    protected void Disarm(Character character) {
        Player player = character.GetComponent<Player>();
        if(player == null) {
            return;
        }
        player.GetAttackRange().range.rangeModifier.AddModifier(FloatModifier.Mode.MULTIPLY, FloatModifier.Period.DURATION, rangeMultiplierValue, duration);
    }
}
