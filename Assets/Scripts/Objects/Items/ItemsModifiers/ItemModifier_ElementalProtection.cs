﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;

public class ItemModifier_ElementalProtection : ItemModifier {

    public Element element;
    public float protectionCoef = 1.0f;

    protected FloatModifier protectionModifier;

    public override void Initialize(Item item, bool isUpgrade = false, Rarity upgradeRarity = Rarity.COMMON) {
        base.Initialize(item, isUpgrade, upgradeRarity);
        FloatModifierManager elementModifier = item.owner.elementProtection.GetModifierFor(element);
        protectionModifier = elementModifier.AddModifier(FloatModifier.Mode.ADD, FloatModifier.Period.INFINITE_DURATION, protectionCoef);
    }

    public override void OnUnEquip() {
        FloatModifierManager elementModifier = item.owner.elementProtection.GetModifierFor(element);
        elementModifier.RemoveModifier(protectionModifier);
        base.OnUnEquip();
    }

    public override string Description() {
        string enMoins = protectionCoef >= 0 ? "" : "en moins ";
        string elem = ElementHelper.ToString(element, withPrefix: true);
        return $"{protectionCoef * 100}% de protection {enMoins}contre {elem}";
    }

    public override string DescriptionOneWord() {
        switch (element) {
            case Element.FIRE: return "incandescente";
            case Element.WATER: return "acqueuse";
            default: return "";
        }
        //return $"anti-{ElementHelper.ToString(element)}";
    }
}