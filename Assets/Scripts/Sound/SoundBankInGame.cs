using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SoundBankIngame", menuName = "SoundBankIngame")]
public class SoundBankIngame : ScriptableObject
{
    [Header("Main Sounds")]
    public AudioClipParams firstClips;
}
