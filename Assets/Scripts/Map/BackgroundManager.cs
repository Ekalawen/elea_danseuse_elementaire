using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Events;
using UnityEngine.Tilemaps;

public class BackgroundManager : MonoBehaviour {

    public enum TileType { GRASS, PAVEMENT, INTERSECTION };

    public float borderSize = 10.0f;
    public float tilemapOffset = 0.25f;
    public int nodeDistance = 1;
    public int nbCellByEdgeUnity = 2;
    public float propsProbaOutideGraph = 0.05f;
    public float propsOffsetOfGraph = 2.0f;
    public Grid grid;
    public Tilemap floorTilemap;
    public Tilemap propsTilemap;
    public List<TileBase> grassTiles;
    public List<TileBase> pavementTiles;
    public List<TileBase> intersectionTiles;
    public List<TileBase> propsTiles;

    protected GameManager gm;
    protected Graph graph;
    protected TileType[,] tilesTypes;

    public void Initialize() {
        gm = GameManager.Instance;
        graph = gm.graph;
        InitTilemapsSize();
        PrecomputeTilesType();
        FillBackground();
    }

    protected void InitTilemapsSize() {
        Bounds graphBounds = graph.BoundingBox();
        graphBounds.Expand(borderSize * 2); // Once for left, once for right
        float offset = -borderSize - tilemapOffset;

        floorTilemap.size = new Vector3Int((int)graphBounds.size.x, (int)graphBounds.size.y, 0) * 2; // * 2 because the cell are twice as little as nodes spaces
        floorTilemap.transform.position = new Vector3(offset, offset, 0);
        floorTilemap.origin = Vector3Int.zero;

        propsTilemap.size = new Vector3Int((int)graphBounds.size.x, (int)graphBounds.size.y, 0) * 2;
        propsTilemap.transform.position = new Vector3(offset, offset, 0);
        propsTilemap.origin = Vector3Int.zero;
    }

    protected void PrecomputeTilesType() {
        tilesTypes = new TileType[floorTilemap.cellBounds.size.x, floorTilemap.cellBounds.size.y]; // Default is TileType.GRASS
        SetPavementsToEdges();
        SetIntersectionsToNodes();
        SetSomePropsOutsideGraph();
    }

    protected void SetSomePropsOutsideGraph() {
        Bounds graphBounds = graph.BoundingBox();
        graphBounds.Expand(propsOffsetOfGraph * 2); // Once for left, once for right
        for(int x = 0; x < propsTilemap.size.x; x++) {
            for(int y = 0; y < propsTilemap.size.y; y++) {
                Vector3Int tilePos = new Vector3Int(x, y);
                Vector3 worldPos = propsTilemap.CellToWorld(tilePos);
                //Debug.Log($"worldPos = {worldPos} graphBounds = {graphBounds} contains = {graphBounds.Contains(worldPos)}");
                if(!graphBounds.Contains(worldPos)) {
                    if(UnityEngine.Random.Range(0.0f, 1.0f) <= propsProbaOutideGraph) {
                        propsTilemap.SetTile(tilePos, MathTools.ChoseOne(propsTiles));
                    }
                }
            }
        }
    }

    protected void SetPavementsToEdges() {
        foreach (Edge edge in graph.edges) {
            float edgeMagnitude = edge.Magnitude();
            int nbPoints = (int)(edgeMagnitude * nbCellByEdgeUnity);
            Vector2 increment = edge.Vector() / (nbPoints - 1);
            Vector2 currentPoint = edge.Start().pos;
            for(int i = 0; i < nbPoints; i++) {
                Vector3Int cellPos = floorTilemap.WorldToCell(currentPoint);
                tilesTypes[cellPos.x, cellPos.y] = TileType.PAVEMENT;
                currentPoint += increment;
            }
        }
    }

    protected void SetIntersectionsToNodes() {
        foreach (Node node in graph.nodes) {
            Vector3Int cellPos = floorTilemap.WorldToCell(node.pos);
            for (int x = -nodeDistance; x <= nodeDistance; x++) {
                for (int y = -nodeDistance; y <= nodeDistance; y++) {
                    if (Mathf.Abs(x) + Mathf.Abs(y) <= nodeDistance) {
                        tilesTypes[cellPos.x + x, cellPos.y + y] = TileType.INTERSECTION;
                    }
                }
            }
        }
    }

    protected void FillBackground() {
        for(int x = 0; x < floorTilemap.cellBounds.xMax; x++) {
            for(int y = 0; y < floorTilemap.cellBounds.yMax; y++) {
                Vector3Int tilePos = new Vector3Int(x, y, 0);
                SetTileFor(tilePos);
            }
        }
    }

    protected void SetTileFor(Vector3Int pos) {
        TileType tileType = tilesTypes[pos.x, pos.y];
        if (tileType == TileType.PAVEMENT) {
            floorTilemap.SetTile(pos, MathTools.ChoseOne(pavementTiles));
        } else if (tileType == TileType.INTERSECTION) {
            floorTilemap.SetTile(pos, MathTools.ChoseOne(grassTiles));
            propsTilemap.SetTile(pos, MathTools.ChoseOne(intersectionTiles));
        } else {
            floorTilemap.SetTile(pos, MathTools.ChoseOne(grassTiles));
        }
    }

    protected TileBase GetTileFor(Vector2 pos) {
        if(IsCloseToNode(pos)) {
            return MathTools.ChoseOne(pavementTiles);
        }
        return MathTools.ChoseOne(grassTiles);
    }

    protected bool IsCloseToNode(Vector2 pos) {
        return graph.nodes.Any(n => Vector2.SqrMagnitude(n.pos - pos) <= 1.0f);
    }
}
