﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;

public abstract class CharacterController : MonoBehaviour {

    protected Character character;
    protected GameManager gm;
    protected Graph graph;
    protected Timer waitingTimer;

    public virtual void Initialize(Character character) {
        gm = GameManager.Instance;
        graph = gm.graph;
        waitingTimer = new Timer(setOver: true);
        this.character = character;
    }

    protected void Update() {
        if(!waitingTimer.IsOver()) {
            return;
        }
        if (gm.IsPaused()) {
            return;
        }
        UpdateMouvement();
    }

    protected abstract void UpdateMouvement();

    protected bool MoveInDirection(Vector2 normal) {
        Assert.IsTrue(MathTools.normals2D.Contains(normal));
        Node nextNode = character.CurrentNode().GetVoisinForDirection(normal);
        if (nextNode != null) {
            character.MoveTo(nextNode);
            return true;
        }
        return false;
    }

    public void Wait(float seconds) {
        if(waitingTimer.IsOver()) {
            waitingTimer = new Timer(seconds);
        } else {
            waitingTimer.AddDuree(seconds);
        }
    }
}
