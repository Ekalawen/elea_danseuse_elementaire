using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.VFX;

public class AutomaticStoryOnObject : AutomaticStory {

    public override void Initialize() {
        base.Initialize();
        gm.objectManager.onCatchObject.AddListener(OnCatchObject);
    }

    protected void OnCatchObject(Object obj) {
        TryTrigger();
    }
}
