using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.ResourceManagement.AsyncOperations;

public class ExitNothingLeft : Exit {

    public bool notOnFirstTime = false;
    [ConditionalHide("notOnFirstTime")]
    public int onNTimes = 1;

    protected int nbTimes = 0;
    protected SingleCoroutine singleCoroutine;

    public override void Initialize(Node startingNode) {
        base.Initialize(startingNode);
        singleCoroutine = new SingleCoroutine(this);
        gm.objectManager.onCatchObject.AddListener(OnCatchObject);
        gm.enemyManager.onEnemyDie.AddListener(WhenEnemyDie);
    }

    protected void WhenEnemyDie(Enemy enemy) {
        singleCoroutine.Start(COnSomethingLeft());
    }

    protected void OnCatchObject(Object obj) {
        singleCoroutine.Start(COnSomethingLeft());
    }

    protected IEnumerator COnSomethingLeft() {
        yield return null; // Wait 1 frame for the thing to be destroyed/removed
        if(gm.objectManager.objects.Count <= 1  // <= 1 because of the exit itself ^^'
        && gm.enemyManager.enemies.Count == 0) {
            TryOpenSprite();
        }
    }

    protected void TryOpenSprite() {
        nbTimes++;
        if(notOnFirstTime && nbTimes < onNTimes) {
            return;
        }
        OpenSprite();
    }
}
