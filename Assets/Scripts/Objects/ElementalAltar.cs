using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.VFX;

public class ElementalAltar : Object {

    protected const string VFX_STOP = "Stop";
    protected const string VFX_START = "Start";
    protected const string VFX_SPAWN_RATE = "SpawnRate";

    public enum Phase { ACTIVE, DORMANT };

    public ElementHolder element;
    public float effectDuration = 10.0f;
    public float protectionPercentage = 1.0f;
    public float dormantDuration = 10.0f;
    public VisualEffect whileActivateVfx;
    public VisualEffect activationVfx;
    public float protectionDecreasePrevisualisiationDuration = 1.0f;
    public GameObject protectionAuraVfxPrefab;

    protected Phase phase;
    protected GameObject currentProtectionAuraVfxHolder;

    public override void Initialize(Node startingNode) {
        base.Initialize(startingNode);
        GoToActivePhase();
    }

    public override bool CanBePickedUpBy(Character character) {
        return phase == Phase.ACTIVE;
    }

    protected override void PickUpBySpecific(Socle objectSocle, Character character) {
        Player player = character.GetComponent<Player>();
        if(player == null) {
            DontDestroyAfterPickup();
            return;
        }
        gm.enemyManager.elementManager.ClearElementCountFor(player, element.elem);
        FloatModifierManager modifier = player.elementProtection.GetModifierFor(element.elem);
        modifier.AddModifier(FloatModifier.Mode.ADD, FloatModifier.Period.DURATION, protectionPercentage, effectDuration);
        AddProtectionAuraTo(player);
        GoToDormantPhase();
        DontDestroyAfterPickup();
        activationVfx.SendEvent(VFX_START);
        gm.objectManager.onActivateElementalAltar.Invoke(this);
    }

    protected void AddProtectionAuraTo(Player player) {
        currentProtectionAuraVfxHolder = Instantiate(protectionAuraVfxPrefab, parent: player.transform);
        StartCoroutine(CDestroyProtectionAuraIn(currentProtectionAuraVfxHolder, effectDuration));
        gm.objectManager.onReachExit.AddListener(DestroyCurrentProtectionAuraVfx);
    }

    protected void DestroyCurrentProtectionAuraVfx() {
        if (currentProtectionAuraVfxHolder == null) {
            return;
        }
        Destroy(currentProtectionAuraVfxHolder);
    }

    protected IEnumerator CDestroyProtectionAuraIn(GameObject vfxHolder, float duration) {
        List<VisualEffect> vfxs = vfxHolder.GetComponentsInChildren<VisualEffect>().ToList();
        yield return new WaitForSeconds(duration - protectionDecreasePrevisualisiationDuration);
        foreach(VisualEffect vfx in vfxs) {
            vfx.SetFloat(VFX_SPAWN_RATE, vfx.GetFloat(VFX_SPAWN_RATE) / 2);
        }
        yield return new WaitForSeconds(protectionDecreasePrevisualisiationDuration);
        foreach(VisualEffect vfx in vfxs) {
            vfx.Stop();
        }
        yield return new WaitForSeconds(2.0f);
        Destroy(vfxHolder);
    }

    protected void GoToDormantPhase() {
        phase = Phase.DORMANT;
        StartCoroutine(CSetActiveIn(dormantDuration));
        whileActivateVfx.SendEvent(VFX_STOP);
        spriteRenderer.color = ColorsTools.SetAlpha(spriteRenderer.color, 0.5f);
    }

    protected IEnumerator CSetActiveIn(float duration) {
        yield return new WaitForSeconds(duration);
        GoToActivePhase();
    }

    protected void GoToActivePhase() {
        phase = Phase.ACTIVE;
        whileActivateVfx.SendEvent(VFX_START);
        spriteRenderer.color = ColorsTools.SetAlpha(spriteRenderer.color, 1.0f);
        CheckCollisionWithPlayer();
    }


    protected void CheckCollisionWithPlayer() {
        Player player = socle.CurrentSocleCollisionOf<Player>().FirstOrDefault();
        if(player == null) {
            return;
        }
        PickUpBySpecific(socle, player);
    }
}
