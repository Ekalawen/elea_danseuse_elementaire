using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;

public abstract class CharacterModifier : MonoBehaviour {

    protected Character owner;

    public virtual void Initialize(Character owner) {
        this.owner = owner;
    }
}
