using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class AZone : ANode {

    public AZone() : base() {
    }

    public AZone(Node node, ANode previous) : base(node, previous, null) {
    }

    public override IList<INode> FailResult(IList<INode> closed) {
        return closed;
    }

    public override IList<INode> Backtrack() {
        return null;
    }

    public override IList<INode> Voisins(INode target) {
        return node.Voisins().Select(v => new AZone(v, this)).Cast<INode>().ToList();
    }

    public override string ToString() {
        return node?.ToString() ?? "NullNode";
    }

    public virtual float Cost(INode previousNode) {
        return previousNode.cost + 1;
    }
    public virtual float RemainingEstimate(INode target) {
        return 0;
    }
    public virtual float Heuristic() {
        return 0;
    }
}
