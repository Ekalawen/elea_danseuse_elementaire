using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public interface INode {

    public Node node { get; set; }
    public INode previous { get; set; }
    public float cost { get; set; }
    public float remainingEstimate { get; set; }
    public float heuristic { get; set; }

    public abstract void Initialize(Node node, INode previous, INode target);

    public abstract float Cost(INode previousNode);

    public abstract float RemainingEstimate(INode target);

    public abstract float Heuristic();

    public abstract IList<INode> Voisins(INode target);

    public abstract IList<INode> FailResult(IList<INode> closed);

    public abstract IList<INode> Backtrack();
}
