﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;

public class Damage : MonoBehaviour {

    public int damages = 1;
    public bool ignoreInvulnerability = false;
    public float stunDuration = 0.0f;
    public bool pushAway = true;
    [ConditionalHide("pushAway")]
    public float pushDistance = 3.0f;
    [ConditionalHide("pushAway")]
    public float pushSpeed = 12.0f;
    [ConditionalHide("pushAway")]
    public bool alwaysPushAndStun = false;

    [HideInInspector]
    public UnityEvent<Character> onHit = new UnityEvent<Character>(); // Who we are hitting
    [HideInInspector]
    public UnityEvent<Character> onKill = new UnityEvent<Character>(); // Who we are killing
    [HideInInspector]
    public UnityEvent<Character> onHitOrKill = new UnityEvent<Character>(); // Who we are hitting/killing

    protected GameManager gm;
    protected Character owner;

    public virtual void Initialize(Character owner) {
        gm = GameManager.Instance;
        this.owner = owner;
    }

    public virtual void Hit(Character target) {
        bool willKill = target.health.WillKill(damages, ignoreInvulnerability);
        if (willKill) {
            onKill.Invoke(target);
            target.onKilledBy.Invoke(target, owner);
        } else {
            onHit.Invoke(target);
        }
        onHitOrKill.Invoke(target);
        target.onHitBy.Invoke(target, owner, this);
        if (!ApplyDamages(target)) {
            if (!alwaysPushAndStun) {
                return;
            }
        } else {
            target.onHitByAndDamaged.Invoke(target, owner, this);
        }
        ApplyPush(target);
        ApplyStun(target);
    }

    public virtual bool ApplyDamages(Character target) {
        return target.health.LoseLife(damages, ignoreInvulnerability: ignoreInvulnerability);
    }

    public virtual void ApplyStun(Character target) {
        // TODO
    }

    public virtual void ApplyPush(Character target) {
        if(!pushAway) {
            return;
        }
        target.movement.SetSpeedForOneMove(pushSpeed);
        Vector2 ownerPos = owner != null ? owner.pos : transform.position;
        Vector2 vector = (target.pos - ownerPos).normalized * pushDistance;
        Node targetClosestNode = target.ClosestNode();
        Node pushedToNode = gm.graph.ClosestToNotIn(target.pos + vector, targetClosestNode);
        target.MoveTo(pushedToNode);
    }
}