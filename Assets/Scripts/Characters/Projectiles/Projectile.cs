﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.VFX;

public class Projectile : MonoBehaviour {
    public enum Mode { ON_EXPLODE, ON_ENTER };

    public static string VFX_START = "Start";
    public static string VFX_STOP = "Stop";
    public static string VFX_RANGE = "Range";


    public Mode mode = Mode.ON_EXPLODE;
    public Faction faction = Faction.ENEMY;
    public Socle socle;
    public float speed = 1.0f;
    public float duration = 1.0f;
    public Damage damage;
    public VisualEffect previsualisationVfx;
    public VisualEffect explosionVfx;

    public Vector2 pos {
        get { return new Vector2(transform.position.x, transform.position.y); }
        set { transform.position = value; }
    }

    protected GameManager gm;
    protected Vector2 direction;
    protected Character owner;
    protected List<ProjectileModifier> modifiers;

    public virtual void Initialize(Character owner, Vector2 pos, Vector2 direction) {
        gm = GameManager.Instance;
        this.owner = owner;
        this.pos = pos;
        socle.Initialize();
        this.direction = direction == Vector2.zero ? Vector2.zero : direction.normalized;
        InitializeVfx();
        damage.Initialize(owner);
        StartCoroutine(CStartMoving());
        gm.enemyManager.RegisterProjectile(this);
        if (mode == Mode.ON_ENTER) {
            socle.onEnterOppositeFaction.AddListener(ApplyDamageTo);
        }
        InitializeModifiers();
    }

    protected void InitializeModifiers() {
        modifiers = GetComponentsInChildren<ProjectileModifier>().ToList();
        foreach(ProjectileModifier modifier in modifiers) {
            modifier.Initialize(this);
        }
    }

    private void OnDestroy() {
        gm.enemyManager.UnregisterProjectile(this);
        //vfx.SendEvent(VFX_STOP);
        //Destroy(vfx.gameObject, 1.0f);
        Destroy(previsualisationVfx.gameObject);
    }

    protected IEnumerator CStartMoving() {
        Timer timer = new Timer(duration);
        while (!timer.IsOver()) {
            MoveOneFrame();
            yield return null;
        }
        if (mode == Mode.ON_EXPLODE) {
            Explode();
        } else {
            Destroy(gameObject);
        }
    }

    protected void Explode()
    {
        List<Character> targets = socle.CurrentSocleCollisionOf<Character>().FindAll(c => FactionHelper.OppositeFactions(this, c));
        foreach (Character target in targets)
        {
            DamageTarget(target);
        }
        StartExplosionVfx();
        Destroy(gameObject);
    }

    protected void StartExplosionVfx()
    {
        explosionVfx.SendEvent(VFX_START);
        explosionVfx.transform.SetParent(gm.postProcessManager.vfxFolder);
        Destroy(explosionVfx.gameObject, 1.0f);
    }

    protected void MoveOneFrame() {
        if (direction == Vector2.zero) {
            return;
        }
        Vector2 movement = direction * speed * Time.deltaTime;
        pos += movement;
    }

    protected void InitializeVfx() {
        float socleRange = socle.Get();
        previsualisationVfx.transform.SetParent(gm.postProcessManager.vfxFolder);
        previsualisationVfx.SetFloat(VFX_RANGE, socleRange);
        previsualisationVfx.SendEvent(VFX_START);
        explosionVfx.SetFloat(VFX_RANGE, socleRange);
    }

    protected void ApplyDamageTo(Socle thisSocle, Character target, Range targetRange) {
        if(!targetRange.IsSocle()) {
            return;
        }
        DamageTarget(target);
    }

    protected void DamageTarget(Character target) {
        damage.Hit(target);
    }
}
