NARRATOR Lorsque vous brisez la dernière jarre, vous entendez un long sifflement.
NARRATOR Comme si le fluide que la jarre contenait prenait vie.
FIREWISP Sautille sur place avec joie et contentement :D
NARRATOR Vous regardez ce qui semble être un élémentaire de feu. Cela vous intrigue.
FIREWISP Saute un peu partout, mettant le feu aux débris.
NARRATOR Vous réalisez que si vous ne faites rien, vous aurez bientôt à faire à un incendie.
FIREWISP Le feu se propage. Les élémentaires se multiplient.
FIREWISP Et certains tentent de sortir des Archives.
NARRATOR Vous vous jetez sur les élémentaires pour les arrêter !