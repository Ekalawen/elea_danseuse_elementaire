﻿using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;

public class PlayerControllerDirection {
    public KeyCode keycode;
    public Vector2 direction;
    public Timer timer;

    public PlayerControllerDirection(KeyCode keyCode, Vector2 direction) {
        this.keycode = keyCode;
        this.direction = direction;
        this.timer = new Timer();
    }

    public bool IsPressed() {
        return Input.GetKey(keycode);
    }
}

