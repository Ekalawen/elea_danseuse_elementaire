﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;

public class RarityHelper {

    public static string RarityString(Rarity rarity) {
        switch (rarity) {
            case Rarity.COMMON:
                return "Commun";
            case Rarity.RARE:
                return "Rare";
            case Rarity.LEGENDARY:
                return "LÉGENDAIRE !";
            default:
                return "";
        }
    }

    public static Rarity BestRarity(Rarity r1, Rarity r2) {
        if(r1 == Rarity.LEGENDARY || r2 == Rarity.LEGENDARY) {
            return Rarity.LEGENDARY;
        }
        if(r1 == Rarity.RARE || r2 == Rarity.RARE) {
            return Rarity.RARE;
        }
        return Rarity.COMMON;
    }

    public static T RarityToList<T>(List<T> list, Rarity rarity) {
        Assert.IsTrue(list.Count == 3);
        switch (rarity)
        {
            case Rarity.COMMON: return list[0];
            case Rarity.RARE: return list[1];
            case Rarity.LEGENDARY: return list[2];
            default: throw new Exception($"Cette rareté {rarity} n'est pas référencé dans RarityToList !");
        }
    }


    public static int CompareTo(Rarity r1, Rarity r2) {
        if(r1 == r2) {
            return 0;
        }
        return r1 > r2 ? 1 : -1;
    }
}