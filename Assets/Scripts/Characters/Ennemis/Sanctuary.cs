﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public class Sanctuary : Enemy {

    protected static string VFX_START = "Start";
    protected static string VFX_RANGE = "Range";

    [Header("Sanctuary")]
    public Range triggerRange;
    public float timeToReduceRange = 0.8f;
    public VisualEffect vfxActivated;
    public List<Spawner> defaultSpawners;
    public List<Spawner> triggerSpawners;
    public bool shouldDropItem = true;

    protected bool hasBeenTriggered = false;

    public override void Initialize(Node startNode) {
        base.Initialize(startNode);
        AddDefaultSpawners();
        triggerRange.Initialize();
        triggerRange.onEnterSocle.AddListener(TryTrigger);
        onHitByAndDamaged.AddListener(PushAwayOnBeingHit);
        gm.objectManager.GetExit().RegisterSanctuary(this);
    }

    protected void AddDefaultSpawners() {
        gm.enemyManager.AddSpawners(defaultSpawners);
    }

    protected void TryTrigger(Range triggerRange, Socle socle) {
        if(socle.faction != Faction.PLAYER) {
            return;
        }
        if(socle.GetComponentInParent<Character>() == null) {
            return;
        }
        if(hasBeenTriggered) {
            return;
        }
        Trigger();
    }

    protected void Trigger()
    {
        hasBeenTriggered = true;
        StartActivatedVfx();
        triggerRange.ReduceAndDestroyIn(timeToReduceRange);
        gm.enemyManager.RemoveSpawners(defaultSpawners);
        gm.enemyManager.AddSpawners(triggerSpawners);
    }

    protected void StartActivatedVfx()
    {
        vfxActivated.SetFloat(VFX_RANGE, triggerRange.Get());
        vfxActivated.SendEvent(VFX_START);
    }

    public override void Die() {
        base.Die();
        gm.enemyManager.AddSpawners(triggerSpawners);
        if (shouldDropItem) {
            gm.objectManager.DropItemTo(CurrentNode());
        }
    }

    protected void PushAwayOnBeingHit(Character sanctuary, Character player, Damage damageUsed) {
        meleeDamages.Hit(player);
    }
}
