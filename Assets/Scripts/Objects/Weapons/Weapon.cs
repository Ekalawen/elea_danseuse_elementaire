﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

public class Weapon : Item {

    public enum Type
    {
        BASIC_SWORD,
        LARGE_SWORD,
        LONG_SWORD,
        AXE,
        BOW,
        CROSSBOW,
        DAGGER,
        HALBERD,
        HAMMER,
        RAPIER,
        SPEAR,
        WHIP,
        CLAWS, // At the end because I forget it at first XD
    }

    public GameObject attackPrefab;
    public Type type;
    public List<WeaponUpgrade> weaponUpgrades;

    [HideInInspector]
    public Attack instantiatedAttack;
    [HideInInspector]
    public AttackRange instantiatedAttackRange;

    public override void Initialize(Player owner) {
        base.Initialize(owner);
        instantiatedAttack = Instantiate(attackPrefab, position: owner.pos, rotation: Quaternion.identity, parent: transform).GetComponent<Attack>();
        owner.EquipAttack(instantiatedAttack);
        foreach(WeaponUpgrade upgrade in weaponUpgrades) {
            upgrade.Initialize(this);
        }
        instantiatedAttackRange = instantiatedAttack.GetComponent<AttackRange>();
        instantiatedAttackRange.onHit.AddListener(owner.SufferElementOf);
        instantiatedAttackRange.onKill.AddListener(owner.SufferElementOf);
    }

    public override List<ItemUpgrade> GetAllUpgrades() {
        return upgrades.Concat(weaponUpgrades).ToList();
    }

    public override void OnUnEquip() {
        foreach(WeaponUpgrade weaponUpgrade in weaponUpgrades) {
            weaponUpgrade.OnUnEquip();
        }
        if (instantiatedAttack != null) {
            foreach (AttackModifier modifier in instantiatedAttack.GetModifiers().FindAll(m => !m.isUpgrade)) {
                modifier.UnEquip();
            }
        }
        base.OnUnEquip();
    }

    public override Rarity Rarity() {
        Rarity baseRarity = base.Rarity();
        List<Rarity> weaponsUpgradesRarity = weaponUpgrades.Select(wu => wu.rarity).ToList();
        weaponsUpgradesRarity.Add(baseRarity);
        return weaponsUpgradesRarity.Max();
    }
}