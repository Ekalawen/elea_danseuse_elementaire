﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public abstract class ProjectileModifier : MonoBehaviour {

    protected Projectile projectile;

    public virtual void Initialize(Projectile projectile) {
        this.projectile = projectile;
    }
}
