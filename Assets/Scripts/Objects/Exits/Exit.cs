using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.ResourceManagement.AsyncOperations;

public class Exit : Object {

    public Sprite openDoorSprite;
    public bool stayClosedUntilTrigger = false;

    protected List<Sanctuary> remainingSanctuaries;
    protected bool isOpen = false;
    protected Sprite closeDoorSprite;

    public override void Initialize(Node startingNode) {
        base.Initialize(startingNode);
        closeDoorSprite = spriteRenderer.sprite;
        remainingSanctuaries = gm.enemyManager.GetAllEnemyOfType<Sanctuary>();
        gm.enemyManager.onEnemyDie.AddListener(OnEnemyDie);
    }

    public override bool CanBePickedUpBy(Character character) {
        return isOpen;
    }

    protected override void PickUpBySpecific(Socle objectSocle, Character character) {
        Debug.Log($"YOU WIN !");
        objectManager.onReachExit.Invoke();
    }

    protected void OnEnemyDie(Enemy enemy) {
        if(stayClosedUntilTrigger) {
            return;
        }
        Sanctuary sanctuary = enemy.GetComponent<Sanctuary>();
        if(sanctuary == null) {
            return;
        }
        remainingSanctuaries.Remove(sanctuary);
        if(remainingSanctuaries.Count == 0) {
            OpenSprite();
        }
    }

    public void OpenSprite() {
        if(spriteRenderer == null) {
            return;
        }
        spriteRenderer.sprite = openDoorSprite;
        spriteRenderer.GetComponent<Animator>().enabled = true;
        isOpen = true;
    }

    public void CloseExit() {
        if(spriteRenderer == null) {
            return;
        }
        spriteRenderer.sprite = closeDoorSprite;
        spriteRenderer.GetComponent<Animator>().enabled = false;
        isOpen = false;
    }

    public void RegisterSanctuary(Sanctuary sanctuary) {
        if(remainingSanctuaries == null) {
            return;
        }
        remainingSanctuaries.Add(sanctuary);
    }
}
