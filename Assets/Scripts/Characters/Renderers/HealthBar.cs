using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthBar : MonoBehaviour {

    public GameObject heartPrefab;
    public Transform heartFolder;

    protected Health health;
    protected List<GameObject> hearts;

    public void Initialize(Health health) {
        this.health = health;
        hearts = new List<GameObject>();
        SetHealth();
        health.onChangeLife.AddListener(SetHealth);
    }

    protected void SetHealth(int newLife) {
        SetHealth();
    }

    protected void SetHealth() {
        DestroyHearts();
        PopulateHearts();
    }

    protected void PopulateHearts() {
        for(int i = 0; i < health.CurrentLife(); i++) {
            hearts.Add(Instantiate(heartPrefab, parent: heartFolder));
        }
    }

    protected void DestroyHearts() {
        foreach(GameObject heart in hearts) {
            Destroy(heart);
        }
        hearts.Clear();
    }
}
