using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class APath : ANode {

    public APath() : base() {
    }

    public APath(Node node, ANode previous, ANode target) : base(node, previous, target) {
    }

    public override IList<INode> Voisins(INode target) {
        return node.Voisins().Select(v => new APath(v, this, (ANode)target)).Cast<INode>().ToList();
    }

    public override IList<INode> FailResult(IList<INode> closed) {
        return new List<APath>().Cast<INode>().ToList();
    }

    public override IList<INode> Backtrack() {
        List<APath> path = new List<APath>();
        APath current = this;
        while (current != null) {
            path.Add(current);
            current = (APath)current.previous;
        }
        path.RemoveAt(path.Count - 1); // Don't include the starting node
        path.Reverse();
        return path.Cast<INode>().ToList();
    }
}
