using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {

    public TMP_Text killCounterText;
    public TMP_Text bookCounterText;
    public LifeCounter lifeCounter;
    public List<ItemUIDisplayer> itemDisplayers;
    public List<ElementSlider> sliders;

    [Header("StoryManager links")]
    public TMP_Text levelNameText;
    public Animator linesAnimator;
    public TMP_Text storyText;
    public Image storyImage;
    public Animator sceneTransitionAnimator;

    protected GameManager gm;
    protected int nbKills = 0;

    public void Initialize()
    {
        gm = GameManager.Instance;
        gm.player.onKill.AddListener(IncrementKillCounter);
        lifeCounter.Initialize();
        InitializeItemDisplayers();
        InitializeSliders();
        InitializeBookCounter();
    }

    protected void InitializeItemDisplayers() {
        foreach(ItemUIDisplayer displayer in itemDisplayers) {
            displayer.Initialize();
        }
    }

    protected void InitializeSliders() {
        foreach(ElementSlider slider in sliders) {
            slider.Initialize();
        }
    }

    public void IncrementKillCounter(Character character) {
        IncrementKillCounter();
    }

    public void IncrementKillCounter() {
        nbKills++;
        killCounterText.text = $"{nbKills}";
    }

    public void SetBookCounterTo(int value) {
        bookCounterText.text = $"{value}";
    }
    protected void InitializeBookCounter() {
        SetBookCounterTo(Book.GetBookCount());
    }
}
