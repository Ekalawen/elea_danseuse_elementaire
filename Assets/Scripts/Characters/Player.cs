using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class Player : Character {

    protected const string ANIMATOR_START_IDLE = "StartIdle";
    protected const string ANIMATOR_START_RUN = "StartRun";
    protected const string ANIMATOR_START_ATTACK = "StartAttack";

    public Animator animator;
    public Inventory inventory;
    public ElementProtection elementProtection;

    [HideInInspector]
    public UnityEvent<Character, Item> onEquipItem = new UnityEvent<Character, Item>();
    [HideInInspector]
    public UnityEvent<Character, Weapon> onEquipWeapon = new UnityEvent<Character, Weapon>();

    protected bool dontUseAttackAnimationNextTime = false;

    public override void Initialize(Node startNode)
    {
        int currentHearts = GameManager.Instance.IsPlayerLoaded() ? health.CurrentHearts() : 0;
        int currentArmor = GameManager.Instance.IsPlayerLoaded() ? health.CurrentArmor() : 0;
        base.Initialize(startNode);
        onReachNode.AddListener(StartAnimatorIdle);
        onStartMove.AddListener(StartAnimatorRun);
        elementProtection.Initialize(this);
        inventory.Initialize(this);
        RestoreOldLife(currentHearts, currentArmor);
        DestroyOnLoad();
    }

    private void RestoreOldLife(int currentHearts, int currentArmor) {
        if(!gm.IsPlayerLoaded()) {
            return;
        }
        health.SetCurrentHeartsAndArmorNoCallback(currentHearts, currentArmor);
    }

    protected void StartAnimatorRun(Node node) {
        animator.SetTrigger(ANIMATOR_START_RUN);
    }

    protected void StartAnimatorIdle(Node node) {
        animator.SetTrigger(ANIMATOR_START_IDLE);
    }

    public override bool Attack() {
        if (base.Attack()) {
            StartAttackAnimation();
            return true;
        }
        if (CanAttack()) {
            StartAttackAnimation();
        }
        return false;
    }

    protected void StartAttackAnimation() {
        if(dontUseAttackAnimationNextTime) {
            dontUseAttackAnimationNextTime = false;
            return;
        }
        animator.SetTrigger(ANIMATOR_START_ATTACK);
    }

    public void DontUseAttackAnimationNextTime() {
        dontUseAttackAnimationNextTime = true;
    }

    public void SufferElementOf(Character enemy) {
        if(!ElementHelper.SufferFrom(this, enemy)) {
            return;
        }
        gm.enemyManager.elementManager.SufferElementFrom(this, enemy.element);
    }

    public void DontDestroyOnLoad() {
        movingCoroutineSingleton.Stop();
        inventory.SetCurrentItemsToInitialItems();
        characterRenderer.SetOrientationToInitialValues();
        DontDestroyOnLoad(gameObject);
    }

    public void DestroyOnLoad() {
        SceneManager.MoveGameObjectToScene(gameObject, SceneManager.GetActiveScene());
    }

    public AttackRange GetAttackRange() {
        return attacks.First().GetComponent<AttackRange>();
    }

    public void Equip(Item item) {
        inventory.Equip(item);
    }
}
