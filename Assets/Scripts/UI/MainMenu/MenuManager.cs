using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;
using UnityEngine.UI;
using System.Linq;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour {

    public GameObject mainMenu;
    public GameObject levelsMenu;
    public TMP_Text optionButtonText;
    public Transform levelButtonsFolder;

    protected List<LevelButton> levelButtons;

    public void Start() {
        Initialize(); 
    }

    protected void Initialize() {
        GoToMainMenu();
        InitializeLevelButtons();
    }

    protected void InitializeLevelButtons() {
        levelButtons = levelButtonsFolder.GetComponentsInChildren<LevelButton>().ToList();
        foreach(LevelButton levelButton in levelButtons) {
            levelButton.Initialize();
        }
    }

    public void GoToMainMenu() {
        mainMenu.SetActive(true);
        levelsMenu.SetActive(false);
    }

    public void GoToLevelsMenu() {
        levelsMenu.SetActive(true);
        mainMenu.SetActive(false);
    }

    public void Play() {
        if (PrefsManager.GetBool(PrefsManager.HAS_ALREADY_PLAY_KEY, false)) {
            GoToLevelsMenu();
        } else {
            PrefsManager.SetBool(PrefsManager.HAS_ALREADY_PLAY_KEY, true);
            SceneManager.LoadScene(levelButtons.First().levelSceneName);
        }
    }

    public void Options() {
        List<string> cyclicOptionStrings = new List<string>() {
            "Param�tres",
            "T'as cru que c'�tait un vrai jeu ?",
            "Bah non :)",
            "Arr�te d'insister.",
            "�a suffit !",
            "J'ai dis stop !",
            "Non mais tu vas jouer au jeu oui ?",
            "Eh !",
            "Non mais je te jure les jeunes aujourd'hui.",
            "Ils croient qu'ils peuvent appuyer partout comme �a.",
            "Sans permission.",
            "Et que �a va d�ranger personne.",
            "Mais j'ai une vie moi !",
            "Des droits !",
            "Des enfants !",
            "Ah �a en fait non.",
            "Mais c'est pas grave.",
            "M�me si je ne suis qu'un bouton ...",
            "J'aimerais qu'on me respecte pour ce que je suis.",
            "Fin tu fais ce que tu veux.",
            "Mais j'appr�cirais quoi.",
            "Bon allez.",
            "Bon jeu � toi l'ami !",
            "<i>� Billy le bouton</i>",
        };
        int indice = cyclicOptionStrings.IndexOf(optionButtonText.text);
        int newIndice = (indice + 1) % cyclicOptionStrings.Count;
        optionButtonText.text = cyclicOptionStrings[newIndice];
    }

    public void Quitter() {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }
}
