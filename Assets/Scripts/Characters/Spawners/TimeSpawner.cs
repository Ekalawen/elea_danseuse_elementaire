﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeSpawner : Spawner {

    public float periode = 5.0f;

    protected Coroutine spawningCoroutine;

    public override void Initialize(SpawnerManager spawnerManager) {
        base.Initialize(spawnerManager);
        spawningCoroutine = StartCoroutine(PeriodicSpawning());
    }

    protected IEnumerator PeriodicSpawning() {
        while(true) {
            yield return new WaitForSeconds(periode);
            Spawn();
        }
    }

    public override void Stop() {
        base.Stop();
        if(spawningCoroutine != null) {
            StopCoroutine(spawningCoroutine);
        } 
    }
}
