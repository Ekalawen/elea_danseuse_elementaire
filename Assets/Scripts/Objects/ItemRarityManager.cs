using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.ResourceManagement.AsyncOperations;

public class ItemRarityManager : MonoBehaviour {

    [Tooltip("The weight of the number of upgrades for each indice. Starting from 0.")]
    public List<float> nbUpgradesWeightsForWeapon = new List<float>() { 12, 45, 25, 15, 1, 1, 1 };
    [Tooltip("The weight of the number of upgrades for each indice. Starting from 0.")]
    public List<float> nbUpgradesWeightsForAmulets = new List<float>() { 48, 45, 5, 1, 1 };
    [Tooltip("The weight of the number of upgrades for each indice. Starting from 0.")]
    public List<float> nbUpgradesWeightsForArmors = new List<float>() { 30, 25, 15, 10, 5, 1, 1 };
    [Tooltip("The weight of each rarity. In the common, rare, legendary order.")]
    public List<float> rarityWeights = new List<float>() { 70, 25, 5 };
    public List<ItemUpgrade> allUpgrades;
    public List<ItemUpgrade> weaponOnlyUpgrades;
    public List<ItemUpgrade> amuletOnlyUpgrades;
    public List<ItemUpgrade> armorOnlyUpgrades;

    protected GameManager gm;
    protected ObjectManager objectManager;
    protected List<ItemUpgrade> commonItemUpgrades;
    protected List<ItemUpgrade> rareItemUpgrades;
    protected List<ItemUpgrade> legendaryItemUpgrades;
    protected List<ItemUpgrade> commonWeaponUpgrades;
    protected List<ItemUpgrade> rareWeaponUpgrades;
    protected List<ItemUpgrade> legendaryWeaponUpgrades;

    public void Initialize() {
        gm = GameManager.Instance;
        objectManager = gm.objectManager;
        Assert.IsTrue(rarityWeights.Count == 3);
        commonItemUpgrades = allUpgrades.FindAll(u => u.IsItemOnlyUpgrade() && u.rarity == Rarity.COMMON);
        rareItemUpgrades = allUpgrades.FindAll(u => u.IsItemOnlyUpgrade() && u.rarity == Rarity.RARE);
        legendaryItemUpgrades = allUpgrades.FindAll(u => u.IsItemOnlyUpgrade() && u.rarity == Rarity.LEGENDARY);
        commonWeaponUpgrades = allUpgrades.FindAll(u => u.rarity == Rarity.COMMON);
        rareWeaponUpgrades = allUpgrades.FindAll(u => u.rarity == Rarity.RARE);
        legendaryWeaponUpgrades = allUpgrades.FindAll(u => u.rarity == Rarity.LEGENDARY);
    }

    public int GetNbOfUpgradesForItem(Item.ItemType itemType) {
        List<int> nbOfUpgrades;
        List<float> weights;
        switch (itemType) {
            case Item.ItemType.WEAPON:
                nbOfUpgrades = Enumerable.Range(0, nbUpgradesWeightsForWeapon.Count).ToList();
                weights = nbUpgradesWeightsForWeapon;
                break;
            case Item.ItemType.AMULET:
                nbOfUpgrades = Enumerable.Range(0, nbUpgradesWeightsForAmulets.Count).ToList();
                weights = nbUpgradesWeightsForAmulets;
                break;
            case Item.ItemType.ARMOR:
                nbOfUpgrades = Enumerable.Range(0, nbUpgradesWeightsForArmors.Count).ToList();
                weights = nbUpgradesWeightsForArmors;
                break;
            default:
                throw new Exception($"Item.ItemType {itemType} unknown.");
        }
        return MathTools.ChoseOneWeighted(nbOfUpgrades, weights);
    }

    public int GetNbOfUpgradesForWeapon() {
        List<int> nbOfUpgrades = Enumerable.Range(0, nbUpgradesWeightsForWeapon.Count).ToList();
        return MathTools.ChoseOneWeighted(nbOfUpgrades, nbUpgradesWeightsForWeapon);
    }

    public List<ItemUpgrade> ChoseRarityPool(List<float> rarityWeights, List<List<ItemUpgrade>> poolsByRarity) {
        return MathTools.ChoseOneWeighted(poolsByRarity, rarityWeights);
    }

    protected List<ItemUpgrade> ChoseItemWeaponList(List<ItemUpgrade> items, List<ItemUpgrade> weapons, bool isWeapon) {
        return (isWeapon ? weapons : items).Select(u => u).ToList();
    }

    protected List<ItemUpgrade> GetItemSpecificPool(Item.ItemType itemType) {
        switch (itemType) {
            case Item.ItemType.WEAPON: return weaponOnlyUpgrades;
            case Item.ItemType.AMULET: return amuletOnlyUpgrades;
            case Item.ItemType.ARMOR: return armorOnlyUpgrades;
            default:
                throw new Exception($"Item.ItemType {itemType} unknown.");
        }
    }

    protected List<ItemUpgrade> GetPoolFor(Rarity rarity, Item.ItemType itemType) {
        List<ItemUpgrade> itemSpecificPool = GetItemSpecificPool(itemType);
        itemSpecificPool = itemSpecificPool.FindAll(u => u.rarity == rarity);
        bool isWeapon = itemType == Item.ItemType.WEAPON;
        switch (rarity) {
            case Rarity.COMMON:
                return ChoseItemWeaponList(commonItemUpgrades, commonWeaponUpgrades, isWeapon).Concat(itemSpecificPool).ToList();
            case Rarity.RARE:
                return ChoseItemWeaponList(rareItemUpgrades, rareWeaponUpgrades, isWeapon).Concat(itemSpecificPool).ToList();
            case Rarity.LEGENDARY:
                return ChoseItemWeaponList(legendaryItemUpgrades, legendaryWeaponUpgrades, isWeapon).Concat(itemSpecificPool).ToList();
            default:
                throw new Exception($"Rarity {rarity} unknown.");
        }
    }

    public List<ItemUpgrade> GetNUpgrades(int nbUpgrades, Item.ItemType itemType) {
        List<float> currentRarityWeights = rarityWeights.Select(w => w).ToList();
        List<ItemUpgrade> upgrades = new List<ItemUpgrade>();
        List<ItemUpgrade> commonPool = GetPoolFor(Rarity.COMMON, itemType);
        List<ItemUpgrade> rarePool = GetPoolFor(Rarity.RARE, itemType);
        List<ItemUpgrade> legendaryPool = GetPoolFor(Rarity.LEGENDARY, itemType);
        for(int i = 0; i < nbUpgrades; i++) {
            List<List<ItemUpgrade>> all3Pools = new List<List<ItemUpgrade>>() { commonPool, rarePool, legendaryPool };
            List<ItemUpgrade> pool = ChoseRarityPool(currentRarityWeights, all3Pools);
            if(pool == legendaryPool) {
                currentRarityWeights[2] = 0; // Max 1 legendary upgrade ! :p
            }
            ItemUpgrade upgrade = MathTools.ChoseOne(pool);
            if(pool.Count == 1) {
                int poolIndice = all3Pools.FindIndex(p => p == pool);
                currentRarityWeights[poolIndice] = 0; // If we used all upgrades of a rarity, stop sampling from this one :)
            }
            pool.Remove(upgrade);
            upgrades.Add(upgrade);
        }
        return upgrades;
    }

    public List<ItemUpgrade> GetUpgradesForWeapon() {
        int nbUpgrades = GetNbOfUpgradesForWeapon();
        return GetNUpgrades(nbUpgrades, Item.ItemType.WEAPON);
    }

    public List<ItemUpgrade> GetUpgradesForItem(Item.ItemType itemType) {
        int nbUpgrades = GetNbOfUpgradesForItem(itemType);
        return GetNUpgrades(nbUpgrades, itemType);
    }

    public void AddUpgradesToWeapon(Weapon weapon) {
        List<ItemUpgrade> itemUpgrades = GetUpgradesForWeapon();
        List<ItemUpgrade> itemOnlyUpgrades = itemUpgrades.FindAll(u => u.IsItemOnlyUpgrade()).ToList();
        List<WeaponUpgrade> weaponUpgrades = itemUpgrades.FindAll(u => u.IsWeaponUpgrade()).Select(u => u.GetComponent<WeaponUpgrade>()).ToList();
        foreach(ItemUpgrade upgrade in itemOnlyUpgrades) {
            ItemUpgrade newUpgrade = Instantiate(upgrade.gameObject, parent: weapon.transform).GetComponent<ItemUpgrade>();
            weapon.upgrades.Add(newUpgrade);
        }
        foreach(WeaponUpgrade upgrade in weaponUpgrades) {
            WeaponUpgrade newUpgrade = Instantiate(upgrade.gameObject, parent: weapon.transform).GetComponent<WeaponUpgrade>();
            weapon.weaponUpgrades.Add(newUpgrade);
        }
    }

    public void AddUpgradesToItem(Item item) {
        List<ItemUpgrade> itemUpgrades = GetUpgradesForItem(item.itemType);
        foreach(ItemUpgrade upgrade in itemUpgrades) {
            ItemUpgrade newUpgrade = Instantiate(upgrade.gameObject, parent: item.transform).GetComponent<ItemUpgrade>();
            item.upgrades.Add(newUpgrade);
        }
    }
}
