﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Inventory : MonoBehaviour {

    public Weapon initialWeaponPrefab;
    public Item initialAmuletPrefab;
    public Item initialArmorPrefab;

    protected Weapon weapon;
    protected Item amulet;
    protected Item armor;

    protected Player owner;

    public void Initialize(Player owner) {
        this.owner = owner;
        InitializeInitialItems();
    }

    protected void InitializeInitialItems() {
        if (initialWeaponPrefab != null) {
            Weapon initialWeapon = Instantiate(initialWeaponPrefab.gameObject, parent: transform).GetComponent<Weapon>();
            EquipWeapon(initialWeapon);
        }
        if (initialAmuletPrefab != null) {
            Item initialAmulet = Instantiate(initialAmuletPrefab.gameObject, parent: transform).GetComponent<Item>();
            Equip(initialAmulet);
        }
        if (initialArmorPrefab != null) {
            Item initialArmor = Instantiate(initialArmorPrefab.gameObject, parent: transform).GetComponent<Item>();
            Equip(initialArmor);
        }
    }

    public void Equip(Item item) {
        if(item.IsWeapon()) {
            EquipWeapon(item.GetComponent<Weapon>());
        } else {
            EquipItem(item);
        }
    }

    public void EquipItem(Item item) {
        Item oldItem = GetItemOfType(item.itemType);
        if (oldItem != null) {
            UnEquip(oldItem);
        }
        AffectItem(item);
        item.Initialize(owner);
        owner.onEquipItem.Invoke(owner, item);
    }

    protected void AffectItem(Item item) {
        switch (item.itemType) {
            case Item.ItemType.WEAPON:
                weapon = item.GetComponent<Weapon>();
                break;
            case Item.ItemType.AMULET:
                amulet = item;
                break;
            case Item.ItemType.ARMOR:
                armor = item;
                break;
            default:
                throw new Exception($"Unknown itemType : {item.itemType}.");
        }
    }

    protected void UnAffectItem(Item item) {
        switch (item.itemType) {
            case Item.ItemType.WEAPON:
                weapon = null;
                break;
            case Item.ItemType.AMULET:
                amulet = null;
                break;
            case Item.ItemType.ARMOR:
                armor = null;
                break;
            default:
                throw new Exception($"Unknown itemType : {item.itemType}.");
        }
    }

    public void SetCurrentItemsToInitialItems() {
        initialWeaponPrefab = weapon;
        initialAmuletPrefab = amulet;
        initialArmorPrefab = armor;
        weapon = null;
        amulet = null;
        armor = null;
    }

    public Item GetItemOfType(Item.ItemType itemType) {
        switch (itemType) {
            case Item.ItemType.WEAPON: return weapon;
            case Item.ItemType.AMULET: return amulet;
            case Item.ItemType.ARMOR: return armor;
            default: throw new Exception($"Unknown itemType : {itemType}.");
        }
    }

    public void EquipWeapon(Weapon newWeapon) {
        EquipItem(newWeapon);
        owner.onEquipWeapon.Invoke(owner, newWeapon);
    }

    public void UnEquip(Item item) {
        if(item.IsWeapon()) {
            UnEquipWeapon(item.GetComponent<Weapon>());
        } else {
            UnEquipItem(item);
        }
    }

    public void UnEquipItem(Item item) {
        Item oldItem = GetItemOfType(item.itemType);
        if(oldItem != null) {
            oldItem.OnUnEquip();
        }
        UnAffectItem(item);
    }

    public void UnEquipWeapon(Weapon oldWeapon) {
        UnEquipItem(oldWeapon);
        // Additionnal stuff for weapon here if needed
    }

    public Weapon GetWeapon() {
        return weapon;
    }
    public Item GetAmulet() {
        return amulet;
    }
    public Item GetArmor() {
        return armor;
    }
}