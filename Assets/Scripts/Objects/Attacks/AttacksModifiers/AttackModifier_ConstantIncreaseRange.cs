﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;

public class AttackModifier_ConstantIncreaseRange : AttackModifier {

    public float rangeIncreaseSpeed = 1.0f;

    protected AttackRange attackRange;
    protected float maxRange;
    protected FloatModifier stickyModifier;

    public override void Initialize(Attack attack, bool isUpgrade=false, Rarity upgradeRarity = Rarity.COMMON) {
        base.Initialize(attack, isUpgrade, upgradeRarity);
        this.attackRange = (AttackRange)attack;
        Assert.IsNotNull(attackRange);
        stickyModifier = new FloatModifier(FloatModifier.Mode.SET, FloatModifier.Period.INFINITE_DURATION, attackRange.range.initialRange);
        attackRange.range.rangeModifier.AddModifier(stickyModifier);
        attackRange.range.rangeModifier.onRemoveModifier.AddListener(OnRemoveOtherModifier);
    }

    protected void OnRemoveOtherModifier(FloatModifier otherModifier) {
        if(otherModifier.IsAdder()) {
            stickyModifier.value += otherModifier.value;
        }
    }

    public void Update() {
        float currentRange = attackRange.range.Get();
        //float addedValue = attackRange.range.rangeModifier.GetAddedValue();
        float multipliedCoef = attackRange.range.rangeModifier.GetMultipliersCoef();
        float multipliedMaxRange = attackRange.range.GetMaxRange() * multipliedCoef; // (maxRange + addedValue) * multipliedCoef;
        if (currentRange < multipliedMaxRange) {
            float newRange = currentRange + rangeIncreaseSpeed * Time.deltaTime;
            newRange = Mathf.Min(multipliedMaxRange, newRange);
            float variation = newRange - currentRange;
            stickyModifier.ChangeValue(stickyModifier.value + variation);
        }
    }

    public override string Description() {
        return $"Gagne {rangeIncreaseSpeed} de portée par secondes";
    }

    public override string DescriptionOneWord() {
        return "d'expansion";
    }
}