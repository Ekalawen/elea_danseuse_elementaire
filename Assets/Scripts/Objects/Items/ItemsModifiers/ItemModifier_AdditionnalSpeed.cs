﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;

public class ItemModifier_AdditionnalSpeed : ItemModifier {

    public float addedSpeed = 1.0f;

    protected SpeedModifier speedModifier;

    public override void Initialize(Item item, bool isUpgrade = false, Rarity upgradeRarity = Rarity.COMMON) {
        base.Initialize(item, isUpgrade, upgradeRarity);
        speedModifier = item.owner.movement.AddModifier(new SpeedModifier(SpeedModifier.Mode.ADD, SpeedModifier.Period.INFINITE_DURATION, addedSpeed));
    }

    public override void OnUnEquip() {
        item.owner.movement.RemoveModifier(speedModifier);
        base.OnUnEquip();
    }

    public override string Description() {
        return $"{addedSpeed} de vitesse supplémentaire";
    }

    public override string DescriptionOneWord()
    {
        return "rapide";
    }

}