using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public abstract class GraphGenerator : MonoBehaviour {

    [HideInInspector]
    public List<Node> nodes;
    [HideInInspector]
    public List<Edge> edges;

    protected Graph graph;

    public void Initialize(Graph graph) {
        this.graph = graph;
    }

    public void Generate() {
        nodes = new List<Node>();
        edges = new List<Edge>();
        CreateGraph();
    }

    public abstract void CreateGraph();

    public virtual void Step() {
    }

    public Node CreateNode(Vector2 pos) {
        Node node = Instantiate(graph.nodePrefab, parent: graph.nodesFolder).GetComponent<Node>();
        node.Initialize(pos);
        return node;
    }

    private Edge CreateEdge(Node node1, Node node2) {
        if(node1.IsVoisin(node2) || node2.IsVoisin(node1)) {
            return null;
        }
        if(nodes.Any(n => n.IsInsideEdge(node1, node2))) {
            return null;
        }
        Edge edge = Instantiate(graph.edgePrefab, parent: graph.edgesFolder).GetComponent<Edge>();
        edge.Initialize(node1, node2);
        return edge;
    }

    public Edge CreateAndAddEdge(Node node1, Node node2) {
        Edge newEdge = CreateEdge(node1, node2);
        if(newEdge != null) {
            edges.Add(newEdge);
        }
        return newEdge;
    }
}
