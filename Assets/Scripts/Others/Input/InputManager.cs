using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Localization;
using UnityEngine.Localization.Settings;

public class InputManager : MonoBehaviour
{

    static InputManager _instance;
    public static InputManager Instance { get { return _instance ?? (_instance = new GameObject().AddComponent<InputManager>()); } }

    protected bool isInGame = false;

    void Awake()
    {
        if (!_instance) { _instance = this; }
        Initialize();
    }

    public void Initialize()
    {
        name = "InputManager";
        DontDestroyOnLoad(this);
    }

    public void Update() {
        //DetectPlugUnplugController();
        //DetectUseOtherController();
    }

    public bool GetRestartGame() {
        return Input.GetKeyDown(KeyCode.R);
    }

    public bool GetPauseGame() {
        return Input.GetKeyDown(KeyCode.Escape);
    }

    public bool GetQuitGame() {
        return false;
        //return Input.GetKeyDown(KeyCode.Escape);
    }

    public void SetInGame()
    {
        isInGame = true;
    }

    public void SetNotInGame()
    {
        isInGame = false;
    }
}
