using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class ElementSlider : MonoBehaviour {

    public Element element;
    public Slider slider;
    public Image fill;
    public Gradient fillGradient;
    public float alphaTransitionDurationIn = 0.5f;
    public float explodedStayDuration = 1.0f;
    public float alphaTransitionDurationOut = 1.0f;
    public CanvasGroup canvasGroup;

    protected GameManager gm;
    protected ElementManager elementManager;
    protected SingleCoroutine updateSingleCoroutine;
    protected SingleCoroutine alphaSingleCoroutine;
    protected BoolTimer hasExploded;

    public void Initialize() {
        gm = GameManager.Instance;
        elementManager = gm.enemyManager.elementManager;
        updateSingleCoroutine = new SingleCoroutine(this);
        alphaSingleCoroutine = new SingleCoroutine(this);
        hasExploded = new BoolTimer(this);
        canvasGroup.alpha = 0;
        canvasGroup.gameObject.SetActive(false);
        LinkListenersToEvents();
    }

    protected abstract void LinkListenersToEvents();

    protected void RememberExploded(Player player) {
        hasExploded.SetTime(alphaTransitionDurationOut);
    }

    protected void StartCoroutine(Player player) {
        canvasGroup.gameObject.SetActive(true);
        alphaSingleCoroutine.Start(CChangeAlphaTo(1.0f, 0.0f, alphaTransitionDurationIn));
        updateSingleCoroutine.Start(UpdateSlider());
    }

    protected void StopCoroutine(Player player) {
        float stayDuration = hasExploded.value ? explodedStayDuration : 0.0f;
        alphaSingleCoroutine.Start(CChangeAlphaTo(0.0f, stayDuration, alphaTransitionDurationOut));
        if (hasExploded.value) {
            SetSliderValue(ElementManager.MAX_COUNT);
        }
        updateSingleCoroutine.Stop();
    }

    protected IEnumerator CChangeAlphaTo(float newAlpha, float wait, float duration) {
        if (wait > 0) {
            yield return new WaitForSeconds(wait);
        }
        Timer timer = new Timer(duration);
        float initialAlpha = canvasGroup.alpha;
        while(!timer.IsOver()) {
            canvasGroup.alpha = MathCurves.Linear(initialAlpha, newAlpha, timer.GetAvancement());
            yield return null;
        }
        if(newAlpha == 0) {
            canvasGroup.gameObject.SetActive(false);
        }
        canvasGroup.alpha = newAlpha;
    }

    protected IEnumerator UpdateSlider() {
        while (true) {
            float elementValue = GetElementCount();
            SetSliderValue(elementValue);
            yield return null;
        }
    }

    protected float GetElementCount() {
        return elementManager.GetElementCount(element);
    }

    protected void SetSliderValue(float elementValue) {
        if(hasExploded.value) {
            elementValue = ElementManager.MAX_COUNT;
        }
        slider.value = elementValue;
        fill.color = fillGradient.Evaluate(elementValue / ElementManager.MAX_COUNT);
    }
}
