using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;

public class ElementProtection : MonoBehaviour {

    protected Character owner;
    protected float fireProtection = 0.0f;
    protected float waterProtection = 0.0f;
    [HideInInspector]
    public FloatModifierManager fireModifier;
    [HideInInspector]
    public FloatModifierManager waterModifier;

    public void Initialize(Character owner) {
        this.owner = owner;
        fireModifier = new FloatModifierManager(this, GetRawFireProtection);
        waterModifier = new FloatModifierManager(this, GetRawWaterProtection);
    }

    protected float GetRawFireProtection() {
        return fireProtection;
    }

    protected float GetRawWaterProtection() {
        return waterProtection;
    }

    public float GetFireProtection() {
        return fireModifier.Get();
    }

    public float GetWaterProtection() {
        return waterModifier.Get();
    }

    public FloatModifierManager GetModifierFor(Element element) {
        switch (element)
        {
            case Element.FIRE: return fireModifier;
            case Element.WATER: return waterModifier;
            default:
                throw new Exception($"This element {element} is not implemented yet in GetModifierForElement ! :)");
        }
    }

    public float GetProtectionFor(Element element) {
        switch (element)
        {
            case Element.FIRE: return fireModifier.Get();
            case Element.WATER: return waterModifier.Get();
            default:
                throw new Exception($"This element {element} is not implemented yet in GetProtectionFor ! :)");
        }
    }
}