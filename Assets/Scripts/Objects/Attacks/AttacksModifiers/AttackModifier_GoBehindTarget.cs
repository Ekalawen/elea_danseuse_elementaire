﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AttackModifier_GoBehindTarget : AttackModifier_OnEvent {

    public bool teleport = false;

    protected Graph graph;

    public override void Initialize(Attack attack, bool isUpgrade = false, Rarity upgradeRarity = Rarity.COMMON) {
        base.Initialize(attack, isUpgrade, upgradeRarity);
        graph = GameManager.Instance.graph;
        AddListenerToEvents(StealPlace);
    }

    public void StealPlace(Character other) {
        Node behindNode = GetBehindNode(other);
        if (teleport) {
            attack.owner.TeleportTo(behindNode);
        } else {
            attack.owner.MoveTo(behindNode);
        }
    }

    protected Node GetBehindNode(Character other) {
        return graph.ClosestTo(other.pos + (other.pos - attack.owner.pos));
    }

    public override string Description() {
        string action = teleport ? "Teleporte" : "Déplace";
        return $"{action} derrière la cible {DescriptionOnEvent()}";
    }

    public override string DescriptionOneWord()
    {
        return "de contournement";
    }
}