using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;


public class GraphGenerator_GridReduced : GraphGenerator {

    // Theorem: a straight-line drawing is planar iff every inner vertex is inside the convex hull of its neighbours

    public GraphGenerator_Grid gridGenerator;
    public int nbNodesToDestroy = 10;
    public int nbNodesToReduce = 10;
    public GameObject redNodePrefab;

    protected Node nextNode;

    public override void CreateGraph() {
        gridGenerator.Initialize(graph);
        gridGenerator.Generate();
        nodes = gridGenerator.nodes;
        edges = gridGenerator.edges;

        ChoseNextNode();

        DestroySomeNodes();
        ReduceSomeNodes();
    }

    protected void DestroySomeNodes() {
        Assert.IsTrue(nodes.Count >= nbNodesToDestroy);
        for(int i = 0; i < nbNodesToDestroy; i++) {
            Node node = MathTools.ChoseOne(nodes);
            DestroyNodeWithEdges(node);
        }
    }

    protected void ReduceSomeNodes() {
        Assert.IsTrue(nodes.Count >= nbNodesToReduce);
        for(int i = 0; i < nbNodesToReduce; i++) {
            ReduceOneNode();
        }
    }

    public override void Step() {
        ReduceOneNode();
    }

    protected void ReduceOneNode() {
        Node node = nextNode;
        nodes.Remove(node);
        ConnectAdjacentVoisinsOfNode(node);
        DestroyNodeWithEdges(node);
        ChoseNextNode();
    }

    protected void ChoseNextNode() {
        nextNode = MathTools.ChoseOne(nodes);
        nextNode.SetColorHue(0.0f);
    }

    protected void DestroyNodeWithEdges(Node node) {
        List<Edge> nodeEdges = node.edges.Select(e => e).ToList();
        foreach(Edge e in nodeEdges) {
            edges.Remove(e);
            e.Delete();
        }
        nodes.Remove(node);
        Destroy(node.gameObject);
    }

    protected void ConnectAdjacentVoisinsOfNode(Node node) {
        List<Node> voisins = node.Voisins();
        if (voisins.Count <= 1) return;

        if (voisins.Count == 2) {
            CreateAndAddEdgeWithNothingInTriangle(voisins[0], voisins[1], node);
            return;
        }

        voisins = voisins.OrderBy(v => Vector2.SignedAngle(v.pos - node.pos, Vector2.right)).ToList();
        for (int i = 0; i < voisins.Count; i++) {
            Node current = voisins[i];
            Node next = voisins[(i + 1) % voisins.Count];
            CreateAndAddEdgeWithNothingInTriangle(current, next, node);
        }
    }

    protected Edge CreateAndAddEdgeWithNothingInTriangle(Node node1, Node node2, Node triangleNode) {
        foreach(Node node in nodes) {
            if (node.IsInsideTriangle(node1, node2, triangleNode)) {
                return null;
            }
        }
        if(nodes.Any(n => n.IsInsideTriangle(node1, node2, triangleNode))) {
            return null;
        }
        return CreateAndAddEdge(node1, node2);
    }
}
