﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Item : MonoBehaviour {

    public enum ItemType { WEAPON, AMULET, ARMOR };

    public ItemType itemType;
    public new string name;
    public string description;
    public Sprite sprite;
    public List<ItemModifier> modifiers;
    public List<ItemUpgrade> upgrades;

    [HideInInspector]
    public Player owner;

    public virtual void Initialize(Player owner) {
        this.owner = owner;
        InitializeModifiersAndUpgrades();
        transform.SetParent(owner.transform);
    }

    protected void InitializeModifiersAndUpgrades() {
        foreach(ItemModifier modifier in modifiers) {
            modifier.Initialize(this);
        }
        foreach(ItemUpgrade upgrade in upgrades) {
            upgrade.Initialize(this);
        }
    }

    public virtual void OnUnEquip() {
        foreach (ItemModifier modifier in modifiers) {
            modifier.OnUnEquip();
        }
        foreach (ItemUpgrade upgrade in upgrades) {
            upgrade.OnUnEquip();
        }
        owner = null;
    }

    public virtual Rarity Rarity() {
        if (upgrades.Count == 0) {
            return global::Rarity.COMMON;
        }
        Rarity rarity = upgrades.Max(u => u.rarity);
        return rarity;
    }

    public void AddUpgradeWithoutInitializing(ItemUpgrade upgrade) {
        upgrades.Add(upgrade);
    }

    public bool IsWeapon() {
        return itemType == ItemType.WEAPON;
    }

    public virtual List<ItemUpgrade> GetAllUpgrades() {
        return upgrades;
    }

    public Weapon AsWeapon() {
        return GetComponent<Weapon>();
    }
}