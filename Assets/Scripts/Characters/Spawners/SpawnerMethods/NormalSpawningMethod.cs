﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NormalSpawningMethod : MonoBehaviour {

    protected GameManager gm;
    protected SpawnerManager manager;

    public virtual void Initialize(SpawnerManager manager) {
        gm = GameManager.Instance;
        this.manager = manager;
    }

    public virtual Node GetNode(float size) {
        return manager.GetSpawningNode(size);
    }
}
