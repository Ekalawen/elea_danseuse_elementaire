using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;

public class CharacterModifier_DropProjectileOnDeath : CharacterModifier {

    public GameObject projectilePrefab;

    protected Projectile projectile;

    public override void Initialize(Character owner) {
        base.Initialize(owner);
        owner.onKilled.AddListener(DropProjectile);
    }

    protected void DropProjectile(Character thisCharacter) {
        projectile = Instantiate(projectilePrefab).GetComponent<Projectile>();
        projectile.Initialize(owner, owner.pos, Vector2.zero);
    }
}
