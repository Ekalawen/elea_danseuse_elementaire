﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class SpawnerManager : MonoBehaviour {

    public abstract List<MonoBehaviour> SpawnSpawningQuantity(List<SpawningQuantity> spawningQuantities, Func<float, Node> method);

    public abstract Node GetSpawningNode(float forSize);

    public abstract Spawner AddSpawner(Spawner spawner);

    public abstract bool RemoveSpawner(Spawner spawner);
}
