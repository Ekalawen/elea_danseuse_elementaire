using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FireSlider : ElementSlider {
    protected override void LinkListenersToEvents() {
        elementManager.onStartSufferFire.AddListener(StartCoroutine);
        elementManager.onStopSufferFire.AddListener(StopCoroutine);
        elementManager.onFireExplode.AddListener(RememberExploded);
    }
}
