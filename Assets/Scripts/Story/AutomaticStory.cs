using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.VFX;

public class AutomaticStory : MonoBehaviour {

    public TextAsset file;
    public bool oneTime = true;
    public float delay = 0.0f;

    protected GameManager gm;
    protected StoryManager storyManager;
    protected List<StoryLine> storyLines;
    protected int nbTimesTriggered = 0;

    public virtual void Initialize() {
        gm = GameManager.Instance;
        storyManager = gm.storyManager;
        storyLines = StoryManager.GetStoryLinesFor(file, storyManager.characters);
    }

    protected void TryTrigger() {
        if(oneTime && nbTimesTriggered > 0) {
            return;
        }
        nbTimesTriggered++;
        StartCoroutine(CTriggerIn());
    }

    public void InitializeAndTryTrigger() {
        Initialize();
        TryTrigger();
    }

    protected IEnumerator CTriggerIn() {
        yield return new WaitForSeconds(delay);
        Trigger();
    }

    private void Trigger() {
        storyManager.DisplayLines(storyLines);
    }
}
