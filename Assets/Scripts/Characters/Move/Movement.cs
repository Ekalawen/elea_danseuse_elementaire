using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;

public class Movement : MonoBehaviour {

    public enum Type { FIX_SPEED, FIX_DURATION };

    public Type type = Type.FIX_SPEED;
    [ConditionalHide("type", Type.FIX_SPEED)]
    public float speed = 3.0f;
    [ConditionalHide("type", Type.FIX_DURATION)]
    public float duration = 1 / 3f;
    public float waitingTime = 1.0f;
    public AnimationCurve speedCurve = AnimationCurve.Linear(0, 0, 1, 1);

    [SerializeField]
    protected List<SpeedModifier> modifiers;
    [HideInInspector]
    public Character owner;

    public void Initialize(Character owner) {
        this.owner = owner;
        FixSpeedAndDurationOnOther();
        modifiers = new List<SpeedModifier>();
    }

    protected void FixSpeedAndDurationOnOther() {
        if(type == Type.FIX_DURATION) {
            speed = 1.0f / duration;
        } else {
            duration = 1.0f / speed;
        }
    }

    protected float InitialSpeed(float distance) {
        if (type == Type.FIX_SPEED) {
            return speed;
        }
        return distance / duration;
    }

    protected float InitialDuration(float distance) {
        if (type == Type.FIX_DURATION) {
            return duration;
        }
        return distance / speed;
    }

    public void MultiplySpeedBy(float coef) {
        speed *= coef;
        duration /= coef;
    }

    public void SetSpeed(float newSpeed) {
        float coef = newSpeed / speed;
        MultiplySpeedBy(coef);
    }

    public SpeedModifier SetSpeedForOneMove(float newSpeed) {
        return AddModifier(new OneMoveSpeedSetter(newSpeed));
    }

    public SpeedModifier AddModifier(SpeedModifier modifier) {
        if(modifier.IsSetter()) {
            RemoveAllSetterModifiers();
        }
        modifier.Initialize(this);
        modifiers.Add(modifier);
        if (modifier.UseDuration()) {
            StartCoroutine(CUnregisterDurationModifiersAtEnd(modifier));
        }
        return modifier;
    }

    protected void RemoveAllSetterModifiers() {
        modifiers = modifiers.FindAll(m => m.IsAdder());
    }

    protected IEnumerator CUnregisterDurationModifiersAtEnd(SpeedModifier modifier) {
        yield return new WaitForSeconds(modifier.duration);
        RemoveModifier(modifier);
    }

    public void RemoveModifier(SpeedModifier modifier) {
        modifiers.Remove(modifier);
    }

    public void RemoveAllModifiers() {
        modifiers.Clear();
    }

    public List<SpeedModifier> GetAllModifiers() {
        return modifiers;
    }

    public float GetModifiedSpeed(float distance) {
        RemoveOverModifiers();
        float speed = InitialSpeed(distance);
        SpeedModifier setterModifier = modifiers.Find(m => m.IsSetter());
        if(setterModifier != null) {
            speed = setterModifier.Get();
        }
        foreach(SpeedModifier modifier in modifiers.FindAll(m => m.IsAdder())) {
            speed += modifier.Get();
        }
        return Mathf.Max(speed, 0);
    }

    protected void RemoveOverModifiers() {
        modifiers = modifiers.FindAll(m => !m.IsOver());
    }

    public float GetModifiedDuration(float distance) {
        return distance / GetModifiedSpeed(distance);
    }
}
