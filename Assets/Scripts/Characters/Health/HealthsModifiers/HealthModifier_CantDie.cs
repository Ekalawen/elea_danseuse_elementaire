﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;

public class HealthModifier_CantDie : HealthModifier {

    public override void Initialize(Health health) {
        base.Initialize(health);
        health.SetMinNumberOfHeartsTo1();
    }
}