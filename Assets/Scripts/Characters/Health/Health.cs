﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;
using System.Linq;

public class Health : MonoBehaviour {

    public int initialLife = 1;

    [HideInInspector]
    public UnityEvent<int> onLoseLife = new UnityEvent<int>();
    [HideInInspector]
    public UnityEvent<int> onGainLife = new UnityEvent<int>();

    [HideInInspector]
    public UnityEvent<int> onChangeLife = new UnityEvent<int>();
    [HideInInspector]
    public UnityEvent onDie = new UnityEvent();
    [HideInInspector]
    public UnityEvent onBackToMaxHealth = new UnityEvent();

    protected Character character;
    protected int maxHearts;
    protected int currentHearts;
    protected int currentArmor = 0;
    [HideInInspector]
    public BoolTimer isInvulnerable;
    [HideInInspector]
    public BoolTimer hasTakenHit;
    protected float hasTakenHitDuration = 0.5f;
    protected List<HealthModifier> modifiers;
    protected int minNumberOfHearts = 0;

    public virtual void Initialize(Character character) {
        this.character = character;
        maxHearts = initialLife;
        currentHearts = initialLife;
        isInvulnerable = new BoolTimer(this);
        hasTakenHit = new BoolTimer(this);
        hasTakenHitDuration = GameManager.Instance.postProcessManager.hasTakenHitColorDuration;
        isInvulnerable.onStop.AddListener(CheckForCollisionDamages);
        InitializeModifiers();
    }

    protected void InitializeModifiers() {
        modifiers = GetComponents<HealthModifier>().ToList();
        foreach(HealthModifier modifier in modifiers) {
            modifier.Initialize(this);
        }
    }

    public bool LoseLife(int lifeLost, bool ignoreInvulnerability = false) {
        Assert.IsTrue(lifeLost >= 0);
        if(lifeLost == 0) {
            return false;
        }
        if(!ignoreInvulnerability && isInvulnerable.value) {
            return false;
        }
        if(CurrentArmor() > 0) {
            int nbArmorToLose = Mathf.Min(lifeLost, currentArmor);
            currentArmor -= nbArmorToLose;
            lifeLost -= nbArmorToLose;
        }
        currentHearts = Mathf.Max(minNumberOfHearts, currentHearts - lifeLost);
        onChangeLife.Invoke(CurrentLife());
        if(CurrentLife() == 0) {
            onDie.Invoke();
            character.Die();
        } else {
            onLoseLife.Invoke(lifeLost);
            hasTakenHit.SetTime(hasTakenHitDuration);
        }
        return true;
    }

    public bool LoseArmor(int armorToLose, bool ignoreInvulnerability = false) {
        Assert.IsTrue(armorToLose >= 0);
        if (armorToLose == 0) {
            return false;
        }
        if (!ignoreInvulnerability && isInvulnerable.value) {
            return false;
        }
        int nbArmorToLose = Mathf.Min(armorToLose, currentArmor);
        currentArmor -= nbArmorToLose;
        onChangeLife.Invoke(CurrentLife());
        if(CurrentLife() == 0) {
            onDie.Invoke();
            character.Die();
        } else {
            onLoseLife.Invoke(armorToLose);
            hasTakenHit.SetTime(hasTakenHitDuration);
        }
        return true;
    }


    public bool WillKill(int lifeLost, bool ignoreInvulnerability = false) {
        if(!ignoreInvulnerability && isInvulnerable.value) {
            return false;
        }
        return lifeLost > 0 && lifeLost >= CurrentLife() && minNumberOfHearts == 0;
    }

    public void GainLife(int lifeGain) {
        Assert.IsTrue(lifeGain >= 0);
        if(lifeGain == 0) {
            return;
        }
        currentHearts = Mathf.Min(maxHearts, currentHearts + lifeGain);
        onChangeLife.Invoke(CurrentLife());
        if (CurrentLife() == MaxLife()) {
            onBackToMaxHealth.Invoke();
        } else {
            onGainLife.Invoke(lifeGain);
        }
    }

    public void GainArmor(int armorToGain) {
        Assert.IsTrue(armorToGain >= 0);
        if(armorToGain == 0) {
            return;
        }
        currentArmor += armorToGain;
        onChangeLife.Invoke(CurrentLife());
        onGainLife.Invoke(armorToGain);
    }

    public void ModifyLife(int variation) {
        if(variation > 0) {
            GainLife(variation);
        } else {
            LoseLife(-variation);
        }
    }

    public void SetLife(int newLife) {
        ModifyLife(newLife - currentHearts);
    }

    public int CurrentLife() {
        return currentHearts + currentArmor;
    }

    public int CurrentHearts() {
        return currentHearts;
    }

    public void SetCurrentHeartsAndArmorNoCallback(int nbHearts, int nbArmor) {
        currentHearts = nbHearts;
        currentArmor = nbArmor;
    }

    public int CurrentArmor() {
        return currentArmor;
    }

    public int MaxLife() {
        return maxHearts + currentArmor;
    }

    public int MaxHearts() {
        return maxHearts;
    }

    public void AddInvulnerabilityTime(float time) {
        isInvulnerable.AddTime(time);
    }

    public void SetInvulnerableFor(float time) {
        isInvulnerable.SetTime(time);
    }

    public bool IsMaxHealth() {
        return CurrentLife() >= MaxLife();
    }

    public void GainMaxLifeOnly(int maxLifeAdded) {
        maxHearts += maxLifeAdded;
        onChangeLife.Invoke(CurrentLife());
    }

    public void LoseMaxLifeOnly(int maxLifeLost) {
        maxHearts -= maxLifeLost;
        if(currentHearts > maxHearts) {
            SetLife(maxHearts);
        }
        onChangeLife.Invoke(CurrentLife());
    }

    public void SetHasTakenHitColorDuration(float newDuration) {
        hasTakenHitDuration = newDuration;
    }

    protected void CheckForCollisionDamages() {
        List<Character> opponents = character.socle.CurrentSocleCollisionOf<Character>().FindAll(c => FactionHelper.OppositeFactions(c, character));
        if(opponents.Count > 0) {
            Character closest = MathTools.Argmin(opponents, o => Vector2.SqrMagnitude(o.pos - character.pos));
            closest.MeleeAttack(character);
        }
    }

    public Character Character() {
        return character;
    }

    public void SetMinNumberOfHeartsTo1() {
        minNumberOfHearts = 1;
    }
}