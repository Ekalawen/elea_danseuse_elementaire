using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(CircleCollider2D))]
public class Range : MonoBehaviour {

    public float initialRange = 1.0f;

    protected GameManager gm;
    protected CircleCollider2D collider2D;
    [HideInInspector]
    public UnityEvent<Range, Collider2D> onEnter = new UnityEvent<Range, Collider2D>(); // ThisRange, OtherCollider
    [HideInInspector]
    public UnityEvent<Range, Range> onEnterRange = new UnityEvent<Range, Range>(); // ThisRange, OtherRange
    [HideInInspector]
    public UnityEvent<Range, Socle> onEnterSocle = new UnityEvent<Range, Socle>(); // ThisRange, OtherSocle
    protected List<Collider2D> currentCollisions = new List<Collider2D>();
    protected SpriteRenderer spriteRenderer;
    [HideInInspector]
    public FloatModifierManager rangeModifier;
    protected float maxRange;

    public Vector2 pos {
        get { return new Vector2(transform.position.x, transform.position.y); }
        set { transform.position = value; }
    }

    public virtual void Initialize() {
        gm = GameManager.Instance;
        rangeModifier = new FloatModifierManager(this, GetInitialRange);
        rangeModifier.onChangeValue.AddListener(UpdateSize);
        ResizeTo(initialRange);
        maxRange = initialRange;
        collider2D = GetComponent<CircleCollider2D>();
        onEnter = new UnityEvent<Range, Collider2D>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        spriteRenderer.material = new Material(spriteRenderer.material);
    }

    protected float GetInitialRange() {
        return initialRange;
    }

    protected void ResizeTo(float newRange) {
        transform.localScale = Vector3.one * newRange;
    }

    protected void OnTriggerEnter2D(Collider2D collider) {
        currentCollisions.Add(collider);
        onEnter.Invoke(this, collider);
        Range otherRange = collider.GetComponent<Range>();
        if(otherRange != null) {
            onEnterRange.Invoke(this, otherRange);
        }
        Socle otherSocle = collider.GetComponent<Socle>();
        if(otherSocle != null) {
            onEnterSocle.Invoke(this, otherSocle);
        }
    }

    protected void OnTriggerExit2D(Collider2D collider) {
        currentCollisions.Remove(collider);
    }

    public List<Collider2D> CurrentCollisions() {
        return currentCollisions;
    }

    protected void SanitizeCurrentCollisions() {
        currentCollisions = currentCollisions.FindAll(c => c != null);
    }

    public List<T> CurrentCollisionOf<T>() {
        SanitizeCurrentCollisions();
        return currentCollisions
            .Select(c => c.gameObject.GetComponentInParent<T>())
            .Where(c => c != null)
            .ToList();
    }

    public List<T> CurrentSocleCollisionOf<T>() {
        SanitizeCurrentCollisions();
        return currentCollisions
            .FindAll(c => c.GetComponent<Range>().IsSocle())
            .Select(c => c.gameObject.GetComponentInParent<T>())
            .Where(c => c != null)
            .ToList();
    }

    public virtual bool IsSocle() {
        return false;
    }

    public List<Node> NodesIn(float rangeOffset) {
        float d2 = Mathf.Pow(Get() + rangeOffset, 2);
        return gm.graph.nodes.FindAll(n => Vector2.SqrMagnitude(n.pos - pos) <= d2);
    }

    public float Get() {
        if(rangeModifier == null) {
            rangeModifier = new FloatModifierManager(this, GetInitialRange);
        }
        return rangeModifier.Get();
    }

    public void UpdateSize() {
        ResizeTo(Get());
    }

    public void ReduceAndDestroyIn(float duration) {
        StartCoroutine(CReduceAndDestroyIn(duration));
    }

    protected IEnumerator CReduceAndDestroyIn(float duration) {
        Timer timer = new Timer(duration);
        FloatModifier stickyModifier = new FloatModifier(FloatModifier.Mode.MULTIPLY, FloatModifier.Period.INFINITE_DURATION, 1.0f);
        rangeModifier.AddModifier(stickyModifier);
        while(!timer.IsOver()) {
            stickyModifier.ChangeValue(1.0f - timer.GetAvancement());
            yield return null;
        }
        stickyModifier.ChangeValue(0.0f);
        Destroy(gameObject);
    }

    public void IncreaseMaxRangeOf(float rangeAdded) {
        maxRange += rangeAdded;
    }

    public float GetMaxRange() {
        return maxRange;
    }
}
