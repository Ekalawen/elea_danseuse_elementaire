using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public abstract class ANode : INode {

    public Node node { get; set; }
    public INode previous { get; set; }
    public float cost { get; set; }
    public float remainingEstimate { get; set; }
    public float heuristic { get; set; }

    public ANode() {
        Initialize(null, null, null);
    }

    public ANode(Node node, ANode previous, ANode target) {
        Initialize(node, previous, target);
    }

    public virtual void Initialize(Node node, INode previous, INode target) {
        this.node = node;
        this.previous = previous;
        this.cost = Cost(previous);
        this.remainingEstimate = RemainingEstimate(target);
        this.heuristic = Heuristic();
    }

    public override bool Equals(object other) {
        ANode otherNode = other as ANode;
        if(otherNode == null) {
            return false;
        }
        return node == otherNode.node;
    }

    public override int GetHashCode() {
        return base.GetHashCode();
    }

    public virtual float Cost(INode previousNode) {
        if (previousNode == null || node == null) {
            return 0;
        }
        return previousNode.cost + Vector2.Distance(previousNode.node.pos, node.pos);
    }

    public virtual float RemainingEstimate(INode target) {
        if (target == null || node == null) {
            return 0;
        }
        return Vector2.Distance(target.node.pos, node.pos);
    }

    public virtual float Heuristic() {
        return cost + remainingEstimate;
    }

    public abstract IList<INode> Voisins(INode target);

    public abstract IList<INode> FailResult(IList<INode> closed);

    public abstract IList<INode> Backtrack();

    public override string ToString() {
        return $"{base.ToString()}({node})";
    }
}
