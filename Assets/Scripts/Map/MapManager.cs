using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Events;

public class MapManager : MonoBehaviour
{
    public GameObject graphPrefab;
    public GameObject backgroundManagerPrefab;

    [HideInInspector]
    public GameManager gm;
    [HideInInspector]
    public Graph graph;
    protected Node playerSpawnNode;
    [HideInInspector]
    public BackgroundManager backgroundManager;

    public void Initialize()
    {
        gm = GameManager.Instance;
        name = "MapManager";
        CreateGraph();
        DecidePlayerSpawn();
        ExitPosition();
        InitializeBackgroundManager();
    }

    protected void InitializeBackgroundManager() {
        backgroundManager = Instantiate(backgroundManagerPrefab, parent: transform).GetComponent<BackgroundManager>();
        backgroundManager.Initialize();
    }

    protected void CreateGraph() {
        graph = Instantiate(graphPrefab).GetComponent<Graph>();
        graph.Initialize();
    }

    public Node PlayerStartPosition() {
        return playerSpawnNode;
    }

    protected void DecidePlayerSpawn() {
        List<Vector2> normals = MathTools.GetNormals2D();
        Vector2 normal = MathTools.ChoseOne(normals);
        // 1 / 2 chance to go to a corner;
        if(MathTools.Proba(0.5f)) {
            normals.Remove(normal);
            normals.Remove(-normal);
            Vector2 normal2 = MathTools.ChoseOne(normals);
            normal = (normal + normal2).normalized;
        }
        playerSpawnNode = graph.GetFarestNodeInDirection(normal);
    }

    public Node ExitPosition() {
        List<float> angles = new List<float>() { -90, -45, 0, 45, 90 };
        float angle = MathTools.ChoseOne(angles);
        Vector2 playerDirection = (playerSpawnNode.pos - graph.Center()).normalized;
        Vector2 exitDirection = Quaternion.AngleAxis(angle, Vector3.forward) * -playerDirection;
        Node exitNode = graph.GetFarestNodeInDirection(exitDirection);
        return exitNode;
    }
}
