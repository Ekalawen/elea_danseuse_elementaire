﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public abstract class ItemModifier : MonoBehaviour {

    protected Item item;
    [HideInInspector]
    public bool isUpgrade;
    [HideInInspector]
    public Rarity upgradeRarity;

    public virtual void Initialize(Item item, bool isUpgrade = false, Rarity upgradeRarity = Rarity.COMMON)
    {
        this.item = item;
        this.isUpgrade = isUpgrade;
        if (isUpgrade) {
            this.upgradeRarity = upgradeRarity;
        }
    }

    public abstract string Description();

    public abstract string DescriptionOneWord();

    public virtual void OnUnEquip() { }

    public Rarity GetRarity() {
        if(!isUpgrade) {
            return Rarity.COMMON;
        }
        return upgradeRarity;
    }
}