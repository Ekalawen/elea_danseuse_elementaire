﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Damage))]
public abstract class Attack : MonoBehaviour {


    [HideInInspector]
    public UnityEvent<Character> onHit = new UnityEvent<Character>(); // Who we are hitting
    [HideInInspector]
    public UnityEvent<Character> onKill = new UnityEvent<Character>(); // Who we are killing

    [HideInInspector]
    public Character owner;
    [HideInInspector]
    public Damage damage;
    protected List<AttackModifier> modifiers;

    public virtual void Initialize(Character owner) {
        this.owner = owner;
        this.damage = GetComponent<Damage>();
        this.damage.Initialize(owner);
        this.damage.onHit.AddListener(onHit.Invoke);
        this.damage.onKill.AddListener(onKill.Invoke);
        onHit.AddListener(owner.onHit.Invoke);
        onKill.AddListener(owner.onKill.Invoke);
        InitializeModifiers();
    }

    protected void InitializeModifiers() {
        modifiers = GetModifiers();
        foreach(AttackModifier modifier in modifiers) {
            modifier.Initialize(this);
        }
    }

    public bool TryTrigger() {
        if(CanTrigger()) {
            return Trigger();
        }
        return false;
    }

    public virtual bool CanTrigger() {
        if(modifiers.Count > 0) {
            return modifiers.All(m => m.CanTrigger());
        }
        return true;
    }

    protected abstract bool Trigger();

    public List<AttackModifier> GetModifiers() {
        if(modifiers == null || modifiers.Count == 0) {
            modifiers = gameObject.GetComponents<AttackModifier>().ToList();
        }
        return modifiers;
    }

    public List<AttackModifier> GetUpgrades() {
        return GetModifiers().FindAll(m => m.isUpgrade);
    }

    public void AddWithoutInitializeModifier(AttackModifier modifier) {
        modifiers.Add(modifier);
    }

    public abstract string BaseDescription();
}