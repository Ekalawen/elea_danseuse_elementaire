using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ItemUIDisplayer : MonoBehaviour {

    public Item.ItemType itemType;
    public Image itemImage;
    public float fontSizeTitleCoef = 1.5f;
    public float fontSizeShortDescription = 1.2f;
    public TooltipActivator itemTooltip;
    public float tooltipDurationOnEquip = 5.0f;
    public Vector2 tooltipFadeInOutDuration = new Vector2(0.5f, 0.5f);
    public Transform gemsHolder;
    public GameObject gemPrefab;
    public List<Material> gemRarityMaterials;
    public Image itemBorder;
    public List<Material> borderRarityMaterials;
    public Color fontNormalModifierColor;
    public List<Color> fontRarityColors;

    protected GameManager gm;
    protected Player player;
    protected List<Image> gems;
    protected SingleCoroutine stopTooltipCoroutine;

    public void Initialize() {
        gm = GameManager.Instance;
        player = gm.player;
        gems = new List<Image>();
        stopTooltipCoroutine = new SingleCoroutine(this);
        player.onEquipItem.AddListener(OnPlayerEquipItem);
        UpdateDisplayForNewItem(player.inventory.GetItemOfType(itemType));
    }

    protected void OnPlayerEquipItem(Character playerCharacter, Item newItem) {
        if(newItem.itemType != itemType) {
            return;
        }
        UpdateDisplayForNewItem(newItem);
        DisplayTooltipOnEquipItem();
    }

    protected void DisplayTooltipOnEquipItem() {
        itemTooltip.ShowImmediate();
        itemTooltip.SetPositionTo(Vector2.zero);
        itemTooltip.Unroll(tooltipFadeInOutDuration.x);
        stopTooltipCoroutine.Start(CStopTooltip());
    }

    protected IEnumerator CStopTooltip() {
        yield return new WaitForSeconds(tooltipDurationOnEquip - tooltipFadeInOutDuration.y);
        itemTooltip.Roll(tooltipFadeInOutDuration.y);
        yield return new WaitForSeconds(tooltipFadeInOutDuration.y);
        itemTooltip.Hide();
    }

    protected void UpdateDisplayForNewItem(Item newItem) {
        if(newItem == null) {
            gameObject.SetActive(false);
            return;
        }
        gameObject.SetActive(true);

        itemImage.sprite = newItem.sprite;
        itemImage.GetComponent<AspectRatioFitter>().aspectRatio = itemImage.sprite.rect.width / itemImage.sprite.rect.height;

        itemTooltip.message = GetItemModifiersName(newItem);
        itemTooltip.message += GetItemShortDescription(newItem);
        itemTooltip.message += GetItemModifiersDescription(newItem);
        UpdateDisplayGemsForNewItem(newItem);
        UpdateBorder(newItem);
    }

    protected string GetItemShortDescription(Item newItem) {
        return UIHelper.SurroundWithFontSize(newItem.description + "\n\n", fontSizeShortDescription);
    }

    protected void UpdateBorder(Item item) {
        Rarity rarity = item.Rarity();
        Material material = RarityToMaterial(borderRarityMaterials, rarity);
        itemBorder.material = material;
    }

    protected void UpdateDisplayGemsForNewItem(Item newItem) {
        foreach (Image gem in gems) {
            Destroy(gem.gameObject);
        }
        gems.Clear();
        foreach (ItemUpgrade upgrade in newItem.GetAllUpgrades()) {
            Image gem = Instantiate(gemPrefab, parent: gemsHolder).GetComponent<Image>();
            gem.sprite = upgrade.gemSprite;
            gem.material = RarityToMaterial(gemRarityMaterials, upgrade.rarity);
            gems.Add(gem);
        }
    }

    protected Material RarityToMaterial(List<Material> materials, Rarity rarity) {
        switch (rarity) {
            case Rarity.COMMON: return materials[0];
            case Rarity.RARE: return materials[1];
            case Rarity.LEGENDARY: return materials[2];
            default: return null;
        }
    }


    public string GetItemModifiersDescription(Item item) {
        string description = "";
        if(item.IsWeapon()) {
            Attack attack = item.AsWeapon().instantiatedAttack.GetComponent<Attack>();
            description = $"{attack.BaseDescription()}\n";
            List<AttackModifier> modifiers = attack.GetModifiers();
            foreach(AttackModifier modifier in modifiers) {
                description += GetModifierDescription(modifier);
            }
        } else {
            foreach(ItemModifier modifier in item.modifiers) {
                description += GetItemOnlyModifierDescription(modifier, notAnUpgrade: true);
            }
        }
        foreach (ItemUpgrade upgrade in item.upgrades) {
            description += GetItemOnlyUpgradeDescription(upgrade);
        }
        return description;
    }

    protected string GetModifierDescription(AttackModifier modifier) {
        string description = modifier.Description();
        if(description == null) {
            return "";
        }
        if(modifier.isUpgrade) {
            description = $"{description} ({RarityHelper.RarityString(modifier.upgradeRarity)})";
        }
        description = $"- {description}\n";
        if(modifier.isUpgrade) {
            Color color = RarityHelper.RarityToList(fontRarityColors, modifier.upgradeRarity);
            description = UIHelper.SurroundWithColorWithoutB(description, color);
        } else {
            description = UIHelper.SurroundWithColorWithoutB(description, fontNormalModifierColor);
        }
        return description;
    }

    protected string GetItemOnlyUpgradeDescription(ItemUpgrade upgrade) {
        return GetItemOnlyModifierDescription(upgrade.modifier);
    }

    protected string GetItemOnlyModifierDescription(ItemModifier modifier, bool notAnUpgrade = false) {
        string description = modifier.Description();
        if(description == null) {
            return "";
        }
        if (!notAnUpgrade) {
            description = $"{description} ({RarityHelper.RarityString(modifier.GetRarity())})";
        }
        description = $"- {description}\n";
        Color color = notAnUpgrade ? fontNormalModifierColor : RarityHelper.RarityToList(fontRarityColors, modifier.GetRarity());
        description = UIHelper.SurroundWithColorWithoutB(description, color);
        return description;
    }

    protected string GetItemModifiersName(Item item) {
        string name = item.name;
        if (item.IsWeapon()) {
            Attack attack = item.AsWeapon().instantiatedAttack.GetComponent<Attack>();
            List<AttackModifier> modifiers = attack.GetModifiers();
            foreach (AttackModifier upgrade in modifiers.FindAll(m => m.isUpgrade)) {
                name = $"{name} {upgrade.DescriptionOneWord()}";
            }
        } else {
            foreach(ItemModifier modifier in item.modifiers) {
                name = $"{name} {modifier.DescriptionOneWord()}";
            }
        }
        foreach (ItemUpgrade upgrade in item.upgrades) {
            name = $"{name} {upgrade.DescriptionOneWord()}";
        }
        name = name + "\n\n";
        return UIHelper.SurroundWithFontSize(name, fontSizeTitleCoef);
    }
}