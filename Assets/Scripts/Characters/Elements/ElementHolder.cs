using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;

public class ElementHolder : MonoBehaviour {

    public Element elem = Element.NO_ELEMENT;
    public float intensity = 1.0f;

}
