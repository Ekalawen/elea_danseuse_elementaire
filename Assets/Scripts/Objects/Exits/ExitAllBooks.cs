using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.ResourceManagement.AsyncOperations;

public class ExitAllBooks : Exit {

    public override void Initialize(Node startingNode) {
        base.Initialize(startingNode);
        objectManager.onCatchBook.AddListener(OnCatchBook);
    }

    protected void OnCatchBook(Book book) {
        if(objectManager.GetBooks().Count == 1 && !book.WillRepop()) {
            OpenSprite();
        }
    }

    public override bool CanBePickedUpBy(Character character) {
        return base.CanBePickedUpBy(character) && objectManager.GetBooks().Count == 0;
    }
}
