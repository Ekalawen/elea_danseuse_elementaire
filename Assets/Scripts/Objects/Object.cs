using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.ResourceManagement.AsyncOperations;

public abstract class Object : MonoBehaviour {

    public Faction faction;
    public Socle socle;
    public SpriteRenderer spriteRenderer;

    protected GameManager gm;
    protected ObjectManager objectManager;
    protected Node startingNode;
    private bool dontDestroyAfterPickup = false;

    public Vector2 pos {
        get { return new Vector2(transform.position.x, transform.position.y); }
        set { transform.position = value; }
    }

    public virtual void Initialize(Node startingNode) {
        gm = GameManager.Instance;
        this.socle.Initialize();
        this.objectManager = GameManager.Instance.objectManager;
        this.socle.onEnterSameFaction.AddListener(PickUpBy);
        this.startingNode = startingNode;
        pos = startingNode.pos;
        objectManager.objects.Add(this);
    }

    protected void PickUpBy(Socle objectSocle, Character character, Range characterRange) {
        if(!characterRange.IsSocle()) {
            return;
        }
        if(!CanBePickedUpBy(character)) {
            return;
        }
        PickUpBySpecific(objectSocle, character);
        AfterPickUp();
    }

    public virtual bool CanBePickedUpBy(Character character) {
        return true;
    }

    protected abstract void PickUpBySpecific(Socle objectSocle, Character character);

    protected void AfterPickUp() {
        objectManager.onCatchObject.Invoke(this);
        objectManager.onCatchObject2.Invoke();
        if (dontDestroyAfterPickup) {
            dontDestroyAfterPickup = false;
            return;
        }
        objectManager.objects.Remove(this);
        Destroy(gameObject);
    }

    public List<Node> NodesInSocle(float rangeOffset) {
        return socle.NodesIn(rangeOffset);
    }

    protected void DontDestroyAfterPickup() {
        dontDestroyAfterPickup = true;
    }
}
