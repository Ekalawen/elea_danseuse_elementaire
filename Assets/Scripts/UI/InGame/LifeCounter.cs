using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LifeCounter : MonoBehaviour {

    public GameObject fullHeartPrefab;
    public GameObject emptyHeartPrefab;
    public GameObject armorHeartPrefab;

    protected GameManager gm;
    protected UIManager uiManager;
    protected Player player;
    protected List<GameObject> hearts = new List<GameObject>();

    public void Initialize() {
        gm = GameManager.Instance;
        uiManager = gm.uiManager;
        player = gm.player;
        player.health.onChangeLife.AddListener(SetLife);
        SetLife(player.health.CurrentLife());
    }

    protected void SetLife(int currentLife) {
        ClearHearts();
        PopulateHearts();
    }

    protected void PopulateHearts() {
        int nbFullHearts = player.health.CurrentHearts();
        int nbEmptyHearts = player.health.MaxHearts() - nbFullHearts;
        int nbArmorHearts = player.health.CurrentArmor();
        for(int i = 0; i < nbFullHearts; i++) {
            hearts.Add(Instantiate(fullHeartPrefab, parent: transform));
        }
        for(int i = 0; i < nbEmptyHearts; i++) {
            hearts.Add(Instantiate(emptyHeartPrefab, parent: transform));
        }
        for(int i = 0; i < nbArmorHearts; i++) {
            hearts.Add(Instantiate(armorHeartPrefab, parent: transform));
        }
    }

    protected void ClearHearts() {
        foreach(GameObject heart in hearts) {
            Destroy(heart);
        }
        hearts.Clear();
    }
}
