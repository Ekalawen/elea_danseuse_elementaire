﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Range))]
public class AttackRange : Attack {

    public enum TargetMode { CLOSEST, IN_DIRECTION };

    public TargetMode targetMode = TargetMode.CLOSEST;
    [ConditionalHide("targetMode", TargetMode.IN_DIRECTION)]
    public float angleCoef = 1.0f;

    [HideInInspector]
    public int nbTargets = 1;

    [HideInInspector]
    public Range range;

    public override void Initialize(Character owner) {
        this.range = GetComponent<Range>();
        this.range.Initialize();
        base.Initialize(owner);
    }

    protected override bool Trigger() {
        List<Character> targets = GetOpponentsInAttackRange();
        targets = targets.FindAll(t => t != null);
        foreach (Character target in targets) {
            damage.Hit(target);
        }
        return targets.Count > 0;
    }

    protected List<Character> GetOpponentsInAttackRange() {
        List<Character> opponents = range.CurrentSocleCollisionOf<Character>().FindAll(c => FactionHelper.OppositeFactions(c, owner));
        opponents = OrderByTargetMode(opponents);
        return opponents.Take(nbTargets).ToList();
    }

    protected List<Character> OrderByTargetMode(List<Character> opponents) {
        if(targetMode == TargetMode.CLOSEST) {
            return OrderByDistance(opponents);
        } else {
            return OrderByDirection(opponents);
        }
    }

    protected List<Character> OrderByDirection(List<Character> opponents) {
        return opponents.OrderBy(o => ComputeScore(o)).ToList();
    }

    protected float ComputeScore(Character opponent) {
        // Lower is better
        Vector2 currentDirection = GetCurrentPressedDirection();
        Vector2 axe = opponent.pos - owner.pos;
        if(currentDirection == Vector2.zero) {
            // No direction used !
            return Vector2.SqrMagnitude(axe);
        }
        float angle = Vector2.Angle(currentDirection, axe);
        angle = angle / 180 * range.Get(); // Normalize to have the same weight has distance
        float distance = axe.magnitude;
        float score = MathCurves.Linear(distance, angle, angleCoef);
        return score;
    }

    protected Vector2 GetCurrentPressedDirection() {
        Vector2 direction = Vector2.zero;
        if (Input.GetKey(KeyCode.UpArrow)) {
            direction += Vector2.up;
        }
        if(Input.GetKey(KeyCode.DownArrow)) {
            direction += Vector2.down;
        }
        if(Input.GetKey(KeyCode.LeftArrow)) {
            direction += Vector2.left;
        }
        if(Input.GetKey(KeyCode.RightArrow)) {
            direction += Vector2.right;
        }
        if(direction != Vector2.zero) {
            direction = direction.normalized;
        }
        return direction;
    }

    private List<Character> OrderByDistance(List<Character> opponents) {
        return opponents.OrderBy(o => Vector2.SqrMagnitude(o.pos - owner.pos)).ToList();
    }

    public override string BaseDescription() {
        int nbDamages = GetComponent<Damage>().damages;
        string s = nbDamages > 1 ? "s" : "";
        return $"Inflige {nbDamages} dégât{s} jusqu'à {GetComponent<Range>().initialRange} de portée.";
    }

    public override bool CanTrigger() {
        return base.CanTrigger() && range.Get() > 0;
    }
}