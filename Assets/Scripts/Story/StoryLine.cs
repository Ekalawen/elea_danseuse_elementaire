using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;
using UnityEngine.VFX;

[Serializable]
public class StoryLine {

    public StoryCharacter storyCharacter;
    public string text;

    public StoryLine(StoryCharacter storyCharacter, string text) {
        this.storyCharacter = storyCharacter;
        this.text = text;
    }
}
