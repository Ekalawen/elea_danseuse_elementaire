VIANA Même s'il ne s'agit que d'une épée, à l'Académie, son efficacité viendra de la façon dont vous vous en servirez.
VIANA De la façon dont vous la ferez <b>Danser</b> !
VIANA Retenez bien que pour qu'une Danse soit réussit, il faut que vous restiez toujours en mouvement.
VIANA Feintez, fendez, tranchez, soyez vifs comme l'air et ne vous arrêtez jamais !