using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;
using UnityEngine.VFX;

public class StoryCharacter : MonoBehaviour {

    public string id;
    public new string name;
    public Sprite sprite;
    public Color color;
    public Color nameColor;
    public bool useItalic = false;

}
