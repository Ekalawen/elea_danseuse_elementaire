﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : Character {

    protected int id;

    public override void Initialize(Node startNode) {
        base.Initialize(startNode);
        gm.enemyManager.enemies.Add(this);
        id = gm.enemyManager.NextEnemyId();
        name = $"{name}({id})";
    }

    public override void Die() {
        base.Die();
        gm.enemyManager.RemoveDeadEnemy(this);
    }
}
