using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;

public class Character : MonoBehaviour {

    protected const float COEF_SPEED_ON_MOVE_TO_EMPTY_NODE = 10.0f;
    protected const float MIN_DIST_TO_PLAYER_ON_MOVE_TO_EMPTY_NODE = 5.0f;

    public enum State { ON_NODE, ON_EDGE };

    public Faction faction = Faction.ENEMY;
    public ElementHolder element;
    public Movement movement;
    public CharacterRenderer characterRenderer;
    public Socle socle;
    public CharacterController controller;
    public Health health;
    public Damage meleeDamages;
    public List<Attack> attacks;

    protected GameManager gm;
    protected Node currentNode = null;
    protected Node targetingNode = null;
    [HideInInspector]
    public State state;
    [HideInInspector]
    public UnityEvent<Node> onStartMove = new UnityEvent<Node>();
    [HideInInspector]
    public UnityEvent<Node> onReachNode = new UnityEvent<Node>();
    [HideInInspector]
    public UnityEvent<Character, Character, Damage> onHitBy = new UnityEvent<Character, Character, Damage>(); // character / hitter / Damage used
    [HideInInspector]
    public UnityEvent<Character, Character, Damage> onHitByAndDamaged = new UnityEvent<Character, Character, Damage>(); // character / hitter / Damage used
    [HideInInspector]
    public UnityEvent<Character> onKilled = new UnityEvent<Character>();
    [HideInInspector]
    public UnityEvent<Character, Character> onKilledBy = new UnityEvent<Character, Character>(); // character / killer
    [HideInInspector]
    public UnityEvent<Character> onKill = new UnityEvent<Character>();
    [HideInInspector]
    public UnityEvent<Character> onHit = new UnityEvent<Character>();
    [HideInInspector]
    public UnityEvent<Character, Attack> onEquipNewAttack = new UnityEvent<Character, Attack>();

    protected SingleCoroutine movingCoroutineSingleton;
    protected BoolTimer hasTakenHit;
    protected List<CharacterModifier> characterModifiers;

    public Vector2 pos {
        get { return new Vector2(transform.position.x, transform.position.y); }
        set { transform.position = value; }
    }


    public virtual void Initialize(Node startNode) {
        gm = GameManager.Instance;
        movingCoroutineSingleton = new SingleCoroutine(this);
        socle.Initialize();
        movement.Initialize(this);
        InitializeAttacks();
        InitializeOnKill();
        TeleportTo(startNode);
        controller.Initialize(this);
        health.Initialize(this);
        InitializeCharacterModifiers();
        characterRenderer.Initialize(this);
    }

    protected void InitializeCharacterModifiers() {
        characterModifiers = GetComponentsInChildren<CharacterModifier>().ToList();
        foreach(CharacterModifier modifier in characterModifiers) {
            modifier.Initialize(this);
        }
    }

    protected void InitializeOnKill() {
        if(meleeDamages) {
            meleeDamages.onKill.AddListener(onKill.Invoke);
        }
        if(meleeDamages) {
            meleeDamages.onHit.AddListener(onHit.Invoke);
        }
    }

    protected void InitializeAttacks() {
        meleeDamages.Initialize(this);
        foreach(Attack attack in attacks) {
            attack.Initialize(this);
        }
    }

    public void MoveBackFrom(Character fromOther) {
        List<Character> others = socle.CurrentCollisionOf<Character>().FindAll(c => c != fromOther);
        if (others.Count > 0) {
            // If I am already colliding with someone else ...
            // Then I can moveBack only if those someone else aren't targeting for where I will go back !
            if (others.Any(o => o.InCurrentOrTargeting(CurrentNode()))) {
                MoveToEmptyNode();
                return;
            }
        }
        if(CurrentNode() == TargetingNode()) {
            MoveAwayFrom(fromOther);
            return;
        }
        MoveBack();
    }

    private void MoveAwayFromOnNode(Character other) {
        Assert.IsTrue(state == State.ON_NODE);
        List<Node> voisins = CurrentNode().Voisins();
        Node bestMoveAwayVoisin = voisins.OrderByDescending(v => Vector2.Angle(v.pos - pos, other.pos - pos)).First();
        
        // If event the best option doesn't make us walk away from the target, then we need to move to an empty node !
        if(Vector2.Angle(bestMoveAwayVoisin.pos - pos, other.pos - pos) < 90) {
            MoveToEmptyNode();
        } else {
            MoveTo(bestMoveAwayVoisin);
        }
    }

    private void MoveAwayFromOnEdge(Character other) {
        Assert.IsTrue(state == State.ON_EDGE);
        Node backTargetingNode = CurrentNode();
        if(Vector2.Angle(backTargetingNode.pos - pos, other.pos - pos) < 90) {
            MoveBackFrom(other);
        } else {
            MoveToEmptyNode();
        }
    }

    public void MoveAwayFrom(Character other) {
        if(state == State.ON_NODE) {
            MoveAwayFromOnNode(other);
        } else {
            MoveAwayFromOnEdge(other);
        }
    }

    protected bool InCurrentOrTargeting(Node node) {
        bool res = CurrentNode() == node || TargetingNode() == node;
        return res;
    }

    public void MoveBack() {
        Node tmp = TargetingNode();
        targetingNode = CurrentNode();
        currentNode = tmp;
        MoveTo(targetingNode);
    }

    public void MoveToEmptyNode() {
        movement.MultiplySpeedBy(COEF_SPEED_ON_MOVE_TO_EMPTY_NODE);
        onReachNode.AddListener(DivideSpeedOnReachNodeOnce); // Bug : this can't be add twice !
        List<Node> emptyNodes = gm.graph.EmptyNodes(socle.Get() / 2.0f);
        if(faction == Faction.ENEMY && gm.player != null) {
            emptyNodes = gm.graph.GetAllAtLeastAsFarFrom(emptyNodes, gm.player.pos, MIN_DIST_TO_PLAYER_ON_MOVE_TO_EMPTY_NODE);
        }
        if(emptyNodes.Count == 0) {
            return;
        }
        Node closestEmptyNode = MathTools.Argmin(emptyNodes, n => Vector2.SqrMagnitude(n.pos - pos));
        currentNode = ClosestNode();
        MoveTo(closestEmptyNode);
    }

    protected void DivideSpeedOnReachNodeOnce(Node node) {
        movement.MultiplySpeedBy(1 / COEF_SPEED_ON_MOVE_TO_EMPTY_NODE);
        onReachNode.RemoveListener(DivideSpeedOnReachNodeOnce);
    }

    public void MoveTo(Node node) {
        movingCoroutineSingleton.Start(CMoveTo(node));
    }

    protected IEnumerator CMoveTo(Node node) {
        Vector2 initialPos = pos;
        float distance = Vector2.Distance(initialPos, node.pos);
        float duration = movement.GetModifiedDuration(distance);
        targetingNode = node;
        onStartMove.Invoke(node);

        Timer timer = new Timer(duration);
        state = State.ON_EDGE;
        while(!timer.IsOver() && duration > 0) {
            float avancement = movement.speedCurve.Evaluate(timer.GetAvancement());
            pos = Vector2.Lerp(initialPos, node.pos, avancement);
            yield return null;
        }

        TeleportTo(node);
    }

    public void TeleportTo(Node node) {
        state = State.ON_NODE;
        currentNode = node;
        targetingNode = node;
        pos = node.pos;
        movingCoroutineSingleton.Stop();
        onReachNode.Invoke(node);
    }

    public bool CanAttack() {
        return attacks.First().CanTrigger();
    }

    public virtual bool Attack() {
        if(attacks.Count == 0) {
            return false;
        }
        return attacks.First().TryTrigger();
    }

    public Node CurrentNode() {
        if(!currentNode) {
            currentNode = ClosestNode();
        }
        return currentNode;
    }

    public Node ClosestNode() {
        return gm.graph.ClosestTo(pos);
    }

    public Node ClosestAndSafeNode() {
        return gm.graph.ClosestAndSafeNodeTo(pos);
    }

    public Node TargetingNode() {
        if(!targetingNode) {
            targetingNode = CurrentNode();
        }
        return targetingNode;
    }

    public Node ClosestBetweenCurrentAndTargeting() {
        float distToCurrent = Vector2.SqrMagnitude(pos - CurrentNode().pos);
        float distToTargeting = Vector2.SqrMagnitude(pos - TargetingNode().pos);
        if (distToCurrent <= distToTargeting) {
            return CurrentNode();
        }
        return TargetingNode();
    }

    public virtual void Die() {
        onKilled.Invoke(this);
        Destroy(gameObject);
        //Destroy(socle.gameObject);
        //Destroy(characterRenderer.gameObject);
        //Destroy(gameObject, CharacterRenderer.TIME_AFTER_DEATH);
    }

    public virtual void MeleeAttack(Character target) {
        if (meleeDamages) {
            meleeDamages.Hit(target);
        }
    }


    public List<Node> NodesInSocle(float soclesOffset) {
        return socle.NodesIn(soclesOffset);
    }


    public bool TargetSameNodeAs(Character other) {
        return TargetingNode() == other.TargetingNode();
    }

    public bool TargetOppositeNodesAs(Character other) {
        return TargetingNode() == other.CurrentNode() && CurrentNode() == other.TargetingNode();
    }

    public bool IsBiggerThan(Character other) {
        return Size() > other.Size();
    }

    public bool IsSmallerThan(Character other) {
        return Size() < other.Size();
    }

    public Character Smaller(Character other) {
        return IsSmallerThan(other) ? this : other;
    }

    public Character Bigger(Character other) {
        return IsBiggerThan(other) ? this : other;
    }

    public bool IsSameSizeAs(Character other) {
        return Size() == other.Size();
    }

    public float Size() {
        return socle.Get();
    }

    public void EquipAttack(Attack newAttack) {
        newAttack.transform.SetParent(transform);
        if(attacks.Count == 0) {
            attacks.Add(newAttack);
        } else {
            Destroy(attacks.First().gameObject);
            attacks[0] = newAttack;
        }
        newAttack.Initialize(this);
        onEquipNewAttack.Invoke(this, newAttack);
    }

}
