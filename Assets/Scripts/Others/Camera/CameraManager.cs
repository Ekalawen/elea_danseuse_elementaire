using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour {

    public CameraController cameraController;

    protected GameManager gm;

    public void Initialize() {
        gm = GameManager.Instance;
        cameraController.Initialize();
    }
}
