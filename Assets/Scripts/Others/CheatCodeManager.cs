using EZCameraShake;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CheatCode {
    public List<KeyCode> code;
    public Action action;
    public int state = 0;
}

public class CheatCodeManager : MonoBehaviour
{

    [Header("Cheat Codes")]
    public List<KeyCode> firstCode;

    protected GameManager gm;
    protected List<CheatCode> cheatCodes;
    [HideInInspector]
    public UnityEvent onUseCheatCode;

    public void Initialize()
    {
        gm = GameManager.Instance;
        cheatCodes = new List<CheatCode>();

        CheatCode quitCode = new CheatCode();
        quitCode.code = this.firstCode;
        quitCode.action = gm.QuitGame;
        cheatCodes.Add(quitCode);

    }

    public void Update()
    {
        foreach (CheatCode cheatCode in cheatCodes)
        {
            KeyCode currentKey = cheatCode.code[cheatCode.state];
            if (Input.GetKeyDown(currentKey))
            {
                cheatCode.state += 1;
                if (cheatCode.state == cheatCode.code.Count)
                {
                    cheatCode.state = 0;
                    cheatCode.action.Invoke();
                    onUseCheatCode.Invoke();
                }
            }
            else if (Input.anyKeyDown)
            {
                cheatCode.state = 0;
            }
        }
    }
}
