﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;

public class AttackModifier_AdditionnalDamage : AttackModifier {

    public int nbAdditionalDamage = 1;

    public override void Initialize(Attack attack, bool isUpgrade = false, Rarity upgradeRarity = Rarity.COMMON) {
        base.Initialize(attack, isUpgrade, upgradeRarity);
        attack.damage.damages += nbAdditionalDamage;
    }

    public override string Description() {
        string s = nbAdditionalDamage > 1 ? "s" : "";
        return $"{nbAdditionalDamage} dégât{s} supplémentaire{s}";
    }

    public override string DescriptionOneWord() {
        return $"+{nbAdditionalDamage}";
    }
}