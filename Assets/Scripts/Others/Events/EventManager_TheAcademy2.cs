﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;

public class EventManager_TheAcademy2 : EventManager {

    [Header("Phase 2 : destroy pots")]
    public AutomaticStory phase2Story;
    public Spawner phase2Objects;
    public Spawner phase2Enemies;

    [Header("Phase 3 : FireWisps spawning")]
    public int nbEnemiesInPhase3 = 10;
    public AutomaticStory phase3Story;
    public Spawner phase3EnemiesInitial;
    public Spawner phase3EnemiesOnNothingLeft;

    [Header("Phase 4 : more FireWisps")]
    public float phase4Duration = 10.0f;
    public Spawner phase4Enemies;

    protected int startPhase3KillCount;

    public override void Initialize() {
        base.Initialize();
        GoToPhase1();
        gm.eventManager.onNothingLeft.AddListener(GoToPhase2);
    }

    public override void StartPhase2() {
        phase2Story.InitializeAndTryTrigger();
        gm.objectManager.AddSpawner(phase2Objects);
        gm.enemyManager.AddSpawner(phase2Enemies);
        gm.eventManager.onNothingLeft.AddListener(GoToPhase3);
    }

    public override void StartPhase3() {
        phase3Story.InitializeAndTryTrigger();
        gm.enemyManager.AddSpawner(phase3EnemiesInitial);
        gm.enemyManager.AddSpawner(phase3EnemiesOnNothingLeft);
        startPhase3KillCount = gm.enemyManager.GetKillCount();
        gm.enemyManager.onEnemyDie.AddListener(CheckNumberOfKill);
    }

    protected void CheckNumberOfKill(Enemy enemy) {
        if(gm.enemyManager.GetKillCount() == startPhase3KillCount + nbEnemiesInPhase3) {
            GoToPhase4();
        }
    }

    public override void StartPhase4() {
        gm.enemyManager.RemoveSpawner(phase3EnemiesOnNothingLeft);
        gm.enemyManager.AddSpawner(phase4Enemies);
        GoToPhaseIn(5, phase4Duration);
    }

    public override void StartPhase5() {
        gm.enemyManager.RemoveSpawner(phase4Enemies);
        onNothingLeft.AddListener(gm.objectManager.OpenExit);
    }
}