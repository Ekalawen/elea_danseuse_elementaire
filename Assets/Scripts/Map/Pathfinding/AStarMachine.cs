using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class AStarMachine<N> where N : class, INode, new() {

    protected Graph graph;
    protected Func<IList<N>, IList<N>> failFunction;
    protected Func<N, bool> stopSearchFunction;
    protected N start = null;
    protected N end = null;
    protected float currentHue = 0.0f;
    protected bool colorGraph = false;

    public AStarMachine(Graph graph, Node start) {
        this.graph = graph;
        this.failFunction = defaultFailFunction;
        this.stopSearchFunction = defaultStopSearchFunction;
        this.start = new N();
        this.start.Initialize(start, null, null);
    }

    public AStarMachine<N> Target(Node end) {
        this.end = new N();
        this.end.Initialize(end, null, null);
        Node startNode = start.node;
        this.start = new N();
        this.start.Initialize(startNode, null, this.end);
        this.stopSearchFunction = GetStopSearchAtTargetFunction(end);
        return this;
    }

    public AStarMachine<N> Start(Node start) {
        this.start = new N();
        this.start.Initialize(start, null, null);
        return this;
    }

    public AStarMachine<N> StopSearchFunction(Func<N, bool> stopSearchFunction) {
        this.stopSearchFunction = stopSearchFunction;
        return this;
    }

    public AStarMachine<N> FailFunction(Func<IList<N>, IList<N>> failFunction) {
        this.failFunction = failFunction;
        return this;
    }

    public AStarMachine<N> ColorGraph(bool value=true) {
        this.colorGraph = value;
        return this;
    }

    public IList<N> Compute() {
        return Compute(start, end);
    }

    public List<Node> ComputeNodes() {
        return Compute(start, end).Select(n => n.node).ToList();
    }

    protected IList<N> Compute(N start, N end) {
        IList<N> opened = new List<N>() { start };
        IList<N> closed = new List<N>();
        currentHue = 0.0f;

        while(opened.Count > 0)
        {
            N current = opened.Last();
            ColorNode(current);

            if (stopSearchFunction(current)) {
                return current.Backtrack().Cast<N>().ToList();
            }

            IList<N> voisins = current.Voisins(end).Cast<N>().ToList();

            foreach (N voisin in voisins)
            {
                bool inClosed = closed.Contains(voisin);
                bool isBetterPlacedInOpened = BetterPlacedIn(opened, voisin);
                if (!inClosed && !isBetterPlacedInOpened)
                {
                    InsertAndRemoveOldOneIn(opened, voisin);
                }
            }

            closed.Add(current);
            opened.Remove(current);
        }

        // End function
        IList<N> failResult = new N().FailResult(closed.Cast<INode>().ToList()).Cast<N>().ToList();
        return failResult;
    }

    protected void ColorNode(N current) {
        if (!colorGraph)
            return;
        current.node.SetColorHue(currentHue);
        currentHue = (currentHue + 0.025f) % 1.0f;
    }

    protected bool BetterPlacedIn(IList<N> opened, N voisin) {
        return opened.FirstOrDefault(o => o.node == voisin.node && o.heuristic <= voisin.heuristic) != null;
    }

    protected void InsertAndRemoveOldOneIn(IList<N> opened, N voisin) {
        bool isInserted = false;
        for(int i = 0; i < opened.Count; i++) {
            N current = opened[i];

            if(!isInserted && voisin.heuristic > current.heuristic) {
                opened.Insert(i, voisin);
                isInserted = true;
                i++;
            }

            if(current.Equals(voisin)) {
                opened.Remove(current);
                i--;
            }
        }

        if(!isInserted) { // empty list
            opened.Add(voisin);
        }
    }

    public IList<N> defaultFailFunction(IList<N> closed) {
        return new List<N>();
    }

    public Func<N, bool> GetStopSearchAtTargetFunction(Node target) {
        return current => current.node == target;
    }

    public bool defaultStopSearchFunction(N current) {
        return false;
    }
}
