﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AttackModifier_SwapPlaces : AttackModifier_OnEvent {

    public bool teleport = false;
    public bool canSwapWithSanctuaries = false;

    public override void Initialize(Attack attack, bool isUpgrade = false, Rarity upgradeRarity = Rarity.COMMON) {
        base.Initialize(attack, isUpgrade, upgradeRarity);
        AddListenerToEvents(SwapPlace);
    }

    public void SwapPlace(Character other) {
        if(other.GetComponent<Sanctuary>() != null) {
            return;
        }
        Node closestNode = other.ClosestNode();
        Node myClosestNode = attack.owner.ClosestNode();
        if (teleport) {
            attack.owner.TeleportTo(closestNode);
            other.TeleportTo(myClosestNode);
        } else {
            attack.owner.MoveTo(closestNode);
            other.MoveTo(myClosestNode);
        }
    }

    public override string Description() {
        string action = teleport ? "via une téléportation" : "";
        return $"Échange de place avec la cible {action} {DescriptionOnEvent()}";
    }

    public override string DescriptionOneWord() {
        return "d'échange";
    }
}