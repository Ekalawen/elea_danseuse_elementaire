﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;

public class GoToPlayerController : CharacterController {

    protected Player player;

    public override void Initialize(Character character) {
        base.Initialize(character);
        player = gm.player;
        character.onReachNode.AddListener(WaitOnReachNode);
        character.socle.onEnterSameFaction.AddListener(GoBackwardOnCollisionWithAlly);
        character.onReachNode.AddListener(CheckNotAnotherPersonHere);
        Wait(character.movement.waitingTime);
    }

    protected override void UpdateMouvement() {
        if(character.state == Character.State.ON_NODE) {
            GoToPlayer();
        }
    }

    protected void GoToPlayer() {
        Node playerNode = player.CurrentNode();
        List<Node> path = new AStarMachine<APath>(graph, character.CurrentNode()).Target(playerNode).ComputeNodes();
        if(path == null || path.Count == 0) {
            return;
        }
        character.MoveTo(path.First());
    }

    protected void WaitOnReachNode(Node node) {
        Wait(character.movement.waitingTime);
    }

    protected void GoBackwardOnCollisionWithAlly(Socle thisSocle, Character other, Range otherRange) {
        if(character.IsSmallerThan(other)) {
            GoBackwardOnCollisionWithAllyOfBiggerSize(other);
        } else if(character.IsBiggerThan(other)) {
            GoBackwardOnCollisionWithAllyOfSmallerSize(other);
        } else { // Same size
            GoBackwardOnCollisionWithAllyOfSameSize(other);
        }
    }

    protected void GoBackwardOnCollisionWithAllyOfBiggerSize(Character bigger) {
        // Always let priority to big guys ! :D
        character.MoveBackFrom(bigger);
    }

    protected void GoBackwardOnCollisionWithAllyOfSmallerSize(Character smaller) {
        // Ignore it ! :p
    }

    protected void GoBackwardOnCollisionWithAllyOfSameSize(Character other) {
        if (character.Size() > 1) {
            GoBackwardOnCollisionWithAllyOfSameSizeSuperiorToNormalSize(other);
        } else if(character.TargetSameNodeAs(other)) {
            GoBackwardOnCollisionWithAllyOfSameSizeTargetingSameNode(other);
        } else if(character.TargetOppositeNodesAs(other)) {
            GoBackwardOnCollisionWithAllyOfSameSizeTargetingOpposingNodes(other);
        } else {
            // Collision but not on the same edge and of small size ==> It's not an issue :)
        }
    }

    protected void GoBackwardOnCollisionWithAllyOfSameSizeTargetingOpposingNodes(Character other) {
        Node myTargetingNode = character.TargetingNode();
        // Si ils se bloquent l'un l'autre
        // (l'autre cas étant quand un rapide rattrape un lent sur la même arrête, alors juste le rapide doit retourner en arrière)
        if (Vector2.SqrMagnitude(character.pos - myTargetingNode.pos) >= Vector2.SqrMagnitude(other.pos - myTargetingNode.pos)) {
            character.MoveBackFrom(other); // And the other will do the same, so they will both bounce :)
        }
    }

    protected void GoBackwardOnCollisionWithAllyOfSameSizeTargetingSameNode(Character other) {
        Node myTargetingNode = character.TargetingNode();
        if (Vector2.SqrMagnitude(character.pos - myTargetingNode.pos) >= Vector2.SqrMagnitude(other.pos - myTargetingNode.pos)) {
            character.MoveBackFrom(other);
        }
    }

    protected void GoBackwardOnCollisionWithAllyOfSameSizeSuperiorToNormalSize(Character other) {
        Node myTargetingNode = character.TargetingNode();
        Node otherTargetingNode = other.TargetingNode();
        if (Vector2.SqrMagnitude(character.pos - myTargetingNode.pos) >= Vector2.SqrMagnitude(other.pos - otherTargetingNode.pos)) {
            character.MoveBackFrom(other);
        }
    }

    protected void CheckNotAnotherPersonHere(Node node) {
        List<Character> others = character.socle.CurrentCollisionOf<Character>();
        others = others.FindAll(o => FactionHelper.SameFactions(o, character) && o != this);

        // Si ils sont plus petits, on les sorts
        foreach(Character other in others) {
            if(other.IsSmallerThan(character)) {
                other.MoveAwayFrom(character);
            }
        }

        // Si il est de même taille, mais qu'il est gros, alors il sort
        if (character.Size() > 1) {
            List<Character> otherBigs = others.FindAll(o => o.IsSameSizeAs(character));
            foreach(Character otherBig in otherBigs) {
                otherBig.MoveAwayFrom(character);
            }
        }

        // Si on est plus petit que l'un d'eux, on sort
        List<Character> biggerOthers = others.FindAll(o => o.IsBiggerThan(character));
        if(biggerOthers.Count > 0) {
            Character closestBigger = biggerOthers.OrderBy(o => Vector2.SqrMagnitude(o.pos - character.pos)).First();
            character.MoveAwayFrom(closestBigger);
            return;
        }

        // Si il y a déjà quelqu'un exactement ici, le plus petit (ou le précédent) sort !
        List<Character> othersAtExactSamePos = others.FindAll(o => o.pos == node.pos);
        if(othersAtExactSamePos.Count > 0) {
            Character smaller = character.Smaller(othersAtExactSamePos.First());
            smaller.MoveToEmptyNode();
            if(smaller == character) {
                return;
            }
        }
    }
}
