﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;

public class AttackModifier_HammerRange : AttackModifier_OnEvent {

    public float cooldownDuration = 1.0f;
    public float rangeIncreaseByAdditionalTarget = 1.0f;
    public float maxAdditionalRange = 5.0f;
    public float timeBeforeShrinkingAgain = 0.5f;
    public float decreaseDuration = 0.8f;

    protected AttackRange attackRange;
    protected Timer lastTimeApplied;
    protected SingleCoroutine singleCoroutine;
    protected float initialRange;

    public override void Initialize(Attack attack, bool isUpgrade = false, Rarity upgradeRarity = Rarity.COMMON) {
        base.Initialize(attack, isUpgrade, upgradeRarity);
        this.attackRange = (AttackRange)attack;
        Assert.IsNotNull(attackRange);
        lastTimeApplied = new Timer();
        singleCoroutine = new SingleCoroutine(this);
        initialRange = attackRange.range.initialRange;
        AddListenerToEvents(IncreaseRange);
    }

    public void IncreaseRange(Character other) {
        if (lastTimeApplied.GetElapsedTime() > 0) {
            singleCoroutine.Start(CIncreaseRangeForDuration());
        }
    }

    protected IEnumerator CIncreaseRangeForDuration() {
        lastTimeApplied.Reset();
        int nbOfAdditionnalTargets = attackRange.range.CurrentCollisionOf<Character>().FindAll(c => FactionHelper.OppositeFactions(c, attack.owner)).Count - 1;
        attackRange.range.rangeModifier.SetSizeModifier(0.0f);
        yield return new WaitForSeconds(cooldownDuration);
        float additionnalRange = Mathf.Min(maxAdditionalRange, nbOfAdditionnalTargets * rangeIncreaseByAdditionalTarget);
        attackRange.range.rangeModifier.SetSizeModifier(initialRange + additionnalRange);
        yield return new WaitForSeconds(timeBeforeShrinkingAgain);
        Timer timer = new Timer(decreaseDuration);
        while(!timer.IsOver()) {
            float avancement = timer.GetAvancement();
            float newRange = MathCurves.Linear(initialRange + additionnalRange, initialRange, avancement);
            attackRange.range.rangeModifier.SetSizeModifier(newRange);
            yield return null;
        }
        attackRange.range.rangeModifier.SetSizeModifier(initialRange);
    }

    public override string Description() {
        return $"Cooldown de {cooldownDuration}s et {rangeIncreaseByAdditionalTarget} de portée supplémentaire par cibles additionnelles";
    }

    public override string DescriptionOneWord()
    {
        return "MARTEAU";
    }
}