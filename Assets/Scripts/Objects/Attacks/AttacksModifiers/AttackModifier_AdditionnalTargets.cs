﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;

public class AttackModifier_AdditionnalTargets : AttackModifier {

    public int nbAdditionalTargets = 1;

    protected AttackRange attackRange;

    public override void Initialize(Attack attack, bool isUpgrade = false, Rarity upgradeRarity = Rarity.COMMON) {
        base.Initialize(attack, isUpgrade, upgradeRarity);
        this.attackRange = (AttackRange)attack;
        Assert.IsNotNull(attackRange);
        this.attackRange.nbTargets += nbAdditionalTargets;
    }

    public override string Description() {
        string s = nbAdditionalTargets > 1 ? "s" : "";
        return $"{nbAdditionalTargets} cible{s} supplémentaire{s}";
    }

    public override string DescriptionOneWord() {
        if (nbAdditionalTargets == 1) return "double";
        if (nbAdditionalTargets == 2) return "triple";
        if (nbAdditionalTargets == 3) return "quadruple";
        if (nbAdditionalTargets == 4) return "quintuple";
        return "multiple";
    }

}