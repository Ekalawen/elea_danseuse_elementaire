﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;

public class HealthModifier_SwapWithAttackerOnHit : HealthModifier {

    protected GameManager gm;

    public override void Initialize(Health health) {
        base.Initialize(health);
        gm = GameManager.Instance;
        health.Character().onHitBy.AddListener(OnHitBy);
    }

    protected void OnHitBy(Character thisCharacter, Character attacker, Damage damageUsed) {
        Node attackerClosestNode = attacker.ClosestNode();
        Node myClosestNode = gm.graph.ClosestToNotIn(thisCharacter.ClosestNode().pos, attackerClosestNode);
        attacker.TeleportTo(myClosestNode);
        thisCharacter.TeleportTo(attackerClosestNode);
    }
}