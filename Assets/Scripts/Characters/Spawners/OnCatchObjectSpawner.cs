﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnCatchObjectSpawner : Spawner {

    public bool onlyOnce = true;
    public float delayOnSpawn = 0.0f;

    protected bool hasSpawn = false;

    public override void Initialize(SpawnerManager manager) {
        base.Initialize(manager);
        gm.objectManager.onCatchObject.AddListener(SpawnIn);
    }

    protected void SpawnIn(Object obj) {
        if(hasSpawn) {
            return;
        }
        hasSpawn = true;
        StartCoroutine(CSpawnIn());
    }

    protected IEnumerator CSpawnIn() {
        yield return new WaitForSeconds(delayOnSpawn);
        Spawn();
    }
}
