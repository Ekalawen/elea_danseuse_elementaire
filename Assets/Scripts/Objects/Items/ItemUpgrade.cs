﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

public class ItemUpgrade : MonoBehaviour {

    public Rarity rarity = Rarity.COMMON;
    public Sprite gemSprite;
    public ItemModifier modifier;

    protected Item item;

    public virtual void Initialize(Item item) {
        this.item = item;
        InitializeModifierForItemOnly(item);
    }

    protected virtual void InitializeModifierForItemOnly(Item item) {
        modifier.Initialize(item, isUpgrade: true, upgradeRarity: rarity);
    }

    public virtual void OnUnEquip() {
        modifier.OnUnEquip();
    }

    public virtual string Description() {
        return modifier.Description();
    }

    public virtual string DescriptionOneWord() {
        return modifier.DescriptionOneWord();
    }

    public bool IsItemUpgrade() {
        return true;
    }

    public bool IsWeaponUpgrade() {
        return GetComponent<WeaponUpgrade>() != null;
    }

    public bool IsItemOnlyUpgrade() {
        return !IsWeaponUpgrade();
    }
}