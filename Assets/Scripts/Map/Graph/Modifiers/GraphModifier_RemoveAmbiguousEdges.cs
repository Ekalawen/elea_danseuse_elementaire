using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GraphModifier_RemoveAmbiguousEdges : GraphModifier {
    public override void Apply() {
        foreach(Node node in graph.nodes) {
            RemoveAmbiguities(node);
        }
    }

    protected void RemoveAmbiguities(Node node) {
        Dictionary<Vector2, List<Edge>> edgesMap = node.MapEdgesInclusif();
        foreach(KeyValuePair<Vector2, List<Edge>> pair in edgesMap) {
            Vector2 normal = pair.Key;
            List<Edge> affectedEdges = pair.Value.OrderBy(e => Vector2.Angle(e.From(node), normal)).ToList();
            for(int i = 1; i < affectedEdges.Count; i++) {
                DeleteEdge(affectedEdges[i]);
            }
        }
    }
}
