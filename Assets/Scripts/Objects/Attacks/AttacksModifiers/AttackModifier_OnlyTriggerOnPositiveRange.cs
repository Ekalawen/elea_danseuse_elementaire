﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;

public class AttackModifier_OnlyTriggerOnPositiveRange : AttackModifier {

    protected AttackRange attackRange;

    public override void Initialize(Attack attack, bool isUpgrade = false, Rarity upgradeRarity = Rarity.COMMON) {
        base.Initialize(attack, isUpgrade, upgradeRarity);
        this.attackRange = (AttackRange)attack;
        Assert.IsNotNull(attackRange);
    }

    public override bool CanTrigger() {
        return attackRange.range.Get() > 0;
    }

    public override string Description() {
        return null;
    }

    public override string DescriptionOneWord() {
        return "";
    }
}