using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController_FollowPlayer : CameraController {

    public enum Mode { INTERPOLATE, FOLLOW, PROPORTIONNAL };

    public Mode mode = Mode.INTERPOLATE;
    [ConditionalHide("mode", Mode.INTERPOLATE)]
    public float playerPosCoef = 0.3f;
    [ConditionalHide("mode", Mode.FOLLOW)]
    public float speed = 10.0f;
    [ConditionalHide("mode", Mode.PROPORTIONNAL)]
    public float distanceToSpeedCoef = 0.3f;

    public override void Initialize() {
        base.Initialize();
        camera.transform.position = PlayerPos();
    }

    protected override void UpdateCameraPosition()
    {
        if (mode == Mode.INTERPOLATE) {
            InterpolatePosition();
        } else if (mode == Mode.FOLLOW) {
            FollowPosition();
        } else {
            ProportionnalPosition();
        }
    }

    protected void ProportionnalPosition() {
        if(player == null) {
            return;
        }
        Vector3 playerPos = PlayerPos();
        Vector3 currentPos = camera.transform.position;
        float magnitude = (playerPos - currentPos).magnitude;
        float speed = magnitude * distanceToSpeedCoef;
        Vector3 direction = (playerPos - currentPos).normalized;
        Vector3 movement = direction * speed * Time.deltaTime;
        if(movement.magnitude > magnitude) {
            movement = direction * magnitude;
        }
        camera.transform.position += movement;
    }

    protected void InterpolatePosition()
    {
        Vector3 currentPos = camera.transform.position;
        camera.transform.position = Vector3.Lerp(currentPos, PlayerPos(), playerPosCoef);
    }

    protected void FollowPosition() {
        Vector3 playerPos = PlayerPos();
        Vector3 currentPos = camera.transform.position;
        float magnitude = (playerPos - currentPos).magnitude;
        Vector3 direction = (playerPos - currentPos).normalized;
        Vector3 movement = direction * speed * Time.deltaTime;
        if(movement.magnitude > magnitude) {
            movement = direction * magnitude;
        }
        camera.transform.position += movement;
    }

    protected Vector3 PlayerPos() {
        return new Vector3(player.pos.x, player.pos.y, camera.transform.position.z);
    }
}
