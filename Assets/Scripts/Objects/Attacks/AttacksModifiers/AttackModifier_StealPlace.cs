﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AttackModifier_StealPlace : AttackModifier_OnEvent {

    public bool teleport = false;

    public override void Initialize(Attack attack, bool isUpgrade = false, Rarity upgradeRarity = Rarity.COMMON) {
        base.Initialize(attack, isUpgrade, upgradeRarity);
        AddListenerToEvents(StealPlace);
    }

    public void StealPlace(Character other) {
        Node closestNode = other.ClosestNode();
        if (teleport) {
            attack.owner.TeleportTo(closestNode);
        } else {
            attack.owner.MoveTo(closestNode);
        }
    }

    public override string Description() {
        string action = teleport ? "Téléporte" : "Déplace";
        return $"{action} à la position de la cible {DescriptionOnEvent()}";
    }

    public override string DescriptionOneWord() {
        return "de mouvement";
    }
}