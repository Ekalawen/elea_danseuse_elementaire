using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GraphModifier_EnsureConnectivity : GraphModifier {
    public override void Apply() {
        List<List<Node>> islands = GetIslands();

        ConnectIslands(islands);
    }

    protected void ConnectIslands(List<List<Node>> islands) {
        int kmax = 100;
        for(int k = 0; k < kmax && islands.Count > 1; k++) {
            List<Node> island = islands.First();
            Connect2Islands(island, Complementary(island));
            islands = GetIslands();
        }
    }

    protected void Connect2Islands(List<Node> island1, List<Node> island2) {
        List<Node> small = island1.Count < island2.Count ? island1 : island2;
        List<Node> big = island1.Count >= island2.Count ? island1 : island2;

        foreach(Node smallNode in small) {
            List<Node> bigNodeCandidates = big.FindAll(b => b.CouldBeLinkedWith(smallNode));
            bigNodeCandidates = bigNodeCandidates.OrderBy(b => Vector2.SqrMagnitude(b.pos - smallNode.pos)).ToList();
            foreach(Node bigNodeCandidate in bigNodeCandidates) {
                Edge newEdge = graph.CreateEdge(bigNodeCandidate, smallNode); // This will be careful of intersections ! :)
                if(newEdge != null) {
                    //newEdge.SetColor(0);
                    return;
                }
            }
        }

        // We didn't manage to create an edge ! :'(
        // Debug.Log($"Couldn't connect 2 islands ==> Deleting {small.Count} nodes :'(");
        foreach(Node smallNode in small) {
            graph.DestroyNodeWithEdges(smallNode);
        }
    }

    private List<List<Node>> GetIslands() {
        Node firstNode = graph.nodes.First();
        AStarMachine<AZone> floodFill = new AStarMachine<AZone>(graph, firstNode);
        List<Node> filled = floodFill.ColorGraph().ComputeNodes();
        if(filled.Distinct().ToList().Count != filled.Count) {
            Debug.Log($"Il y a des doublons dans filled ! 1");
        }

        List<List<Node>> islands = new List<List<Node>>() { filled.Select(f => f).ToList() };
        foreach (Node node in graph.nodes) {
            if (!filled.Contains(node)) {
                List<Node> newFilled = floodFill.Start(node).ComputeNodes();
                filled.AddRange(newFilled);
                if(filled.Distinct().ToList().Count != filled.Count) {
                    Debug.Log($"Il y a des doublons dans filled ! 1");
                }
                islands.Add(newFilled);
            }
        }

        return islands;
    }

    protected List<Node> Complementary(List<Node> island) {
        return graph.nodes.FindAll(n => !island.Contains(n));
    }
}
