﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnNothingLeftSpawner : Spawner {

    public bool notOnFirstTime = false;
    [ConditionalHide("notOnFirstTime")]
    public int onNTimes = 1;

    protected int nbTimes = 0;
    protected SingleCoroutine singleCoroutine;

    public override void Initialize(SpawnerManager manager) {
        base.Initialize(manager);
        singleCoroutine = new SingleCoroutine(this);
        gm.objectManager.onCatchObject.AddListener(OnCatchObject);
        gm.enemyManager.onEnemyDie.AddListener(OnEnemyDie);
    }

    protected void OnEnemyDie(Enemy enemy) {
        singleCoroutine.Start(COnSomethingLeft());
    }

    protected void OnCatchObject(Object obj) {
        singleCoroutine.Start(COnSomethingLeft());
    }

    protected IEnumerator COnSomethingLeft() {
        yield return null; // Wait 1 frame for the thing to be destroyed/removed
        if (gm.objectManager.objects.Count <= 1  // <= 1 because of the exit ^^'
        && gm.enemyManager.enemies.Count == 0) {
            TrySpawn();
        }
    }

    protected void TrySpawn() {
        nbTimes++;
        if (notOnFirstTime && nbTimes < onNTimes) {
            return;
        }
        Spawn();
    }

    public override void Stop() {
        base.Stop();
        gm.objectManager.onCatchObject.RemoveListener(OnCatchObject);
        gm.enemyManager.onEnemyDie.RemoveListener(OnEnemyDie);
    }
}
