using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class CameraController : MonoBehaviour {

    protected new Camera camera;
    protected GameManager gm;
    protected Player player;

    public virtual void Initialize() {
        camera = Camera.main;
        gm = GameManager.Instance;
        player = gm.player;
    }

    public void Update() {
        UpdateCameraPosition();
    }

    protected abstract void UpdateCameraPosition();
}
