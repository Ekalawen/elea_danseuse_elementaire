using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;

public class ElementManager : MonoBehaviour {

    public static float MAX_COUNT = 100.0f;

    [Header("Fire")]
    public float fireCountByHit = 10.0f;
    public float fireCountDecreaseSpeed = 25.0f;
    public Damage fireDamage;

    [Header("Water")]
    public float waterCountByHit = 10.0f;
    public float waterCountDecreaseSpeed = 25.0f;
    public float waterExplodeDuration = 3.0f;
    public float maxRangeDecreaseWhileCounting = 0.5f;

    [HideInInspector]
    public UnityEvent<Player, Element> onSufferElement = new UnityEvent<Player, Element>();
    [HideInInspector]
    public UnityEvent<Player> onSufferFire = new UnityEvent<Player>();
    [HideInInspector]
    public UnityEvent<Player> onSufferWater = new UnityEvent<Player>();
    [HideInInspector]
    public UnityEvent<Player> onSufferPlant = new UnityEvent<Player>();

    [HideInInspector]
    public UnityEvent<Player> onStartSufferFire = new UnityEvent<Player>();
    [HideInInspector]
    public UnityEvent<Player> onStopSufferFire = new UnityEvent<Player>();
    [HideInInspector]
    public UnityEvent<Player> onFireExplode = new UnityEvent<Player>();

    [HideInInspector]
    public UnityEvent<Player> onStartSufferWater = new UnityEvent<Player>();
    [HideInInspector]
    public UnityEvent<Player> onStopSufferWater = new UnityEvent<Player>();
    [HideInInspector]
    public UnityEvent<Player> onWaterExplode = new UnityEvent<Player>();

    protected GameManager gm;
    protected float fireCount = 0;
    protected float waterCount = 0;
    protected float plantCount = 0;
    protected SingleCoroutine fireDecreaseCoroutine;
    protected SingleCoroutine waterDecreaseCoroutine;
    protected SingleCoroutine plantDecreaseCoroutine;
    protected FloatModifier waterDecreaseRangeMultiplier;

    public void Initialize() {
        gm = GameManager.Instance;
        fireDecreaseCoroutine = new SingleCoroutine(this);
        waterDecreaseCoroutine = new SingleCoroutine(this);
        plantDecreaseCoroutine = new SingleCoroutine(this);
        gm.player.onEquipWeapon.AddListener(AddStickyWaterMultiplierToPlayerWeapon);
        AddStickyWaterMultiplierToPlayerWeapon(gm.player, gm.player.inventory.GetWeapon());
    }

    public void SufferElementFrom(Player player, ElementHolder element) {
        float playerProtection = player.elementProtection.GetProtectionFor(element.elem);
        if(playerProtection >= 1.0f) {
            return;
        }
        switch (element.elem) {
            case Element.FIRE:
                SufferFireFrom(player, element);
                break;
            case Element.WATER:
                SufferWaterFrom(player, element);
                break;
            case Element.PLANT:
                SufferPlantFrom(player, element);
                break;
            default:
                return;
        }
    }

    protected void SufferFireFrom(Player player, ElementHolder element) {
        float playerProtection = 1 - player.elementProtection.GetProtectionFor(element.elem);
        onStartSufferFire.Invoke(player);
        fireCount += fireCountByHit * element.intensity * playerProtection;
        if(fireCount >= MAX_COUNT) {
            fireDamage.Hit(player);
            fireCount = 0;
            onFireExplode.Invoke(player);
            onStopSufferFire.Invoke(player);
        }
        fireDecreaseCoroutine.Start(CDecreaseFire(player));
        onSufferFire.Invoke(player);
        onSufferElement.Invoke(player, Element.FIRE);
    }

    protected IEnumerator CDecreaseFire(Player player) {
        while(fireCount > 0) {
            yield return null;
            fireCount = Mathf.Max(0, fireCount - fireCountDecreaseSpeed * Time.deltaTime);
        }
        onStopSufferFire.Invoke(player);
    }

    protected void SufferWaterFrom(Player player, ElementHolder element) {
        float playerProtection = 1 - player.elementProtection.GetProtectionFor(element.elem);
        onStartSufferWater.Invoke(player);
        SetWaterCountTo(waterCount + waterCountByHit * element.intensity * playerProtection);
        if (waterCount >= MAX_COUNT) {
            player.GetAttackRange().range.rangeModifier.AddModifier(FloatModifier.Mode.MULTIPLY, FloatModifier.Period.DURATION, 0.0f, waterExplodeDuration);
            SetWaterCountTo(0);
            onWaterExplode.Invoke(player);
            onStopSufferWater.Invoke(player);
        }
        waterDecreaseCoroutine.Start(CDecreaseWater(player));
        onSufferWater.Invoke(player);
        onSufferElement.Invoke(player, Element.WATER);
    }

    protected IEnumerator CDecreaseWater(Player player) {
        while (waterCount > 0) {
            yield return null;
            SetWaterCountTo(Mathf.Max(0, waterCount - waterCountDecreaseSpeed * Time.deltaTime));
        }
        onStopSufferWater.Invoke(player);
    }

    protected void ClearFireCountFor(Player player) {
        float previousFireCount = fireCount;
        fireCount = 0;
        if (previousFireCount > 0) {
            onStopSufferFire.Invoke(player);
        }
    }

    protected void ClearWaterCountFor(Player player) {
        float previousWaterCount = waterCount;
        waterCount = 0;
        if (previousWaterCount > 0) {
            onStopSufferWater.Invoke(player);
        }
    }

    public void ClearElementCountFor(Player player, Element element) {
        switch (element) {
            case Element.FIRE: ClearFireCountFor(player); break;
            case Element.WATER: ClearWaterCountFor(player); break;
            default: throw new Exception($"There is no ClearElementCountFor for this element in ElementManager : {element}");
        }
    }

    protected void SetWaterCountTo(float value) {
        waterCount = value;
        UpdateStickyWaterRangeMultiplier();
    }

    public float GetElementCount(Element element) {
        switch (element) {
            case Element.FIRE: return fireCount;
            case Element.WATER: return waterCount;
            case Element.PLANT: return plantCount;
            default: throw new Exception($"There is no count for this element in ElementManager : {element}");
        }
    }

    protected void SufferPlantFrom(Player player, ElementHolder element) {
        onSufferPlant.Invoke(player);
        onSufferElement.Invoke(player, Element.PLANT);
    }

    public float GetFireCount() {
        return fireCount;
    }

    protected void AddStickyWaterMultiplierToPlayerWeapon(Character player, Weapon weapon) {
        if(weapon == null) {
            return;
        }
        waterDecreaseRangeMultiplier = new FloatModifier(FloatModifier.Mode.MULTIPLY, FloatModifier.Period.INFINITE_DURATION, 1.0f);
        player.GetComponent<Player>().GetAttackRange().range.rangeModifier.AddModifier(waterDecreaseRangeMultiplier);
        UpdateStickyWaterRangeMultiplier();
    }

    protected void UpdateStickyWaterRangeMultiplier() {
        float avancement = waterCount / MAX_COUNT;
        float coef = MathCurves.Linear(1.0f, 1.0f - maxRangeDecreaseWhileCounting, avancement);
        waterDecreaseRangeMultiplier.ChangeValue(coef);
    }
}
