using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class GraphModifier : MonoBehaviour {

    protected Graph graph;

    public virtual void Initialize(Graph graph) {
        this.graph = graph;
    }

    public abstract void Apply();

    protected void DeleteEdge(Edge edge) {
        graph.edges.Remove(edge);
        edge.Delete();
        // Check that we don't split the graph in 2 !
    }
}
