using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class FloatModifier {

    public enum Mode { ADD, SET, MULTIPLY };
    public enum Period { DURATION, INFINITE_DURATION };

    public Mode mode = Mode.ADD;
    public float value = 1.0f;
    public Period period = Period.DURATION;
    //[ConditionalHide("period", Period.DURATION)]
    public float duration = 3.0f;

    protected FloatModifierManager holder;
    protected Timer timer;

    public FloatModifier(Mode mode, Period period, float value, float duration = 0) {
        this.mode = mode;
        this.value = value;
        this.period = period;
        this.duration = duration;
    }


    public FloatModifier(FloatModifier other) {
        this.mode = other.mode;
        this.value = other.value;
        this.period = other.period;
        this.duration = other.duration;
    }

    public void Initialize(FloatModifierManager manager) {
        this.holder = manager;
        if (UseDuration()) {
            timer = new Timer(duration);
            manager.Holder().StartCoroutine(CRemoveWhenDurationIsOver());
        } else {
            timer = new Timer(float.PositiveInfinity);
        }
    }

    public bool IsAdder() {
        return mode == Mode.ADD;
    }

    public bool IsSetter() {
        return mode == Mode.SET;
    }

    public bool IsMultiplier() {
        return mode == Mode.MULTIPLY;
    }

    public bool UseDuration() {
        return period == Period.DURATION;
    }

    public bool UseInfiniteDuration() {
        return period == Period.INFINITE_DURATION;
    }

    public float Get() {
        return value;
    }

    public bool IsOver() {
        return timer.IsOver();
    }

    protected void Stop() {
        timer.SetOver();
        holder.RemoveModifier(this);
    }

    public override string ToString()
    {
        string modeString = mode == Mode.ADD ? "ADD" : (mode == Mode.SET ? "SET" : "MULTIPLY");
        string periodString = period == Period.DURATION ? $"for {duration}" : "forever";
        return $"{modeString} {periodString} : {value}";
    }

    protected IEnumerator CRemoveWhenDurationIsOver()
    {
        yield return new WaitForSeconds(duration);
        Stop();
    }

    public void ChangeValue(float newValue) {
        value = newValue;
        holder.onChangeValue.Invoke();
    }
}