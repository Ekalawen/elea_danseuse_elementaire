﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;

public class EventManager_TheAcademy4 : EventManager {

    [Header("Phase 1 : Go To the Barrage")]
    public float distanceToExitToTriggerPhase2 = 3.0f;

    [Header("Phase 2 : First Ambush")]
    public int nbEnemiesInPhase2 = 100;
    public AutomaticStory phase2Story;
    public Spawner phase2Enemies;

    [Header("Phase 3 : Test against water")]
    public int nbEnemiesInPhase3 = 50;
    public int maxNbEnemiesAtTheSameTime = 4;
    public AutomaticStory phase3Story;
    public Spawner phase3Enemies;

    [Header("Phase 4 : Water Sanctuary")]
    public AutomaticStory phase4Story;
    public Spawner phase4Objects;

    [Header("Phase 5 : Lot of water")]
    public int nbEnemiesInPhase5 = 150;
    public Spawner phase5Enemies;

    public override void Initialize() {
        base.Initialize();
        GoToPhase1();
        gm.objectManager.OpenExit();
        StartCoroutine(CTriggerPhase2());
    }

    protected IEnumerator CTriggerPhase2() {
        yield return new WaitUntil(() => Vector2.Distance(gm.player.pos, gm.objectManager.GetExit().pos) <= distanceToExitToTriggerPhase2);
        gm.objectManager.CloseExit();
        GoToPhase2();
    }

    public override void StartPhase2() {
        phase2Story.InitializeAndTryTrigger();
        gm.enemyManager.AddSpawner(phase2Enemies);
        gm.enemyManager.onEnemyDie.AddListener(CheckNumberOfKillForPhase3);
    }

    protected void CheckNumberOfKillForPhase3(Enemy enemy) {
        if(gm.enemyManager.GetKillCount() == nbEnemiesInPhase2) {
            gm.enemyManager.RemoveSpawner(phase2Enemies);
            if (gm.enemyManager.enemies.Count <= 1) {
                GoToPhase3();
            } else {
                onNothingLeft.AddListener(GoToPhase3);
            }
        }
    }

    public override void StartPhase3() {
        phase3Story.InitializeAndTryTrigger();
        gm.enemyManager.nbMaxEnemies = maxNbEnemiesAtTheSameTime;
        gm.enemyManager.AddSpawner(phase3Enemies);
        gm.enemyManager.onEnemyDie.AddListener(CheckNumberOfKillForPhase4);
    }

    protected void CheckNumberOfKillForPhase4(Enemy enemy) {
        if (gm.enemyManager.GetKillCount() == nbEnemiesInPhase2 + nbEnemiesInPhase3) {
            gm.enemyManager.RemoveSpawner(phase3Enemies);
            if (gm.enemyManager.enemies.Count <= 1) {
                GoToPhase4();
            } else {
                onNothingLeft.AddListener(GoToPhase4);
            }
        }
    }

    public override void StartPhase4() {
        phase4Story.InitializeAndTryTrigger();
        gm.objectManager.AddSpawner(phase4Objects);
        gm.objectManager.onActivateElementalAltar.AddListener(GoToPhase5);
    }

    protected void GoToPhase5(ElementalAltar altar) {
        GoToPhase5();
    }

    public override void StartPhase5() {
        gm.enemyManager.nbMaxEnemies = 100;
        gm.enemyManager.AddSpawner(phase5Enemies);
        gm.enemyManager.onEnemyDie.AddListener(CheckNumberOfKillForPhase5);
    }

    protected void CheckNumberOfKillForPhase5(Enemy enemy) {
        if (gm.enemyManager.GetKillCount() == nbEnemiesInPhase2 + nbEnemiesInPhase3 + nbEnemiesInPhase5) {
            gm.enemyManager.RemoveSpawner(phase5Enemies);
            if (IsNothingLeft()) {
                gm.objectManager.OpenExit();
            } else {
                onNothingLeft.AddListener(gm.objectManager.OpenExit);
            }
        }
    }
}