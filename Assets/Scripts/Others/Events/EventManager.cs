﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;

public class EventManager : MonoBehaviour {

    [HideInInspector]
    public UnityEvent onNothingLeft = new UnityEvent();

    protected GameManager gm;
    protected int currentPhase = 0;


    public virtual void Initialize()
    {
        gm = GameManager.Instance;
        InitializeOnNothingLeftEvent();
    }

    protected int GoToPhase(int phaseIndice) {
        if(currentPhase != phaseIndice - 1) {
            return currentPhase;
        }
        currentPhase++;
        switch (currentPhase) {
            case 1: StartPhase1(); break;
            case 2: StartPhase2(); break;
            case 3: StartPhase3(); break;
            case 4: StartPhase4(); break;
            case 5: StartPhase5(); break;
            default:
                throw new Exception($"Phase {currentPhase} not implemented yet!");
        }
        return currentPhase;
    }

    protected void GoToPhaseIn(int phaseIndice, float duration) {
        StartCoroutine(CGoToPhaseIn(phaseIndice, duration));
    }

    protected IEnumerator CGoToPhaseIn(int phaseIndice, float duration) {
        yield return new WaitForSeconds(duration);
        GoToPhase(phaseIndice);
    }

    protected void GoToPhase1() {
        GoToPhase(1);
    }
    protected void GoToPhase2() {
        GoToPhase(2);
    }
    protected void GoToPhase3() {
        GoToPhase(3);
    }
    protected void GoToPhase4() {
        GoToPhase(4);
    }
    protected void GoToPhase5() {
        GoToPhase(5);
    }

    public virtual void StartPhase1() { }
    public virtual void StartPhase2() { }
    public virtual void StartPhase3() { }
    public virtual void StartPhase4() { }
    public virtual void StartPhase5() { }

    protected void InitializeOnNothingLeftEvent() {
        gm.objectManager.onCatchObject.AddListener(CheckNothingLeft);
        gm.enemyManager.onEnemyDie.AddListener(CheckNothingLeft);
    }

    protected void CheckNothingLeft(Object obj) {
        StartCoroutine(CCheckNothingLeft());
    }

    protected void CheckNothingLeft(Enemy enemy) {
        StartCoroutine(CCheckNothingLeft());
    }

    protected IEnumerator CCheckNothingLeft() {
        yield return null; // Wait 1 frame for objects and enemies to be removed !
        if(IsNothingLeft()) {
            onNothingLeft.Invoke();
        }
    }

    public bool IsNothingLeft() {
        int nbFixObjects = gm.objectManager.GetFixObjects().Count;
        if(gm.objectManager.objects.Count <= nbFixObjects
        && gm.enemyManager.enemies.Count == 0) {
            return true;
        }
        return false;
    }
}