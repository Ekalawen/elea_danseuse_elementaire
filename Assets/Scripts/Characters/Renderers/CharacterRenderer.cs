using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.VFX;
using UnityEngine.Events;

//[RequireComponent(typeof(SpriteRenderer))]
public class CharacterRenderer : MonoBehaviour {

    public enum SpriteOrientation { LOOKING_LEFT, LOOKING_RIGHT };

    public static float TIME_AFTER_DEATH = 2.0f;
    protected static string ON_DEATH_VFX_DIRECTION = "Direction";
    protected static string ON_DEATH_VFX_SPAWN_COUNT = "SpawnCount";
    protected static string ON_DEATH_VFX_ANGLE = "Angle";
    protected static string ATTACK_LINE_SIZE = "Size";
    protected static string ATTACK_LINE_ROTATION = "Rotation";
    protected static float ATTACK_LINE_BEHIND_COEF = 1.5f;

    public SpriteOrientation spriteOrientation = SpriteOrientation.LOOKING_LEFT;

    protected PostProcessManager ppManager;
    protected SpriteRenderer spriteRenderer;
    protected Character character;
    protected HealthBar healthBar;
    protected float initialSpriteXPosition;
    protected float initialSpriteXScale;

    public virtual void Initialize(Character character) {
        ppManager = GameManager.Instance.postProcessManager;
        this.spriteRenderer = GetComponent<SpriteRenderer>();
        this.character = character;
        this.character.health.isInvulnerable.onStart.AddListener(SetInvulnerabilityMode);
        this.character.health.isInvulnerable.onStop.AddListener(SetNotInvulnerabilityMode);
        this.character.health.hasTakenHit.onStart.AddListener(SetHasTakenHitMode);
        this.character.health.hasTakenHit.onStop.AddListener(SetHasNotTakenHitMode);
        this.character.onKilledBy.AddListener(InstantiateVfxOnDeath);
        this.character.onHit.AddListener(InstantiateAttackLineVfx);
        this.character.onKill.AddListener(InstantiateAttackLineVfx);
        this.character.onStartMove.AddListener(OrientSpriteBasedOnDirection);
        ppManager.InstantiateVfxSpawning(character.pos);
        InitializeHealthBar();
        RememberSpriteInitialParameters();
    }

    protected void RememberSpriteInitialParameters() {
        initialSpriteXPosition = spriteRenderer.transform.localPosition.x;
        initialSpriteXScale = spriteRenderer.transform.localScale.x;
    }

    protected void InitializeHealthBar() {
        healthBar = transform.parent.GetComponentInChildren<HealthBar>();
        if (healthBar != null) {
            healthBar.Initialize(character.health);
        }
    }

    protected void SetInvulnerabilityMode() {
        spriteRenderer.color = ppManager.invulnerabilityColor;
    }

    protected void SetNotInvulnerabilityMode() {
        if(character.health.hasTakenHit.value) {
            SetHasTakenHitMode();
        } else {
            spriteRenderer.color = Color.white;
        }
    }

    protected void SetHasTakenHitMode() {
        spriteRenderer.color = ppManager.hasTakenHitColor;
    }

    protected void SetHasNotTakenHitMode() {
        if(character.health.isInvulnerable.value) {
            SetInvulnerabilityMode();
        } else {
            SetNotInvulnerabilityMode();
        }
    }

    protected void InstantiateVfxOnDeath(Character killed, Character killer) {
        Vector2 direction = killer != null ? (killed.pos - killer.pos).normalized : Vector2.zero;
        VisualEffect vfx = Instantiate(
            ppManager.onDeathVfxPrefab,
            position: killed.pos,
            rotation: Quaternion.identity,
            parent: ppManager.vfxFolder).GetComponent<VisualEffect>();
        if (direction == Vector2.zero) {
            vfx.SetFloat(ON_DEATH_VFX_ANGLE, 180);
        } else {
            vfx.SetVector2(ON_DEATH_VFX_DIRECTION, direction);
        }
        Vector2 spawnCount = vfx.GetVector2(ON_DEATH_VFX_SPAWN_COUNT);
        float spawnCountCoef = killed.socle.Get() / 0.9f;
        vfx.SetVector2(ON_DEATH_VFX_SPAWN_COUNT, spawnCount * spawnCountCoef);
        Destroy(vfx.gameObject, TIME_AFTER_DEATH);
    }

    protected void InstantiateAttackLineVfx(Character target) {
        VisualEffect vfx = Instantiate(
            ppManager.attackLinePrefab,
            position: target.pos,
            rotation: Quaternion.identity,
            parent: ppManager.vfxFolder).GetComponent<VisualEffect>();
        float distance = Vector2.Distance(character.pos, target.pos) / 2.0f * ATTACK_LINE_BEHIND_COEF;
        vfx.SetFloat(ATTACK_LINE_SIZE, distance);
        Quaternion rotation = Quaternion.FromToRotation(Vector2.down, target.pos - character.pos);
        vfx.transform.rotation = rotation;
        vfx.SetVector3(ATTACK_LINE_ROTATION, rotation.eulerAngles);
        Destroy(vfx.gameObject, 2.0f);
    }

    protected void OrientSpriteBasedOnDirection(Node node) {
        Vector2 direction = (node.pos - character.pos).normalized;
        float dot = Vector2.Dot(direction, Vector2.right);
        if(dot > 0) {
            OrientTo(SpriteOrientation.LOOKING_RIGHT);
        } else if (dot < 0){
            OrientTo(SpriteOrientation.LOOKING_LEFT);
        }
    }

    protected void OrientTo(SpriteOrientation orientation) {
        Vector3 pos = spriteRenderer.transform.localPosition;
        Vector3 scale = spriteRenderer.transform.localScale;
        float coef = orientation == spriteOrientation ? 1 : -1;
        pos.x = coef * initialSpriteXPosition;
        scale.x = coef * initialSpriteXScale;
        spriteRenderer.transform.localPosition = pos;
        spriteRenderer.transform.localScale = scale;
    }

    public void SetOrientationToInitialValues() {
        Vector3 pos = spriteRenderer.transform.localPosition;
        Vector3 scale = spriteRenderer.transform.localScale;
        pos.x = initialSpriteXPosition;
        scale.x = initialSpriteXScale;
        spriteRenderer.transform.localPosition = pos;
        spriteRenderer.transform.localScale = scale;
    }
}
