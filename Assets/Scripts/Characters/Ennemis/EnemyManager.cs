using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

public class EnemyManager : SpawnerManager {

    public float minSpawnDistance = 8.0f;
    public float absoluteMinSpawnDistance = 4.0f;
    public int nbMaxEnemies = 20;
    public ElementManager elementManager;

    [HideInInspector]
    public UnityEvent<Enemy> onEnemyDie = new UnityEvent<Enemy>(); // The enemy who just died

    protected GameManager gm;
    [HideInInspector]
    public List<Enemy> enemies;
    protected List<Spawner> spawners;
    [HideInInspector]
    public Transform projectilesFolder;
    protected List<Projectile> projectiles;
    protected int killCount = 0;
    protected int nextEnemyId = 0;

    public void Initialize()
    {
        gm = GameManager.Instance;
        elementManager.Initialize();
        enemies = new List<Enemy>();
        InitializeProjectiles();

        InitSpawners();
    }

    protected void InitializeProjectiles() {
        projectiles = new List<Projectile>();
        projectilesFolder = new GameObject(name: "Projectiles").transform;
        projectilesFolder.SetParent(transform);
    }

    protected void InitSpawners() {
        spawners = new List<Spawner>();
        foreach(Spawner spawner in GetComponentsInChildren<Spawner>()) {
            AddSpawner(spawner);
        }
    }

    public override Spawner AddSpawner(Spawner spawner) {
        spawner.Initialize(this);
        spawners.Add(spawner);
        return spawner;
    }

    public void AddSpawners(List<Spawner> newSpawners) {
        foreach(Spawner spawner in newSpawners) {
            AddSpawner(spawner);
        }
    }

    public override bool RemoveSpawner(Spawner spawner) {
        bool hasBeenRemoved = spawners.Remove(spawner);
        spawner.Stop();
        return hasBeenRemoved;
    }

    public void RemoveSpawners(List<Spawner> spawners) {
        foreach(Spawner spawner in spawners) {
            RemoveSpawner(spawner);
        }
    }

    public override List<MonoBehaviour> SpawnSpawningQuantity(List<SpawningQuantity> spawningQuantities, Func<float, Node> method) {
        return SpawnEnemyQuantities(spawningQuantities, method).Cast<MonoBehaviour>().ToList();
    }

    public List<Enemy> SpawnEnemyQuantities(List<SpawningQuantity> enemyQuantities, Func<float, Node> method) {
        List<Enemy> spawnedEnemies = new List<Enemy>();
        foreach(SpawningQuantity enemyQuantity in enemyQuantities) {
            List<Enemy> newEnemies = SpawnEnemyQuantity(enemyQuantity, method);
            spawnedEnemies.AddRange(newEnemies);
        }
        return spawnedEnemies;
    }

    public List<Enemy> SpawnEnemyQuantity(SpawningQuantity enemyQuantity, Func<float, Node> method) {
        List<Enemy> generated = new List<Enemy>();
        foreach(GameObject enemyPrefab in enemyQuantity.GetPrefabs()) {
            Enemy enemy = SpawnEnemy(enemyPrefab, method);
            if (enemy != null) {
                generated.Add(enemy);
            }
        }
        return generated;
    }

    protected Enemy SpawnEnemy(GameObject prefab, Func<float, Node> method) {
        if(enemies.Count >= nbMaxEnemies) {
            return null;
        }
        float enemySize = prefab.GetComponent<Character>().socle.Get();
        Node startNode = method != null ? method(enemySize) : GetSpawningNode(enemySize);
        if(startNode == null) {
            return null;
        }
        Enemy enemy = Instantiate(prefab, parent: transform).GetComponent<Enemy>();
        enemy.Initialize(startNode);
        return enemy;
    }

    public List<Node> EnemyCurrentNodes() {
        List<Node> enemyNodes = enemies.Select(e => e.CurrentNode()).ToList();
        return enemyNodes;
    }

    public List<Node> EnemyTargetingNodes() {
        List<Node> enemyNodes = enemies.Select(e => e.TargetingNode()).ToList();
        return enemyNodes;
    }

    public List<Vector2> EnemyPositions() {
        List<Vector2> enemyPositions = enemies.Select(e => e.pos).ToList();
        return enemyPositions;
    }

    public List<Node> EnemyNodesCurrentAndTargetingNodes() {
        List<Node> enemyNodes = enemies.Select(e => e.CurrentNode()).ToList();
        enemyNodes.AddRange(enemies.Select(e => e.TargetingNode()).ToList());
        return enemyNodes.Distinct().ToList();
    }

    public List<Node> EnemyInSocleNodes(float socleOffset) {
        List<Node> nodes = new List<Node>();
        foreach(Enemy enemy in enemies) {
            nodes.AddRange(enemy.NodesInSocle(socleOffset));
        }
        return nodes.Distinct().ToList();
    }

    public override Node GetSpawningNode(float enemySocleSize) {
        if(gm.player == null) {
            // No spawning when the player is dead ! :p
            return null;
        }
        List<Node> candidates = new List<Node>();
        for (float currentSpawnDistance = minSpawnDistance; currentSpawnDistance >= absoluteMinSpawnDistance && candidates.Count == 0; currentSpawnDistance--) {
            List<Node> emptyNodes = gm.graph.EmptyNodes(enemySocleSize / 2);
            candidates = gm.graph.GetAllAtLeastAsFarFrom(emptyNodes, gm.player.pos, currentSpawnDistance);
        }
        if(candidates.Count == 0) {
            return null;
        }
        return MathTools.ChoseOne(candidates);
    }

    public int NextEnemyId() {
        return ++nextEnemyId;
    }

    public List<Character> Characters() {
        List<Character> characters = enemies.Select(e => e).Cast<Character>().ToList();
        if(gm.player != null) {
            characters.Add(gm.player);
        }
        return characters;
    }

    public List<T> GetAllEnemyOfType<T>() {
        return enemies.Select(e => e.GetComponent<T>()).ToList().FindAll(e => e != null);
    }

    public void RemoveDeadEnemy(Enemy enemy) {
        killCount++;
        enemies.Remove(enemy);
        onEnemyDie.Invoke(enemy);
    }

    public int GetKillCount() {
        return killCount;
    }

    public Projectile RegisterProjectile(Projectile projectile) {
        projectiles.Add(projectile);
        projectile.transform.SetParent(projectilesFolder);
        return projectile;
    }

    public bool UnregisterProjectile(Projectile projectile) {
        return projectiles.Remove(projectile);
    }
}
