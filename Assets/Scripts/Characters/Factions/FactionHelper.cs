﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;

public static class FactionHelper
{
    public static bool SameFactions(Faction f1, Faction f2) {
        return f1 == f2;
    }
    public static bool DifferentFactions(Faction f1, Faction f2) {
        return f1 != f2;
    }
    public static bool OppositeFactions(Faction f1, Faction f2) {
        return (f1 == Faction.PLAYER && f2 == Faction.ENEMY)
            || (f1 == Faction.ENEMY && f2 == Faction.PLAYER);
    }

    public static Faction ToFaction(object o) {
        if(o is Character) {
            return ((Character)o).faction;
        } else if (o is Faction) {
            return (Faction)o;
        } else if (o is Object) {
            return ((Object)o).faction;
        } else if (o is Projectile) {
            return ((Projectile)o).faction;
        } else {
            throw new Exception($"Can't use a {o.GetType()} inside the ToFaction() function ! :)");
        }
    }

    public static bool SameFactions(object o1, object o2) {
        return SameFactions(ToFaction(o1), ToFaction(o2));
    }
    public static bool DifferentFactions(object o1, object o2) {
        return DifferentFactions(ToFaction(o1), ToFaction(o2));
    }
    public static bool OppositeFactions(object o1, object o2) {
        return OppositeFactions(ToFaction(o1), ToFaction(o2));
    }
}