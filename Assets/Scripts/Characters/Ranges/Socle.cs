using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

public class Socle : Range {

    [HideInInspector]
    public UnityEvent<Socle, Character, Range> onEnterSameFaction = new UnityEvent<Socle, Character, Range>(); // ThisSocle, OtherCharacter, OtherRange
    [HideInInspector]
    public UnityEvent<Socle, Character, Range> onEnterOppositeFaction = new UnityEvent<Socle, Character, Range>();
    [HideInInspector]
    public UnityEvent<Socle, Character, Range> onEnterDifferentFaction = new UnityEvent<Socle, Character, Range>();

    [HideInInspector]
    public Faction faction;

    public override void Initialize()
    {
        base.Initialize();
        InitializeFaction();
        onEnter.AddListener(SocleOnEnter);
    }

    protected void InitializeFaction() {
        Character character = GetComponentInParent<Character>();
        if(character != null) {
            this.faction = character.faction;
            return;
        }
        Object obj = GetComponentInParent<Object>();
        if(obj != null) {
            this.faction = obj.faction;
            return;
        }
        Projectile projectile = GetComponentInParent<Projectile>();
        if(projectile != null) {
            this.faction = projectile.faction;
            return;
        }
        Debug.LogError($"Le socle {name} n'a ni Character ni Object ni Projectile en parent !");
    }

    protected void SocleOnEnter(Range thisRange, Collider2D collider) {
        Character other = collider.gameObject.GetComponentInParent<Character>();
        Range otherRange = collider.GetComponent<Range>();
        if(other != null) {
            if (FactionHelper.SameFactions(faction, other)) {
                onEnterSameFaction.Invoke(this, other, otherRange);
            }
            if(FactionHelper.OppositeFactions(faction, other)) {
                onEnterOppositeFaction.Invoke(this, other, otherRange);
            }
            if(FactionHelper.DifferentFactions(faction, other)) {
                onEnterDifferentFaction.Invoke(this, other, otherRange);
            }
        }
    }

    public override bool IsSocle() {
        return true;
    }
}
