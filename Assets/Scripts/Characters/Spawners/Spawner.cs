﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Spawner : MonoBehaviour {

    public NormalSpawningMethod method;
    public List<SpawningQuantity> spawningQuantities;

    protected GameManager gm;
    protected SpawnerManager manager;

    public virtual void Initialize(SpawnerManager manager) {
        gm = GameManager.Instance;
        this.manager = manager;
        if(method != null) {
            method.Initialize(manager);
        }
    }

    public List<MonoBehaviour> Spawn() {
        Func<float, Node> function = method != null ? method.GetNode : null;
        return manager.SpawnSpawningQuantity(spawningQuantities, function);
    }

    public virtual void Stop() { }

    public List<GameObject> GetPrefabs() {
        List<GameObject> prefabs = new List<GameObject>();
        foreach(SpawningQuantity sq in spawningQuantities) {
            prefabs.AddRange(sq.GetPrefabs());
        }
        return prefabs;
    }
}
