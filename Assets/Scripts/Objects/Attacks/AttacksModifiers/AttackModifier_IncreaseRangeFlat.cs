﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;

public class AttackModifier_IncreaseRangeFlat : AttackModifier {

    public float rangeIncrease = 1.0f;

    protected AttackRange attackRange;

    public override void Initialize(Attack attack, bool isUpgrade = false, Rarity upgradeRarity = Rarity.COMMON) {
        base.Initialize(attack, isUpgrade, upgradeRarity);
        this.attackRange = (AttackRange)attack;
        Assert.IsNotNull(attackRange);
        attackRange.range.rangeModifier.AddModifier(FloatModifier.Mode.ADD, FloatModifier.Period.INFINITE_DURATION, rangeIncrease);
        attackRange.range.IncreaseMaxRangeOf(rangeIncrease);
    }

    public override string Description() {
        return $"{rangeIncrease} de portée en plus";
    }

    public override string DescriptionOneWord() {
        return "longue";
    }
}