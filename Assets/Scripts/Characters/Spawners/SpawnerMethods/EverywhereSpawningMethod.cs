﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EverywhereSpawningMethod : NormalSpawningMethod {

    public float distanceFromPlayer = 1.0f;

    public override void Initialize(SpawnerManager manager) {
        base.Initialize(manager);
    }

    public override Node GetNode(float size) {
        List<Node> emptyNodes = gm.graph.EmptyNodes();
        emptyNodes = gm.graph.GetAllAtLeastAsFarFrom(emptyNodes, gm.player.pos, distanceFromPlayer);
        if(emptyNodes.Count == 0) {
            return null;
        }
        return MathTools.ChoseOne(emptyNodes);
    }
}
