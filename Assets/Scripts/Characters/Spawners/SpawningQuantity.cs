﻿using MyBox;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class SpawningQuantity {

    public enum QuantityMode { FIX_AMONG, FIX_FOR_ALL, UNIFORM_AMONG, UNIFORM_FOR_ALL };

    public QuantityMode quantityMode = QuantityMode.FIX_AMONG;
    [ConditionalField("quantityMode", false, QuantityMode.FIX_AMONG, QuantityMode.FIX_FOR_ALL)]
    public int quantity = 1;
    [ConditionalField("quantityMode", false, QuantityMode.UNIFORM_AMONG, QuantityMode.UNIFORM_FOR_ALL)]
    public Vector2Int range = new Vector2Int(0, 1);
    public List<GameObject> prefabs;

    public List<GameObject> GetPrefabs() {
        switch (quantityMode)
        {
            case QuantityMode.FIX_AMONG: return GetFixAmongPrefabs();
            case QuantityMode.FIX_FOR_ALL: return GetFixForAllPrefabs();
            case QuantityMode.UNIFORM_AMONG: return GetUniformAmong();
            case QuantityMode.UNIFORM_FOR_ALL: return GetUniformForAll();
            default: throw new Exception($"Unknown QuantityMode : {quantityMode}");
        }
    }

    protected List<GameObject> GetFixAmongPrefabs() {
        return MathTools.ChoseOneRepeated(prefabs, quantity);
    }

    protected List<GameObject> GetFixForAllPrefabs() {
        List<GameObject> gameObjects = new List<GameObject>();
        for(int i = 0; i < quantity; i++) {
            gameObjects.AddRange(prefabs);
        }
        return gameObjects;
    }

    protected List<GameObject> GetUniformAmong() {
        int nbPrefabs = UnityEngine.Random.Range(range.x, range.y + 1);
        return MathTools.ChoseOneRepeated(prefabs, nbPrefabs);
    }

    protected List<GameObject> GetUniformForAll() {
        List<GameObject> gameObjects = new List<GameObject>();
        foreach(GameObject prefab in prefabs) {
            int nbPrefabs = UnityEngine.Random.Range(range.x, range.y + 1);
            for(int i = 0; i < nbPrefabs; i++) {
                gameObjects.Add(prefab);
            }
        }
        return gameObjects;
    }
}

