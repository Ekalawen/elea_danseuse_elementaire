VIANA Ah vous voilà ! Pour une fois vous tombez bien !
VIANA Nous allions commencer le cours sur les Danses de Combat avec le reste de la classe ...
VIANA ... mais il n'y a pas assez de matériel pour tout le monde.
VIANA Je crois qu'il y a de vieilles lames d'entrainement stockées aux Archives, pourriez-vous aller les chercher rapidement ?
VIANA Et surtout faites bien attention à ne pas appuyer sur Espace pour ne rien casser !
VIANA Il y a aux Archives des rituels de magie élémentaire très puissants qu'il ne faudrait pas abîmer.
VIANA Si vous vous dépechez, je vous laisserai peut-être assister à la classe.
VIANA En fait non, oubliez ... vous gêneriez sûrement les autres étudiants.