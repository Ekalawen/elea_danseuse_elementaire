using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;

public class Edge : MonoBehaviour {

    protected const string OUTLINE_COLOR = "OutlineColor";

    public List<Node> nodes;
    public LineRenderer lineRenderer;

    public Vector2 start { get { return Start().pos; } }
    public Vector2 end { get { return End().pos; } }
    public float maxX { get { return Mathf.Max(start.x, end.x); } }
    public float minX { get { return Mathf.Min(start.x, end.x); } }
    public float maxY { get { return Mathf.Max(start.y, end.y); } }
    public float minY { get { return Mathf.Min(start.y, end.y); } }
    public float diffX { get { return maxX - minX; } }
    public float diffY { get { return maxY - minY; } }

    public void Initialize(Node node1, Node node2) {
        Assert.IsNotNull(node1);
        Assert.IsNotNull(node2);
        nodes = new List<Node>() { node1, node2 };
        name = ToString();
        foreach(Node node in nodes) {
            node.AddEdge(this);
        }
        lineRenderer.SetPositions(nodes.Select(n => n.transform.position).ToArray());
    }

    public Node Other(Node node) {
        if (node == nodes.First()) {
            return nodes.Last();
        }
        if (node == nodes.Last()) {
            return nodes.First();
        }
        throw new System.Exception($"The node {node} is not in this edge {this}, so we can't give the other node!");
    }

    public override string ToString() {
        return $"E({nodes.First()}, {nodes.Last()})";
    }

    public override bool Equals(object other) {
        Edge otherEdge = other as Edge;
        if(otherEdge == null) {
            return false;
        }
        return nodes.Contains(otherEdge.nodes.First()) && nodes.Contains(otherEdge.nodes.Last());
    }

    public override int GetHashCode() {
        return base.GetHashCode();
    }

    public void Delete() {
        foreach(Node node in nodes) {
            node.edges.Remove(this);
        }
        Destroy(gameObject);
    }

    public Vector2 Toward(Node node) {
        Assert.IsTrue(nodes.Contains(node));
        return node.pos - Other(node).pos;
    }

    public Vector2 From(Node node) {
        Assert.IsTrue(nodes.Contains(node));
        return Other(node).pos - node.pos;
    }

    public void SetColor(float hue) {
        Color color = lineRenderer.material.color;
        Color outlineColor = lineRenderer.material.GetColor(OUTLINE_COLOR);
        lineRenderer.material.color = ColorsTools.SetHue(color, hue);
        lineRenderer.material.SetColor(OUTLINE_COLOR, ColorsTools.SetHue(outlineColor, hue));
    }

    public Node Start() {
        return nodes.First();
    }

    public Node End() {
        return nodes.Last();
    }

    public float Magnitude() {
        return (End().pos - Start().pos).magnitude;
    }

    public Vector2 Vector() {
        return End().pos - Start().pos;
    }

    public Vector2 Direction() {
        return (End().pos - Start().pos).normalized;
    }

    public float Slope() {
        return MathTools.Slope(Start().pos, End().pos);
    }

    public float Offset() {
        return MathTools.Offset(Start().pos, End().pos);
    }

    public bool WillIntersectEdgeStrictly(Node node1, Node node2) {
        return MathTools.SegmentSegmentStrict(Start().pos, End().pos, node1.pos, node2.pos);
    }
}
