﻿using System;
using System.Collections;
using UnityEngine;

public class HealthSocle : Health {

    public override void Initialize(Character character) {
        base.Initialize(character);
        this.character.socle.onEnter.AddListener(OnEnterRange);
    }

    protected void OnEnterRange(Range range, Collider2D collider) {
        Character other = collider.gameObject.GetComponentInParent<Character>();
        Range otherRange = collider.gameObject.GetComponent<Range>();
        if(other == null) {
            return;
        }
        if(other == character) {
            return;
        }
        if(!otherRange.IsSocle()) {
            return;
        }
        if(FactionHelper.OppositeFactions(other, character)) {
            other.MeleeAttack(character);
        }
    }
}