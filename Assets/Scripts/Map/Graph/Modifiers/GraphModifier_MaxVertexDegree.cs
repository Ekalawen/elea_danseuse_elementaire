using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GraphModifier_MaxVertexDegree : GraphModifier {

    public enum Method { LAST_EDGE, CLOSEST_EDGE, LEAST_ORTHOGONAL_EDGE };

    public int maxVertexDegree = 4;
    public Method method;


    public override void Apply() {
        foreach(Node node in graph.nodes) {
            RemoveAdditionnalEdges(node);
        }
        //Debug.Log($"Il y a {graph.nodes.FindAll(n => n.edges.Count > maxVertexDegree).Count} noeuds de degr� sup�rieur � {maxVertexDegree}.");
    }

    protected void RemoveAdditionnalEdges(Node node) {
        switch (method) {
            case Method.LAST_EDGE:
                RemoveLastEdges(node);
                break;
            case Method.CLOSEST_EDGE:
                RemoveClosestEdges(node);
                break;
            case Method.LEAST_ORTHOGONAL_EDGE:
                RemoveLeastOrthogonalEdges(node);
                break;
        }
    }

    protected void RemoveClosestEdges(Node node) {
        int initialNbOfEdges = node.edges.Count;
        int nbSuppression = 0;
        for (int i = maxVertexDegree; i < node.edges.Count;) {
            Edge edge = GetClosestToOtherEdges(node);
            DeleteEdge(edge);
            nbSuppression += 1;
        }
        //if (nbSuppression > 0) {
        //    Debug.Log($"On a supprim� {nbSuppression} edges de {node} qui avait initialement {initialNbOfEdges}, elle en a maintenant {node.edges.Count}", node);
        //}
    }

    protected Edge GetClosestToOtherEdges(Node node) {
        List<Edge> edges = node.GetEdgesClockwise();
        Edge closest = edges[0];
        float minAngle = float.PositiveInfinity;
        for(int i = 0; i < edges.Count; i++) {
            Node previousNode = edges[(i + edges.Count - 1) % edges.Count].Other(node);
            Node nextNode = edges[(i + 1) % edges.Count].Other(node);
            float angle = Vector2.SignedAngle(nextNode.pos - node.pos, previousNode.pos - node.pos);
            if (angle < 0) {
                angle = 360 + angle;
            }
            //Debug.Log($"{edges[i]} {angle}�", edges[i]);
            if(angle < minAngle) {
                closest = edges[i];
                minAngle = angle;
            }
        }
        return closest;
    }

    protected void RemoveLastEdges(Node node) {
        for(int i = maxVertexDegree; i < node.edges.Count; i++) {
            DeleteEdge(node.edges[i]);
            i--;
        }
    }

    protected void RemoveLeastOrthogonalEdges(Node node) {
        if(node.edges.Count < maxVertexDegree) {
            return;
        }

        Dictionary<Vector2, List<Edge>> edgesMap = node.MapEdgesExclusif();
        List<Edge> affectedEdges = edgesMap.Values.SelectMany(e => e).ToList();

        foreach(Edge edge in node.GetEdgesCopy()) {
            if (!affectedEdges.Contains(edge)) {
                DeleteEdge(edge);
            }
        }
    }
}
