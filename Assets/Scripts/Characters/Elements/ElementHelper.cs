﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;

public static class ElementHelper {
    public static bool SameElements(Element f1, Element f2) {
        return f1 == f2;
    }
    public static bool DifferentElements(Element f1, Element f2) {
        return f1 != f2;
    }
    public static bool IsWeaknessOf(Element source, Element target) {
        return (source == Element.FIRE && target == Element.PLANT)
            || (source == Element.WATER && target == Element.FIRE)
            || (source == Element.PLANT && target == Element.WATER);
    }
    public static bool IsResistentTo(Element source, Element target) {
        return IsWeaknessOf(source, target) || source == Element.NO_ELEMENT;
    }
    public static bool SufferFrom(Element source, Element target) {
        return IsNotElement(source) && IsElement(target);
    }
    public static bool IsElement(Element element) {
        return element != Element.NO_ELEMENT;
    }
    public static bool IsNotElement(Element element) {
        return element == Element.NO_ELEMENT;
    }

    public static Element ToElement(object o) {
        if (o is Character) {
            return ((Character)o).element.elem;
        } else if (o is Element) {
            return (Element)o;
        } else if (o is ElementHolder) {
            return ((ElementHolder)o).elem;
        } else {
            throw new Exception($"Can't use a {o.GetType()} inside the ToElement() function ! :)");
        }
    }

    public static bool SameElements(object f1, object f2) {
        return SameElements(ToElement(f1), ToElement(f2));
    }
    public static bool DifferentElements(object f1, object f2) {
        return DifferentElements(ToElement(f1), ToElement(f2));
    }
    public static bool IsWeaknessOf(object source, object target) {
        return IsWeaknessOf(ToElement(source), ToElement(target));
    }
    public static bool IsResistentTo(object source, object target) {
        return IsResistentTo(ToElement(source), ToElement(target));
    }
    public static bool SufferFrom(object source, object target) {
        return SufferFrom(ToElement(source), ToElement(target));
    }
    public static bool IsElement(object element) {
        return IsElement(ToElement(element));
    }
    public static bool IsNotElement(object element) {
        return IsNotElement(ToElement(element));
    }

    public static string ToString(Element element, bool withPrefix = false) {
        string prefix;
        switch (element) {
            case Element.NO_ELEMENT: return $"Aucun Element";
            case Element.FIRE:
                prefix = withPrefix ? "le " : "";
                return $"{prefix}Feu";
            case Element.WATER:
                prefix = withPrefix ? "l'" : "";
                return $"{prefix}Eau";
            case Element.PLANT:
                prefix = withPrefix ? "les " : "";
                return $"{prefix}Plantes";
            default: return "";
        }
    }
}