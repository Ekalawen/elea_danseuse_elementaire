using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;
using UnityEngine.UI;
using UnityEngine.Assertions;
using UnityEngine.SceneManagement;

public class LevelButton : MonoBehaviour {

    public enum Mode { CAN_ALWAYS_PLAY, NEED_COMPLETE_ANOTHER_LEVEL, CAN_NEVER_PLAY };

    public string levelSceneName;
    public Mode mode;
    [ConditionalHide("mode", Mode.NEED_COMPLETE_ANOTHER_LEVEL)]
    public string previousLevelSceneName;

    protected Button button;

    public void Initialize() {
        button = GetComponent<Button>();
        button.interactable = IsInterractable();
        button.onClick.AddListener(StartLevel);
    }

    protected void StartLevel() {
        SceneManager.LoadScene(levelSceneName);
    }

    public bool IsInterractable() {
        switch (mode) {
            case Mode.CAN_ALWAYS_PLAY: return true;
            case Mode.NEED_COMPLETE_ANOTHER_LEVEL: return HasCompletedPreviousLevel();
            case Mode.CAN_NEVER_PLAY: return false;
            default: throw new Exception($"Mode {mode} unknown.");
        }
    }

    public bool HasCompletedPreviousLevel() {
        Assert.IsTrue(mode == Mode.NEED_COMPLETE_ANOTHER_LEVEL);
        return PrefsManager.GetHasCompletedLevel(previousLevelSceneName);
    }
}