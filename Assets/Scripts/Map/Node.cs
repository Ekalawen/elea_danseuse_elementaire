using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;

public class Node : MonoBehaviour {

    public const string OUTLINE_COLOR = "OutlineColor";

    public List<Edge> edges;

    private SpriteRenderer _spriteRenderer = null;
    public SpriteRenderer spriteRenderer { get { if (!_spriteRenderer) _spriteRenderer = GetComponent<SpriteRenderer>(); return _spriteRenderer; } }
    public Material material { get { return spriteRenderer.material; } }

    public Vector2 pos {
        get { return new Vector2(transform.position.x, transform.position.y); }
        set { transform.position = value; }
    }

    public void Initialize(Vector2 pos) {
        edges = new List<Edge>();
        SetPosition(pos);
    }

    public void SetPosition(Vector2 pos) {
        transform.position = pos;
        name = ToString();
    }

    public override string ToString() {
        return $"N{new Vector2(transform.position.x, transform.position.y)}";
    }

    public override bool Equals(object other) {
        Node otherNode = other as Node;
        if(otherNode == null) {
            return false;
        }
        return transform.position == otherNode.transform.position;
    }

    public override int GetHashCode() {
        return base.GetHashCode();
    }

    public bool IsVoisin(Node node) {
        return Voisins().Contains(node);
    }

    public bool IsInsideEdge(Edge edge) {
        return IsInsideEdge(edge.nodes.First(), edge.nodes.Last());
    }

    public bool IsInsideEdge(Node node1, Node node2) {
        return MathTools.SegmentPointStrict(node1.pos, node2.pos, this.pos);
    }

    public List<Edge> GetEdgesClockwise() {
        // SignedAngle return the counter-clockwise order
        return edges.OrderBy(e => -Vector2.SignedAngle(Vector2.right, e.Other(this).pos - pos)).ToList();
    }

    public List<Edge> GetEdgesCopy() {
        return edges.Select(e => e).ToList();
    }

    public List<Edge> GetEdgesClosestToNormals() {
        return edges.OrderBy(e => MathTools.AngleToClosestNormal(e.Other(this).pos - pos)).ToList();
    }

    public Edge GetClosestEdgeToDirection(Vector2 direction) {
        if (edges.Count == 0)
            return null;
        return edges.OrderBy(e => Vector2.Angle(e.From(this), direction)).First();
    }

    public Edge GetClosestEdgeToDirectionLimit90(Vector2 direction) {
        List<Edge> goodEdges = edges.FindAll(e => Vector2.Angle(e.From(this), direction) < 90);
        if (goodEdges.Count == 0)
            return null;
        return goodEdges.OrderBy(e => Vector2.Angle(e.From(this), direction)).First();
    }

    public Edge AddEdge(Edge edge) {
        if(!edges.Contains(edge)) {
            edges.Add(edge);
            return edge;
        }
        return null;
    }

    public List<Node> Voisins() {
        return edges.Select(e => e.Other(this)).ToList();
    }

    public Node ClosestTo(List<Node> nodes) {
        if (nodes.Count == 0) return null;
        Node closest = nodes[0];
        float minDist = float.PositiveInfinity;
        foreach(Node n in nodes) {
            if (n != this) {
                float dist = Vector3.Distance(n.pos, pos);
                if(dist < minDist) {
                    closest = n;
                    minDist = dist;
                }
            }
        }
        return closest;
    }

    public bool IsInsideTriangle(Node node1, Node node2, Node node3) {
        Vector2 axe1 = node2.pos - node1.pos;
        Vector2 axe2 = node3.pos - node2.pos;
        Vector2 axe3 = node1.pos - node3.pos;
        float angle = Vector2.SignedAngle(axe1, -axe3); // draw the triangle to understand the minus sign!
        if(angle == 0 || angle == 180) { // flat triangle, can't be inside
            return false;
        }
        if (angle > 0) { // SignedAngle return the counter-clockwise angle, so we need it to be negative!
            Node tmp = node2;
            node2 = node3;
            node3 = tmp;
            axe1 = node2.pos - node1.pos;
            axe2 = node3.pos - node2.pos;
            axe3 = node1.pos - node3.pos;
        }

        float angle1 = Vector2.SignedAngle(pos - node1.pos, axe1);
        float angle2 = Vector2.SignedAngle(pos - node2.pos, axe2);
        float angle3 = Vector2.SignedAngle(pos - node3.pos, axe3);
        bool res = angle1 > 0 && angle2 > 0 && angle3 > 0;
        //if (res) {
        //    Debug.Log($"{this} is inside triangle T({node1}, {node2}, {node3})");
        //}
        return res;
    }

    public bool IsInsideTriangle(List<Node> triangle) {
        Assert.IsTrue(triangle.Count == 3);
        return IsInsideTriangle(triangle[0], triangle[1], triangle[2]);
    }

    public Dictionary<Vector2, List<Edge>> MapEdgesExclusif() {
        List<Vector2> availableNormals = MathTools.GetNormals2D();
        List<Edge> remainingEdges = GetEdgesCopy();
        Dictionary<Vector2, List<Edge>> affectedEdges = new Dictionary<Vector2, List<Edge>>();
        foreach(Vector2 normal in availableNormals) {
            affectedEdges[normal] = new List<Edge>();
        }

        for (int i = 0; i < Mathf.Min(edges.Count, 4); i++) {
            Edge bestEdge = MathTools.Argmin(remainingEdges, e => AngleToClosestDirection(e.From(this), availableNormals));
            remainingEdges.Remove(bestEdge);
            Vector2 chosenNormal = ClosestDirection(bestEdge.From(this), availableNormals);
            affectedEdges[chosenNormal].Add(bestEdge);
            availableNormals.Remove(chosenNormal);
        }

        return affectedEdges;
    }

    public float AngleToClosestDirection(Vector2 vector, List<Vector2> directions) {
        return directions.Select(d => Vector2.Angle(d, vector)).Min();
    }

    public Vector2 ClosestDirection(Vector2 vector, List<Vector2> directions) {
        return MathTools.Argmin(directions, d => Vector2.Angle(d, vector));
    }

    public Dictionary<Vector2, List<Edge>> MapEdgesInclusif() {
        List<Vector2> availableNormals = MathTools.GetNormals2D();
        Dictionary<Vector2, List<Edge>> affectedEdges = new Dictionary<Vector2, List<Edge>>();
        foreach (Vector2 normal in availableNormals) {
            affectedEdges[normal] = new List<Edge>();
        }

        foreach(Edge edge in edges) {
            Vector2 chosenNormal = ClosestDirection(edge.From(this), availableNormals);
            affectedEdges[chosenNormal].Add(edge);
        }

        return affectedEdges;
    }

    public List<Vector2> GetRemainingEdgesDirections() {
        Dictionary<Vector2, List<Edge>> affectedEdges = MapEdgesInclusif();
        List<Vector2> remainingEdges = affectedEdges.Keys.ToList().FindAll(k => affectedEdges[k].Count == 0);
        return remainingEdges;
    }

    public bool HasAnOppositeDirectionRemaining(List<Vector2> directions) {
        List<Vector2> opposites = directions.Select(d => -d).ToList();
        return GetRemainingEdgesDirections().Any(d => opposites.Contains(d));
    }

    public bool IsInConeOfDirections(Node other, List<Vector2> directions) {
        return directions.Any(d => Vector2.Angle(pos - other.pos, d) < 45);
    }


    public void SetColorHue(float hue) {
        Color color = material.color;
        Color outlineColor = material.GetColor(OUTLINE_COLOR);
        material.color = ColorsTools.SetHue(color, hue);
        material.SetColor(OUTLINE_COLOR, ColorsTools.SetHue(outlineColor, hue));
    }

    public bool CouldBeLinkedWith(Node other) {
        // This don't check for edge collisions !
        var remaining = GetRemainingEdgesDirections();
        var otherRemaining = other.GetRemainingEdgesDirections();
        return other.IsInConeOfDirections(this, remaining)
            && IsInConeOfDirections(other, otherRemaining)
            && other.HasAnOppositeDirectionRemaining(remaining);
    }

    public Node GetVoisinForDirection(Vector2 normal) {
        Dictionary<Vector2, List<Edge>> nextNodes = MapEdgesExclusif();
        Node nextNode = null;
        if (nextNodes[normal].Count == 0) {
            nextNode = GetClosestEdgeToDirectionLimit90(normal)?.Other(this);
        } else {
            nextNode = nextNodes[normal].First().Other(this);
        }
        return nextNode;
    }

    public List<Vector2> GetAllVoisinsDirections() {
        Dictionary<Vector2, List<Edge>> mapping = MapEdgesInclusif();
        List<Vector2> usedDirections = mapping.Keys.ToList().FindAll(k => mapping[k].Count > 0);
        return usedDirections;
    }
}
