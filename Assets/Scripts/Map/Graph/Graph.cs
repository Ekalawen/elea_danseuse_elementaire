using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Graph : MonoBehaviour {

    public GraphGenerator generator;
    public List<GraphModifier> modifiers;
    [Tooltip("If modifiers are silent, you will need to press S to apply them one by one")]
    public bool silentModifiers = false;

    [Header("Prefabs")]
    public GameObject nodePrefab;
    public GameObject edgePrefab;

    [HideInInspector]
    public List<Node> nodes;
    [HideInInspector]
    public List<Edge> edges;
    [HideInInspector]
    public Transform nodesFolder;
    [HideInInspector]
    public Transform edgesFolder;

    protected GameManager gm;
    protected int silentModifierIndice = 0;

    public void Initialize()
    {
        name = "Graph";
        gm = GameManager.Instance;
        nodesFolder = new GameObject("Nodes").transform;
        edgesFolder = new GameObject("Edges").transform;
        nodesFolder.parent = transform;
        edgesFolder.parent = transform;

        GenerateGraph();
    }

    protected void GenerateGraph() {
        generator.Initialize(this);
        generator.Generate();
        nodes = generator.nodes;
        edges = generator.edges;
        ApplyModifiers();
    }

    protected void ApplyModifiers() {
        if(silentModifiers) {
            return;
        }
        foreach(GraphModifier modifier in modifiers) {
            ApplyModifier(modifier);
        }
    }

    protected void ApplyModifier(GraphModifier modifier) {
        modifier.Initialize(this);
        modifier.Apply();
    }

    public void Update() {
        //if(Input.GetKeyDown(KeyCode.R)) {
        //    ResetGraph();
        //}
        if(Input.GetKeyDown(KeyCode.N)) {
            GenerateGraphNextStep();
        }
        if(Input.GetKeyDown(KeyCode.S)) {
            ApplyNextSilentModifier();
        }
    }

    protected void ApplyNextSilentModifier() {
        if(!silentModifiers) {
            return;
        }
        ApplyModifier(modifiers[silentModifierIndice]);
        silentModifierIndice++;
    }

    protected void GenerateGraphNextStep() {
        generator.Step();
        nodes = generator.nodes;
        edges = generator.edges;
    }

    protected void ResetGraph() {
        DeleteGraph();
        GenerateGraph();
        silentModifierIndice = 0;
    }

    protected void DeleteGraph() {
        foreach(Edge edge in edges.FindAll(e => e != null)) {
            Destroy(edge.gameObject);
        }
        foreach(Node node in nodes) {
            Destroy(node.gameObject);
        }
        nodes = null;
        edges = null;
    }

    public Edge CreateEdge(Node node1, Node node2) {
        if (node1.IsVoisin(node2) || node2.IsVoisin(node1)) {
            return null;
        }
        if (nodes.Any(n => n.IsInsideEdge(node1, node2))) {
            return null;
        }
        if(edges.Any(e => e.WillIntersectEdgeStrictly(node1, node2))) {
            return null;
        }
        Edge edge = Instantiate(edgePrefab, parent: edgesFolder).GetComponent<Edge>();
        edge.Initialize(node1, node2);
        edges.Add(edge);
        return edge;
    }

    public Node CreateNode(Vector2 pos) {
        Node node = Instantiate(nodePrefab, parent: nodesFolder).GetComponent<Node>();
        node.Initialize(pos);
        nodes.Add(node);
        return node;
    }

    public void HighlightNodes(List<Node> toHighlight) {
        float hue = ColorsTools.RandomHue();
        foreach(Node node in toHighlight) {
            node.SetColorHue(hue);
        }
    }

    public void DestroyNodeWithEdges(Node node) {
        List<Edge> nodeEdges = node.edges.Select(e => e).ToList();
        foreach (Edge e in nodeEdges) {
            edges.Remove(e);
            e.Delete();
        }
        nodes.Remove(node);
        Destroy(node.gameObject);
    }

    public Node GetFarestFromPlayer() {
        return FarestFrom(gm.player.pos);
    }

    public Node ClosestTo(Vector2 pos) {
        return MathTools.Argmin(nodes, n => Vector2.SqrMagnitude(n.pos - pos));
    }

    public Node ClosestToNotIn(Vector2 pos, List<Node> notInList) {
        List<Node> selectedNodes = nodes.FindAll(n => !notInList.Contains(n));
        return MathTools.Argmin(selectedNodes, n => Vector2.SqrMagnitude(n.pos - pos));
    }

    public Node ClosestToNotIn(Vector2 pos, Node notThisNode) {
        return ClosestToNotIn(pos, new List<Node>() { notThisNode });
    }

    public Node ClosestAndSafeNodeTo(Vector2 pos) {
        return MathTools.Argmin(SafeNodes(), n => Vector2.SqrMagnitude(n.pos - pos));
    }

    public Node FarestFrom(Vector2 pos) {
        return MathTools.Argmax(nodes, n => Vector2.SqrMagnitude(n.pos - pos));
    }

    public List<Node> OccupiedCurrentNodes() {
        // Don't include objects !
        List<Node> occupied = gm.enemyManager.Characters().Select(c => c.CurrentNode()).ToList();
        return occupied.Distinct().ToList();
    }

    public List<Node> OccupiedTargetingNodes() {
        // Don't include objects !
        List<Node> occupied = gm.enemyManager.Characters().Select(c => c.TargetingNode()).ToList();
        return occupied.Distinct().ToList();
    }

    public List<Node> OccupiedCurrentAndTargetingNodes() {
        // Don't include objects !
        List<Node> occupied = OccupiedCurrentNodes();
        occupied.AddRange(OccupiedTargetingNodes());
        return occupied.Distinct().ToList();
    }

    public List<Node> OccupiedInSocleNodes(float soclesOffset = 0) {
        List<Node> occupied = gm.enemyManager.Characters().SelectMany(c => c.NodesInSocle(soclesOffset)).ToList();
        occupied.AddRange(gm.objectManager.objects.SelectMany(o => o.NodesInSocle(soclesOffset)).ToList());
        return occupied.Distinct().ToList();
    }

    public List<Node> EmptyNodes(float offset = 0) {
        List<Node> freeNodes = nodes.Select(n => n).ToList();
        List<Node> occupiedNodes = OccupiedInSocleNodes(offset);
        freeNodes = freeNodes.FindAll(n => !occupiedNodes.Contains(n));
        return freeNodes;
    }

    public List<Node> EmptyNodesInRangeTo(Vector2 pos, float range, float offset = 0) {
        List<Node> emptyNodes = EmptyNodes(offset);
        float r2 = range * range;
        return emptyNodes.FindAll(n => Vector2.SqrMagnitude(n.pos - pos) <= r2);
    }

    protected List<Node> SafeNodes() {
        List<Node> nodes = this.nodes.Select(n => n).ToList();
        List<Vector2> enemyPositions = gm.enemyManager.EnemyPositions();
        float safeRange = 1.0f;
        return nodes.FindAll(n => enemyPositions.All(p => Vector2.SqrMagnitude(p - n.pos) >= safeRange));
    }

    public List<Node> GetAllPosAtLeastAsFarFrom(Vector2 fromPos, float distance) {
        List<Node> candidates = new List<Node>();
        for (float currentDistance = distance; currentDistance > 0 && candidates.Count == 0; currentDistance--) {
            candidates = nodes.FindAll(n => IsAtLeastAsFarFrom(n.pos, fromPos, distance));
        }
        return candidates;
    }

    public List<Node> GetAllAtLeastAsFarFrom(List<Node> nodes, Vector2 fromPos, float distance) {
        List<Node> candidates = new List<Node>();
        for (float currentDistance = distance; currentDistance > 0 && candidates.Count == 0; currentDistance--) {
            candidates = nodes.FindAll(n => IsAtLeastAsFarFrom(n.pos, fromPos, distance));
        }
        return candidates;
    }

    public bool IsAtLeastAsFarFrom(Vector2 pos, Vector2 fromPos, float distance) {
        float d2 = distance * distance;
        return Vector2.SqrMagnitude(pos - fromPos) >= d2;
    }

    public float xMin { get { return nodes.Select(n => n.pos.x).Min(); } }
    public float xMax { get { return nodes.Select(n => n.pos.x).Max(); } }
    public float yMin { get { return nodes.Select(n => n.pos.y).Min(); } }
    public float yMax { get { return nodes.Select(n => n.pos.y).Max(); } }

    public Bounds BoundingBox() {
        return new Bounds(Center(), Size());
    }

    public Vector2 Center() {
        return new Vector2((xMin + xMax) / 2, (yMin + yMax) / 2);
    }

    public Vector2 Size() {
        return new Vector2(xMax - xMin, yMax - yMin);
    }

    public float MaxSize() {
        return Mathf.Max(Size().x, Size().y);
    }

    public float Diagonal() {
        Vector2 size = Size();
        return Mathf.Sqrt(size.x * size.x + size.y * size.y);
    }

    public Node GetFarestNodeInDirection(Vector2 direction) {
        Vector2 center = Center();
        return MathTools.Argmax(nodes, n => Vector2.Dot(n.pos - center, direction));
    }
}
