﻿using System;
using System.Collections;
using UnityEngine;

public enum Faction
{
    PLAYER,
    ENEMY,
    NEUTRAL,
};
