﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;

public class PlayerController : CharacterController {

    public float attackBufferDuration = 0.1f;
    public float movementBufferDuration = 0.1f;

    protected SingleCoroutine attackBufferCoroutine;
    protected SingleCoroutine movementBufferCoroutine;
    protected Player player;
    protected KeyCode lastArrowKeyDown = KeyCode.RightArrow;
    protected List<PlayerControllerDirection> directions;

    public override void Initialize(Character character) {
        base.Initialize(character);
        attackBufferCoroutine = new SingleCoroutine(this);
        movementBufferCoroutine = new SingleCoroutine(this);
        player = character.GetComponent<Player>();
        InitializeDiretions();
    }

    protected void InitializeDiretions() {
        List<KeyCode> keycodes = new List<KeyCode>() { KeyCode.UpArrow, KeyCode.DownArrow, KeyCode.LeftArrow, KeyCode.RightArrow };
        List<Vector2> normals = new List<Vector2>() { Vector2.up, Vector2.down, Vector2.left, Vector2.right };
        directions = new List<PlayerControllerDirection>();
        for(int i = 0; i < 4; i++) {
            directions.Add(new PlayerControllerDirection(keycodes[i], normals[i]));
        }
    }

    protected override void UpdateMouvement() {
        ResetDirectionsTimerWhenPressed();
        TryMoveInDirection();

        TryAttack();
    }

    protected void TryAttack() {
        if (Input.GetKeyDown(KeyCode.Space)) {
            AttackBuffered();
        }
    }

    protected void TryMoveInDirection() {
        PlayerControllerDirection lastDirection = GetLastPressedDirection();
        if (lastDirection != null) {
            MoveInDirectionBuffered(lastDirection.direction);
        }
    }

    protected void ResetDirectionsTimerWhenPressed() {
        foreach(PlayerControllerDirection direction in directions) {
            if(Input.GetKeyDown(direction.keycode)) {
                direction.timer.Reset();
            }
        }
    }

    protected PlayerControllerDirection GetLastPressedDirection() {
        List<Vector2> possibleDirections = character.state == Character.State.ON_NODE ?
            character.CurrentNode().GetAllVoisinsDirections()
            : MathTools.GetNormals2D();
        List<PlayerControllerDirection> directionsPressed = directions.FindAll(d => d.IsPressed() && possibleDirections.Contains(d.direction));
        if(directionsPressed.Count == 0) {
            return null;
        }
        PlayerControllerDirection lastDirection = MathTools.Argmin(directionsPressed, d => d.timer.GetElapsedTime());
        return lastDirection;
    }

    private void MoveInDirectionBuffered(Vector2 normal) {
        bool hasMove = false;
        if (character.state == Character.State.ON_NODE) {
            hasMove = MoveInDirection(normal);
        }
        if(!hasMove) {
            movementBufferCoroutine.Start(CStartMovementBuffer(normal));
        } else {
            movementBufferCoroutine.Stop();
        }
    }

    protected IEnumerator CStartMovementBuffer(Vector2 normal) {
        Timer timer = new Timer(movementBufferDuration);
        while(!timer.IsOver() && player != null) {
            yield return null;
            if(CanMoveInDirection(normal)) {
                MoveInDirection(normal);
                break;
            }
        }
    }

    protected bool CanMoveInDirection(Vector2 normal) {
        return character.state == Character.State.ON_NODE && character.CurrentNode().GetVoisinForDirection(normal) != null;
    }

    protected void AttackBuffered() {
        bool hasAttacked = character.Attack();
        if (!hasAttacked) {
            attackBufferCoroutine.Start(CStartAttackBuffer());
        }
    }

    protected IEnumerator CStartAttackBuffer() {
        Timer timer = new Timer(attackBufferDuration);
        while(!timer.IsOver() && player != null) {
            yield return null;
            if(CanAttackSomeoneWithWeapon()) {
                player.DontUseAttackAnimationNextTime();
                character.Attack();
                break;
            }
        }
    }

    public bool CanAttackSomeoneWithWeapon() {
        AttackRange weaponAttack = player.GetAttackRange();
        bool canTrigger = weaponAttack.CanTrigger();
        bool anyOpponentInRange = weaponAttack.range.CurrentCollisionOf<Character>().Any(c => FactionHelper.OppositeFactions(player, c));
        return canTrigger && anyOpponentInRange;
    }
}
