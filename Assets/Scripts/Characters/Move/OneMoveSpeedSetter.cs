﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class OneMoveSpeedSetter : SpeedModifier {
    public OneMoveSpeedSetter(float speed) : base(Mode.SET, Period.UNTIL_NEXT_MOVE, speed){
    }
}