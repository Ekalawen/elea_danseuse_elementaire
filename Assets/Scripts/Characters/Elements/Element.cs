﻿using System;
using System.Collections;
using UnityEngine;

public enum Element
{
    NO_ELEMENT,
    FIRE,
    WATER,
    PLANT,
    // and maybe VOID, but only maybe ;)
};
