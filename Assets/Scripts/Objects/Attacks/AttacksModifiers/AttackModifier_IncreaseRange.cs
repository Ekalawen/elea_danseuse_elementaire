﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;

public class AttackModifier_IncreaseRange : AttackModifier_OnEvent {

    public float rangeIncrease = 0.5f;
    public bool shouldReverseRangeModification = true;
    [ConditionalHide("shouldReverseRangeModification")]
    public float increaseDuration = 1.3f;
    public bool useMinRange = false;
    [ConditionalHide("useMinRange")]
    public float minRange = 0.0f;

    protected AttackRange attackRange;

    public override void Initialize(Attack attack, bool isUpgrade = false, Rarity upgradeRarity = Rarity.COMMON) {
        base.Initialize(attack, isUpgrade, upgradeRarity);
        this.attackRange = (AttackRange)attack;
        Assert.IsNotNull(attackRange);
        AddListenerToEvents(IncreaseRange);
    }

    public void IncreaseRange(Character other)
    {
        float variation = GetRangeVariation();
        if (shouldReverseRangeModification)
        {
            attackRange.range.rangeModifier.AddModifier(FloatModifier.Mode.ADD, FloatModifier.Period.DURATION, variation, increaseDuration);
        }
        else
        {
            attackRange.range.rangeModifier.AddModifier(FloatModifier.Mode.ADD, FloatModifier.Period.INFINITE_DURATION, variation);
        }
    }

    protected float GetRangeVariation() {
        if(rangeIncrease >= 0) {
            return rangeIncrease;
        }
        return -Mathf.Min(attackRange.range.Get() - MinRange(), -rangeIncrease);
    }

    protected float MinRange() {
        float coef = attackRange.range.rangeModifier.GetMultipliersCoef();
        return useMinRange ? minRange * coef : 0.0f;
    }

    public override void UnEquip() {
        StopAllCoroutines();
        base.UnEquip();
    }

    public override string Description() {
        string duration = shouldReverseRangeModification ? $"pendant {increaseDuration}s" : "";
        string plusMoins = rangeIncrease >= 0 ? "plus" : "moins";
        return $"{Mathf.Abs(rangeIncrease)} de portée en {plusMoins} {duration} {DescriptionOnEvent()}";
    }

    public override string DescriptionOneWord() {
        return "augmentée";
    }
}