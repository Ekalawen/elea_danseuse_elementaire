using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WaterSlider : ElementSlider {
    protected override void LinkListenersToEvents() {
        elementManager.onStartSufferWater.AddListener(StartCoroutine);
        elementManager.onStopSufferWater.AddListener(StopCoroutine);
        elementManager.onWaterExplode.AddListener(RememberExploded);
    }
}
