using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GraphGenerator_Grid : GraphGenerator {

    public Vector2Int size = new Vector2Int(10, 10);
    public float espacement = 1;

    public override void CreateGraph() {
        Node lastNodeInLigne = null;
        List<Node> previousNodeLigne = Enumerable.Repeat<Node>(null, size.x).ToList();
        for(int j = 0; j < size.y; j++) {
            for(int i = 0; i < size.x; i++) {
                Vector2 pos = new Vector2(i * espacement, j * espacement);
                Node currentNode = CreateNode(pos);
                nodes.Add(currentNode);

                if(lastNodeInLigne) {
                    CreateAndAddEdge(currentNode, lastNodeInLigne);
                }
                lastNodeInLigne = (i != size.x - 1) ? currentNode : null;

                if(previousNodeLigne[i]) {
                    CreateAndAddEdge(currentNode, previousNodeLigne[i]);
                }
                previousNodeLigne[i] = currentNode;
            }
        }
    }
}
