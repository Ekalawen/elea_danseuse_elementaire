using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.ResourceManagement.AsyncOperations;

public class Book : Object {

    public bool shouldRepop = false;
    [ConditionalHide("shouldRepop")]
    public int nbTimesRepop = 0;

    protected UIManager uIManager;

    public override void Initialize(Node startingNode) {
        base.Initialize(startingNode);
        uIManager = gm.uiManager;
    }

    public override bool CanBePickedUpBy(Character character) {
        return true;
    }

    protected override void PickUpBySpecific(Socle objectSocle, Character character) {
        IncrementBookCount();
        uIManager.SetBookCounterTo(GetBookCount());
        objectManager.onCatchBook.Invoke(this);
        Repop();
    }

    public bool WillRepop() {
        return shouldRepop && nbTimesRepop > 0;
    }

    protected void Repop() {
        if(!shouldRepop || nbTimesRepop <= 0) {
            return;
        }
        nbTimesRepop--;
        objectManager.SpawnObject(gameObject, null);
    }

    public static int GetBookCount() {
        return PrefsManager.GetInt(PrefsManager.BOOK_COUNT_KEY, 0);
    }

    public static void IncrementBookCount() {
        PrefsManager.IncrementInt(PrefsManager.BOOK_COUNT_KEY, 1, 0);
    }

    public static void SetBookCount(int value) {
        PrefsManager.SetInt(PrefsManager.BOOK_COUNT_KEY, value);
    }
}
