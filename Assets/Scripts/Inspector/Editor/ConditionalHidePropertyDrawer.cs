﻿using UnityEngine;
using UnityEditor;
using System.Linq;

[CustomPropertyDrawer(typeof(ConditionalHideAttribute))]
public class ConditionalHidePropertyDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        ConditionalHideAttribute condHAtt = (ConditionalHideAttribute)attribute;
        bool enabled = GetConditionalHideAttributeResult(condHAtt, property);

        bool wasEnabled = GUI.enabled;
        GUI.enabled = enabled;
        if (!condHAtt.HideInInspector || enabled)
        {
            EditorGUI.PropertyField(position, property, label, true);
        }

        GUI.enabled = wasEnabled;
    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        ConditionalHideAttribute condHAtt = (ConditionalHideAttribute)attribute;
        bool enabled = GetConditionalHideAttributeResult(condHAtt, property);

        if (!condHAtt.HideInInspector || enabled)
        {
            return EditorGUI.GetPropertyHeight(property, label);
        }
        else
        {
            return -EditorGUIUtility.standardVerticalSpacing;
        }
    }

    private bool GetConditionalHideAttributeResult(ConditionalHideAttribute condHAtt, SerializedProperty property)
    {
        bool enabled = true;
        string propertyPath = property.propertyPath; //returns the property path of the property we want to apply the attribute to
        string conditionPath = propertyPath.Replace(property.name, condHAtt.ConditionalSourceField); //changes the path to the conditionalsource property path
        SerializedProperty sourcePropertyValue = property.serializedObject.FindProperty(conditionPath);
        //Debug.Log($"propertyPath = {propertyPath}");
        //Debug.Log($"conditionPath = {conditionPath}");
        //Debug.Log($"sourcePropertyValue.serializedObject.targetObject = {sourcePropertyValue.serializedObject.targetObject.GetType()}");
        //Debug.Log($"sourcePropertyValue.propertyPath = {sourcePropertyValue.propertyPath}");

        System.Type parentType = sourcePropertyValue.serializedObject.targetObject.GetType();

        //// This is good ==>
        //string[] splitedPath = sourcePropertyValue.propertyPath.Split('.');
        //for(int i = 0; i < splitedPath.Count() - 1; i++) {
        //    Debug.Log($"Looking for {splitedPath[i]} in {parentType.Name} ...");
        //    parentType = parentType.GetField(splitedPath[i]).FieldType;
        //    Debug.Log($"New parentType = {parentType.Name}");
        //    sourcePropertyValue = 
        //}
        System.Reflection.FieldInfo fi = parentType.GetField(sourcePropertyValue.propertyPath);
        //System.Reflection.FieldInfo fi = parentType.GetField(splitedPath.Last());
        object sourceValue = fi.GetValue(sourcePropertyValue.serializedObject.targetObject);

        if (sourcePropertyValue != null)
        {
            enabled = (sourceValue.Equals(condHAtt.SourceTrueValue)) ^ condHAtt.ShouldReverseDisplay;
        }
        else
        {
            Debug.LogWarning("Attempting to use a ConditionalHideAttribute but no matching SourcePropertyValue found in object: " + condHAtt.ConditionalSourceField);
        }

        return enabled;
    }
}