using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Events;
using System.Linq;

public class FloatModifierManager {

    protected List<FloatModifier> modifiers;
    protected Func<float> getter;
    protected MonoBehaviour holder;

    [HideInInspector]
    public UnityEvent<FloatModifier> onAddModifier = new UnityEvent<FloatModifier>();
    [HideInInspector]
    public UnityEvent<FloatModifier> onRemoveModifier = new UnityEvent<FloatModifier>();
    [HideInInspector]
    public UnityEvent onChangeValue = new UnityEvent();

    public FloatModifierManager(MonoBehaviour holder, Func<float> getter) {
        this.holder = holder;
        this.getter = getter;
        modifiers = new List<FloatModifier>();
    }

    public float Get() {
        RemoveOverModifiers();
        float value = getter();
        foreach (FloatModifier setter in modifiers.FindAll(m => m.IsSetter())) {
            value = setter.Get();
        }
        foreach (FloatModifier adder in modifiers.FindAll(m => m.IsAdder())) {
            value += adder.Get();
        }
        foreach (FloatModifier multiplier in modifiers.FindAll(m => m.IsMultiplier())) {
            value *= multiplier.Get();
        }
        return value;
    }

    public FloatModifier AddModifier(FloatModifier.Mode mode, FloatModifier.Period period, float value, float duration = 0) {
        return AddModifier(new FloatModifier(mode, period, value, duration));
    }

    public FloatModifier SetSizeModifier(float value) {
        return AddModifier(FloatModifier.Mode.SET, FloatModifier.Period.INFINITE_DURATION, value);
    }

    public FloatModifier AddModifier(FloatModifier modifier) {
        if(modifier.IsSetter()) {
            RemoveAllSetterModifiers();
        }
        modifier.Initialize(this);
        modifiers.Add(modifier);
        onAddModifier.Invoke(modifier);
        onChangeValue.Invoke();
        return modifier;
    }

    protected void RemoveAllSetterModifiers() {
        List<FloatModifier> setterModifiers = modifiers.FindAll(m => m.IsSetter());
        foreach(FloatModifier setter in setterModifiers) {
            RemoveModifier(setter);
        }
    }

    public bool RemoveModifier(FloatModifier modifier) {
        bool removed = modifiers.Remove(modifier);
        if (removed) {
            onRemoveModifier.Invoke(modifier);
            onChangeValue.Invoke();
        }
        return removed;
    }

    protected void RemoveOverModifiers() {
        List<FloatModifier> toRemoveModifier = modifiers.FindAll(m => m.IsOver());
        modifiers = modifiers.FindAll(m => !m.IsOver());
        foreach(FloatModifier modifier in toRemoveModifier) {
            onRemoveModifier.Invoke(modifier);
        }
        if(toRemoveModifier.Count > 0) {
            onChangeValue.Invoke();
        }
    }

    public List<FloatModifier> GetModifiers() {
        return modifiers;
    }

    public float GetMultipliersCoef() {
        List<FloatModifier> multipliers = modifiers.FindAll(m => m.IsMultiplier());
        return Enumerable.Aggregate(multipliers, 1.0f, (acc, m) => acc * m.Get());
    }

    public float GetAddedValue() {
        List<FloatModifier> adders = modifiers.FindAll(m => m.IsAdder());
        return Enumerable.Aggregate(adders, 0.0f, (acc, m) => acc + m.Get());
    }

    public MonoBehaviour Holder() {
        return holder;
    }
}