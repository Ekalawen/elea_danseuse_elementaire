﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransformSpawningMethod : NormalSpawningMethod {

    public float maxRange = 4.0f;
    public float minSpawnDistance = 3.0f;
    public bool couldExpandRange = false;
    [ConditionalHide("couldExpandRange")]
    public float expandRangeStep = 1.0f;

    public override void Initialize(SpawnerManager manager) {
        base.Initialize(manager);
    }

    public override Node GetNode(float size) {
        if (couldExpandRange) {
            return GetExpandingRangeNode(size);
        }
        return GetNotExpandingRangeNode(size);
    }

    private Node GetNotExpandingRangeNode(float size) {
        List<Node> emptyNodes = gm.graph.EmptyNodesInRangeTo(transform.position, maxRange, offset: size / 2);
        List<Node> candidates = gm.graph.GetAllAtLeastAsFarFrom(emptyNodes, gm.player.pos, minSpawnDistance);
        if (candidates.Count == 0)
        {
            return null;
        }
        return MathTools.ChoseOne(candidates);
    }

    private Node GetExpandingRangeNode(float size) {
        List<Node> candidates = new List<Node>();
        for(float currentRange = maxRange; currentRange <= gm.map.graph.Diagonal() && candidates.Count == 0; currentRange += expandRangeStep) {
            List<Node> emptyNodes = gm.graph.EmptyNodesInRangeTo(transform.position, currentRange, offset: size / 2);
            candidates = gm.graph.GetAllAtLeastAsFarFrom(emptyNodes, gm.player.pos, minSpawnDistance);
        }
        if (candidates.Count == 0) {
            return null;
        }
        return MathTools.ChoseOne(candidates);
    }
}
