﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrefsManager {

    public static string TRUE = "true";
    public static string FALSE = "false";

    public static string HAS_ALREADY_PLAY_KEY = "hasAlreadyPlayKey";
    public static string BOOK_COUNT_KEY = "bookCountKey";

    public static string LEVEL_COMPLETED_SUFFIX = "_Completed";

    public static int GetInt(string key, int defaultValue) {
        return PlayerPrefs.HasKey(key) ? PlayerPrefs.GetInt(key) : defaultValue;
    }
    public static float GetFloat(string key, float defaultValue) {
        return PlayerPrefs.HasKey(key) ? PlayerPrefs.GetFloat(key) : defaultValue;
    }
    public static string GetString(string key, string defaultValue) {
        return PlayerPrefs.HasKey(key) ? PlayerPrefs.GetString(key) : defaultValue;
    }
    public static bool GetBool(string key, bool defaultValue) {
        return PlayerPrefs.HasKey(key) ? PlayerPrefs.GetString(key) == PrefsManager.TRUE : defaultValue;
    }

    public static void SetInt(string key, int value) {
        PlayerPrefs.SetInt(key, value);
    }
    public static void IncrementInt(string key, int valueToAdd, int defaultValue) {
        int newValue = GetInt(key, defaultValue) + valueToAdd;
        PlayerPrefs.SetInt(key, newValue);
    }
    public static void SetFloat(string key, float value) {
        PlayerPrefs.SetFloat(key, value);
    }
    public static void SetString(string key, string value) {
        PlayerPrefs.SetString(key, value);
    }
    public static void SetBool(string key, bool value) {
        string boolString = value ? TRUE : FALSE;
        PlayerPrefs.SetString(key, boolString);
    }

    public static bool HasKey(string key) {
        return PlayerPrefs.HasKey(key);
    }

    public static void DeleteKey(string key) {
        PlayerPrefs.DeleteKey(key);
    }

    public static void DeleteAll() {
        PlayerPrefs.DeleteAll();
    }

    public static void Save() {
        PlayerPrefs.Save();
    }

    public static bool GetHasCompletedLevel(string levelSceneName) {
        string key = levelSceneName + LEVEL_COMPLETED_SUFFIX;
        return GetBool(key, false);
    }

    public static void SetHasCompletedLevel(string levelSceneName, bool value) {
        string key = levelSceneName + LEVEL_COMPLETED_SUFFIX;
        SetBool(key, value);
    }
}
