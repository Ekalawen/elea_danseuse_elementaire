﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

public class WeaponUpgrade : ItemUpgrade {

    public AttackModifier attackModifier;

    protected Weapon weapon;
    protected Attack attack;

    public override void Initialize(Item item) {
        base.Initialize(item);
        this.weapon = item.GetComponent<Weapon>();
        this.attack = weapon.instantiatedAttack;
        AddUpgradeToAttack(attack); // We are doing this to give the control to child classes
    }

    protected override void InitializeModifierForItemOnly(Item item) {
        // Nothing to do!
    }

    protected virtual void AddUpgradeToAttack(Attack attack) {
        attackModifier.Initialize(attack, isUpgrade: true, upgradeRarity: rarity);
    }

    public override string Description() {
        return attackModifier.Description();
    }

    public override string DescriptionOneWord() {
        return attackModifier.DescriptionOneWord();
    }

    public override void OnUnEquip() {
        // Nothing to do :)
    }
}